export const state = () => ({
  token: "",
  //用户角色
  roles: [],
  //用户信息
  userInfo: {},
  //登录状态
  isLogin: false,
})

export const mutations = {
  SET_TOKEN(state, token) {
    if(token){
      state.token = token;
      state.isLogin = true;
      //持久化
      this.$setToken(token);
      this.$cookies.set("isLogin", state.isLogin, {
        path: "/",
        maxAge: 60 * 60 * 24 * 7
      });
    }
  },
  SET_USERINFO(state, userInfo){
    if(userInfo){
      state.userInfo = userInfo;
    }
  },
  SET_ROLES(state, roles){
    if(roles.length > 0){
      state.roles = roles;
    }
  },
  SET_AVATAR(state, avatar){
    if(avatar){
      state.userInfo.avatar = avatar;
    }
  },
  DEL_LOGIN(state){
    state.userInfo = {};
    state.isLogin = false;
    state.roles = [];
  }
}

export const actions = {
  getUserInfo({ commit }, userInfo){
    commit("SET_USERINFO", userInfo);
  }
}
