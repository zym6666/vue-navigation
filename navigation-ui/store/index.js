export const state = () => ({
  //全局参数
  config: {},
  //分类
  classifyList: [],
  //菜单列表
  menuList: [],
})

export const mutations = {
  SET_CLASSIFY(state, classifyList){
    state.classifyList = classifyList;
  },
  SET_CONFIG(state, config){
    state.config = config;
  },
  SET_MENU(state, menuList){
    state.menuList = menuList;
  }
}

export const actions = {
  set_classify({commit}, classifyList){
    commit('SET_CLASSIFY', classifyList);
  },
  set_config({commit}, config){
    commit('SET_CONFIG', config);
  },
  set_menu({commit}, menuList){
    commit('SET_MENU', menuList);
  }
}
