export default ({ app },inject) => {

  const TokenKey = 'Token';

  inject("getToken", () => {
    return app.$cookies.get(TokenKey);
  });

  inject("setToken", (token) => {
    return app.$cookies.set(TokenKey, token, {
      path: "/",
      maxAge: 60 * 60 * 24 * 7
    });
  });

  inject("removeToken", () => {
    return app.$cookies.remove(TokenKey)}
  );
}
