const webpack = require('webpack')

export default {
  env: {
    MSG_TIME: 1500,
    NUXT_APP_BASE_API: '/api'
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'navigation-ui',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'element-ui/lib/theme-chalk/index.css',
    'element-ui/lib/theme-chalk/display.css',
    '@/static/css/reset.css',
    "@/static/css/common.css",
    '@/static/css/display.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/element-ui',
    '~/plugins/request',
    '~/plugins/auth',
    '~/api/login',
    '~/api/classify',
    '~/api/links',
    '~/api/label',
    '~/api/search',
    '~/utils/formatTime',
    '~/api/comments',
    '~/api/user',
    { src: '@/plugins/vue-cropper', ssr: false },
    '~/api/config',
    '~/api/menu',
    '~/api/interface',
    '~/api/dict'
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt'
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    //是否开启跨域
    proxy: true
  },

  proxy:{
    '/api':{
      target:'http://localhost:8080',
      pathRewrite:{
        '^/api': ''
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: [/^element-ui/],
    plugins: [
      new webpack.ProvidePlugin({
        '$' : 'jquery'
      })
    ]
  },

  //自定义加载组件
  loading: '~/components/Loading.vue',

  router: {
    middleware: 'auth'
  }
}
