export default (app) => {
  //判断是否登录，未登录则转向登录页
  if(!app.store.state.user.isLogin){
    app.redirect(302,{
      path: "/login?msg=您还未登录，请登录后再进行操作&redirect=" + app.route.path
    });
  }
  //获取设置信息
  app.app.$api_config.getList().then(res => {
    if(res.code === 200){
      app.store.dispatch("set_config", res.data);
    }
  });
}
