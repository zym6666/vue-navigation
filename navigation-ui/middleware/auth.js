export default (app) => {
  //从cookie中读取token
  let token = app.$getToken();
  if(token){
    app.store.commit("user/SET_TOKEN",token);
  }
  //获取设置信息
  app.app.$api_config.getList().then(res => {
    if(res.code === 200){
      app.store.dispatch("set_config", res.data);
    }
  });
}
