export default ({ app, $axios },inject) => {

  const LINKS = {};

  /**
   * Get all links
   * @param data 排序参数
   * @returns {*}
   */
  LINKS.getList = function (data) {
    return $axios({
      url: '/navigation/web/sortNoAuditList',
      method: 'get',
      params: data
    })
  };

  /**
   * 随机从数据库中获取10条链接
   * @returns {*}
   */
  LINKS.getRandomList = function (number) {
    return $axios({
      url: '/navigation/web/random',
      method: 'get',
      params: {
        number: number
      }
    })
  };

  /**
   * 查询链接信息
   * @returns {*}
   */
  LINKS.getInfo = function (id) {
    return $axios({
      url: '/navigation/web/info/' + id,
      method: 'get'
    })
  };

  /**
   * 增加浏览量
   */
  LINKS.addViewCount = function (id) {
    return $axios({
      url: '/navigation/web/addViewCount/' + id,
      method: 'get'
    })
  }

  /**
   * 收藏
   */
  LINKS.collect = function (id) {
    return $axios({
      url: '/navigation/navWebCollect/addCollect',
      method: 'get',
      params: {
        webId: id
      }
    });
  }

  /**
   * 取消收藏
   */
  LINKS.cancelCollect = function (id) {
    return $axios({
      url: '/navigation/navWebCollect/deleteCollect',
      method: 'get',
      params: {
        id: id
      }
    });
  }

  /**
   * 查询是否收藏
   */
  LINKS.isCollect = function (id) {
    return $axios({
      url: '/navigation/navWebCollect/isCollect',
      method: 'get',
      params: {
        id: id
      }
    });
  }

  /**
   * 查询当前用户的收藏列表
   */
  LINKS.getCollectList = function (userId, data) {
    return $axios({
      url: '/navigation/navWebCollect/list',
      method: 'get',
      params: {
        userId: userId,
        pageNum: data.pageNum,
        listSize: data.pageSize
      }
    });
  }

  /**
   * 提交链接
   */
  LINKS.submit = function (data) {
    return $axios({
      url: '/navigation/web/nav',
      method: 'post',
      data: data
    });
  }

  //查询链接数量
  LINKS.count = function () {
    return $axios({
      url: '/navigation/web/noAuditCount',
      method: 'get'
    })
  }

  //查询审核链接数量
  LINKS.auditCount = function () {
    return $axios({
      url: '/navigation/web/auditCount',
      method: 'get'
    })
  }

  inject('api_links',LINKS);
}
