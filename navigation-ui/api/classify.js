export default ({ app, $axios },inject) => {

  const CLASSIFY = {};

  //获取分类列表
  CLASSIFY.getList = function () {
    return $axios({
      url: '/navigation/classify/listAll',
      method: 'get'
    })
  };

  //获取分类信息
  CLASSIFY.getClassify = function (id) {
    return $axios({
      url: '/navigation/classify/' + id,
      method: 'get'
    })
  };

  inject('api_classify',CLASSIFY);
}
