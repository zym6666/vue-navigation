export default ({ app, $axios },inject) => {

  const SEARCH = {};

  /**
   * Get all 搜索
   * @returns {*}
   */
  SEARCH.getList = function () {
    return $axios({
      url: '/navigation/search/getList',
      method: 'get'
    })
  };

  inject('api_search',SEARCH);
}
