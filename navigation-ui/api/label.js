export default ({ app, $axios },inject) => {

  const LABEL = {};

  /**
   * Get all label
   * @param listSize 每页显示数量
   * @param pageNum 页码
   * @returns {*}
   */
  LABEL.getList = function (listSize, pageNum) {
    if(listSize == null){
      listSize = 12;
    }
    if(pageNum == null){
      pageNum = 1;
    }
    return $axios({
      url: '/navigation/label/list',
      method: 'get',
      params: {
        pageSize: listSize,
        pageNum: pageNum
      }
    })
  };

  //查询标签数量
  LABEL.count = function () {
    return $axios({
      url: '/navigation/label/labelCount',
      method: 'get'
    })
  }

  inject('api_label',LABEL);
}
