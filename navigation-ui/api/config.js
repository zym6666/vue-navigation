export default ({ app, $axios },inject) => {

  const CONFIG = {};

  /**
   * Get all Config
   *
   * @returns {*}
   */
  CONFIG.getList = function () {
    return $axios({
      url: '/navigation/config/list',
      method: 'get'
    })
  };

  inject('api_config',CONFIG);
}
