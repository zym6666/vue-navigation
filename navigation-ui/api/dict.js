export default ({ app, $axios },inject) => {

  const DICT = {};

  /**
   * 根据字典类型查询字典数据信息
   *
   * @returns {*}
   */
  DICT.getDictInfo = function (dictType) {
    return $axios({
      url: '/system/dict/data/type/' + dictType,
      method: 'get'
    })
  };

  inject('api_dict',DICT);
}
