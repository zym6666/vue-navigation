export default ({ app, $axios },inject) => {

  const COMMENTS = {};

  //获取评论列表
  COMMENTS.getList = function (webId, username, pageNum, pageSize) {
    if(pageSize == null){
      pageSize = 12;
    }
    if(pageNum == null){
      pageNum = 1;
    }
    return $axios({
      url: '/navigation/comments/pidList',
      method: 'get',
      params: {
        webId: webId,
        createBy: username,
        pageNum: pageNum,
        pageSize: pageSize
      }
    })
  };

  //新增评论
  COMMENTS.add = function (webId, commentsText, pId, username) {
    return $axios({
      url: '/navigation/comments',
      method: 'post',
      data: {
        commentsId: pId,
        commentsText: commentsText,
        webId: webId,
        btUsername: username
      }
    })
  }

  //删除评论
  COMMENTS.delete = function (id) {
    return $axios({
      url: '/navigation/comments/nav/' + id,
      method: 'delete'
    })
  }

  //查询评论数量
  COMMENTS.count = function () {
    return $axios({
      url: '/navigation/comments/commentsCount',
      method: 'get'
    })
  }

  inject('api_comments',COMMENTS);
}
