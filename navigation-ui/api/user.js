export default ({ app, $axios },inject) => {

  const USER = {};

  //获取用户详细信息
  USER.getInfo = function (id, username) {
    return $axios({
      url: '/system/user/getUserInfo',
      method: 'get',
      params: {
        id: id,
        username: username
      }
    })
  };

  //修改用户信息
  USER.updateInfo = function (data) {
    return $axios({
      url: '/system/user/profile',
      method: 'put',
      data: {
        user: data
      }
    })
  }

  //重置密码
  USER.updatePassword = function (oldPassword, newPassword) {
    return $axios({
      url: "/system/user/profile/updatePwd",
      method: 'put',
      params: {
        oldPassword: oldPassword,
        newPassword: newPassword
      }
    })
  }

  //上传头像
  USER.uploadAvatar = function (data) {
    return $axios({
      url: '/system/user/profile/avatar',
      method: 'post',
      data: data
    })
  }

  //生成密钥
  USER.generateKey = function () {
    return $axios({
      url: '/navigation/secretkey/random',
      method: 'get'
    })
  }

  inject('api_user',USER);
}
