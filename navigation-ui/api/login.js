export default ({ app, $axios },inject) => {

  const LOGIN = {};

  //登录方法
  LOGIN.login = function (data) {
    return $axios({
      url: '/login',
      headers: {
        isToken: false
      },
      method: 'post',
      data: data
    })
  };

  //注册方法
  LOGIN.register = function (data) {
    return $axios({
      url: '/register',
      headers: {
        isToken: false
      },
      method: 'post',
      data: data
    })
  };

  //获取用户详细信息
  LOGIN.getInfo = function () {
    return $axios({
      url: '/getInfo',
      method: 'get'
    })
  };

  //退出方法
  LOGIN.logout = function () {
    return $axios({
      url: '/logout',
      method: 'post'
    })
  };

  //获取验证码
  LOGIN.getCodeImg = function () {
    return $axios({
      url: '/captchaImage',
      headers: {
        isToken: false
      },
      method: 'get',
      timeout: 20000
    })
  };

  inject('api_login',LOGIN);
}
