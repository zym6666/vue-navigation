export default ({ app, $axios },inject) => {

  const MENU = {};

  /**
   * Get all menu
   *
   * @returns {*}
   */
  MENU.getList = function () {
    return $axios({
      url: '/navigation/menu/noLimitList',
      method: 'get'
    })
  };

  inject('api_menu',MENU);
}
