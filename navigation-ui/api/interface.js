export default ({ app, $axios },inject) => {

  const INTERFACE = {};

  //获取接口列表
  INTERFACE.getList = function (listSize, pageNum) {
    if(listSize == null){
      listSize = 12;
    }
    if(pageNum == null){
      pageNum = 1;
    }
    return $axios({
      url: '/navigation/api/list',
      method: 'get',
      params: {
        pageSize: listSize,
        pageNum: pageNum
      }
    })
  };

  //查询接口信息
  INTERFACE.getInfo = function (id) {
    return $axios({
      url: '/navigation/api/' + id,
      method: 'get'
    })
  };

  inject('api_interface',INTERFACE);
}
