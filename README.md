<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">聚合导航 v1.4</h1>
<h4 align="center">基于SpringBoot+Nuxt前后端分离开发的导航程序</h4>

## 技术栈
SpringBoot + Vue + Nuxt + Axios + ElementUI + Mybatis + Mybatis-plus + MySQL + Redis + SpringSecurity + JWT

## 项目结构
```
│-navigation-ui 前台
│-ruoyi-ui 后台
```

## 后台默认账号密码
账号：admin <br>
密码：admin123 <br>

## 功能介绍
功能等你探索~

## 更新日志
- v1.5
  - 1、新增附件管理
  - 2、修复手机端右上角按钮
  - 3、修复Vue3树形下拉不能默认选中
  - 4、修复代码生成图片/文件/单选时选择必填无法校验问题
  - 5、优化弹窗内容过多展示不全问题
- v1.4
  - 1、新增网站信息redis缓存
  - 2、新增菜单列表redis缓存，修改菜单业务处理方法
  - 3、新增搜索列表Redis缓存
  - 4、新增首页站点显示数量
  - 5、新增后台接口数量显示
  - 6、修复筛选总数量偶尔出现错误
  - 7、修复用户名不存在时会报错
  - 8、优化首页布局
  - 9、优化后台首页的系统版本号获取
  - 10、优化后端代码
  - 11、优化后台新增密钥逻辑
- v1.3
  - 1、新增前台接口预览页面
  - 2、新增后台接口管理模块
  - 3、新增前台菜单
  - 4、新增用户中心生成密钥
- v1.2
  - 1、修复后台新增网址窗口的标签数量只能显示前10个 
  - 2、新增评论状态控制
  - 3、新增多级评论
- v1.1
  - 1、修复在他人用户中心时也会触发上传头像效果
  - 2、修复上传头像组没有头像时，会出现空白的问题
  - 3、修复保存信息时会清空头像的问题
  - 4、修复手机抽屉导航栏标签显示不正确问题
  - 5、新增注册功能
  - 6、新增系统配置信息
- v1.0
    - 项目初期完工

## 演示站点
https://nav.zym88.cn/

## 项目截图
<table>
    <tr>
        <td><img src="./导航程序设计/前台图片/1.png"/></td>
        <td><img src="./导航程序设计/前台图片/2.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/前台图片/3.png"/></td>
        <td><img src="./导航程序设计/前台图片/4.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/前台图片/5.png"/></td>
        <td><img src="./导航程序设计/前台图片/6.png"/></td>
    </tr>
</table>
<table>
    <tr>
        <td><img src="./导航程序设计/后台截图/1.png"/></td>
        <td><img src="./导航程序设计/后台截图/2.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/后台截图/3.png"/></td>
        <td><img src="./导航程序设计/后台截图/4.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/后台截图/5.png"/></td>
        <td><img src="./导航程序设计/后台截图/6.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/后台截图/7.png"/></td>
        <td><img src="./导航程序设计/后台截图/8.png"/></td>
    </tr>
    <tr>
        <td><img src="./导航程序设计/后台截图/9.png"/></td>
        <td><img src="./导航程序设计/后台截图/10.png"/></td>
    </tr>
</table>