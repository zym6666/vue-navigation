package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.NavWebLabel;

/**
 * 网址关联标签Mapper接口
 *
 * @author zouyangming
 */
public interface NavWebLabelMapper extends BaseMapper<NavWebLabel> {

}
