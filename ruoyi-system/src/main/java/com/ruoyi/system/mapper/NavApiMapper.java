package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.NavApi;

/**
 * 接口列表Mapper接口
 * 
 * @author ZouYangMing
 * @date 2022-10-10
 */
public interface NavApiMapper 
{
    /**
     * 查询接口列表
     * 
     * @param id 接口列表主键
     * @return 接口列表
     */
    public NavApi selectNavApiById(Long id);

    /**
     * 查询接口列表列表
     * 
     * @param navApi 接口列表
     * @return 接口列表集合
     */
    public List<NavApi> selectNavApiList(NavApi navApi);

    /**
     * 新增接口列表
     * 
     * @param navApi 接口列表
     * @return 结果
     */
    public int insertNavApi(NavApi navApi);

    /**
     * 修改接口列表
     * 
     * @param navApi 接口列表
     * @return 结果
     */
    public int updateNavApi(NavApi navApi);

    /**
     * 删除接口列表
     * 
     * @param id 接口列表主键
     * @return 结果
     */
    public int deleteNavApiById(Long id);

    /**
     * 批量删除接口列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNavApiByIds(Long[] ids);
}
