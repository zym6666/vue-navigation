package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NavUserKey;

import java.util.List;

/**
 * 密钥列表Mapper接口
 * 
 * @author ZouYangMing
 * @date 2022-10-17
 */
public interface NavUserKeyMapper 
{
    /**
     * 查询密钥列表
     * 
     * @param id 密钥列表主键
     * @return 密钥列表
     */
    public NavUserKey selectNavUserKeyById(Long id);

    /**
     * 查询密钥列表列表
     * 
     * @param navUserKey 密钥列表
     * @return 密钥列表集合
     */
    public List<NavUserKey> selectNavUserKeyList(NavUserKey navUserKey);

    /**
     * 查询密钥是否存在
     *
     * @param key 密钥
     * @return 结果
     */
    public NavUserKey selectNavUserKeyByKey(String key);

    /**
     * 根据用户名查询密钥
     *
     * @param userName 用户名
     * @return 结果
     */
    public NavUserKey selectNavUserKeyByUserName(String userName);

    /**
     * 新增密钥列表
     * 
     * @param navUserKey 密钥列表
     * @return 结果
     */
    public int insertNavUserKey(NavUserKey navUserKey);

    /**
     * 修改密钥列表
     * 
     * @param navUserKey 密钥列表
     * @return 结果
     */
    public int updateNavUserKey(NavUserKey navUserKey);

    /**
     * 删除密钥列表
     * 
     * @param id 密钥列表主键
     * @return 结果
     */
    public int deleteNavUserKeyById(Long id);

    /**
     * 批量删除密钥列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNavUserKeyByIds(Long[] ids);
}
