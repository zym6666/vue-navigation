package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.NavLabel;

import java.util.List;

/**
 * 标签列表Mapper接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface NavLabelMapper extends BaseMapper<NavLabel>
{
    /**
     * 查询标签列表
     * 
     * @param id 标签列表主键
     * @return 标签列表
     */
    public NavLabel selectNavLabelById(Long id);

    /**
     * 查询标签列表列表
     * 
     * @param navLabel 标签列表
     * @return 标签列表集合
     */
    public List<NavLabel> selectNavLabelList(NavLabel navLabel);

    /**
     * 新增标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    public int insertNavLabel(NavLabel navLabel);

    /**
     * 修改标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    public int updateNavLabel(NavLabel navLabel);

    /**
     * 删除标签列表
     * 
     * @param id 标签列表主键
     * @return 结果
     */
    public int deleteNavLabelById(Long id);

    /**
     * 批量删除标签列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNavLabelByIds(Long[] ids);
}
