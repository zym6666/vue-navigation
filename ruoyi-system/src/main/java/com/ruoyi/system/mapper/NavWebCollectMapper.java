package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.NavWebCollect;

/**
 * 收藏Mapper接口
 *
 * @author zouyangming
 */
public interface NavWebCollectMapper extends BaseMapper<NavWebCollect> {

}
