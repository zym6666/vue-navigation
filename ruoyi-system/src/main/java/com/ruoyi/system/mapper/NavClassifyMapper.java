package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.NavClassify;

import java.util.List;

/**
 * 分类Mapper接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface NavClassifyMapper extends BaseMapper<NavClassify>
{
    /**
     * 查询分类
     * 
     * @param id 分类主键
     * @return 分类
     */
    public NavClassify selectNavClassifyById(Integer id);

    /**
     * 查询分类列表
     * 
     * @param navClassify 分类
     * @return 分类集合
     */
    public List<NavClassify> selectNavClassifyList(NavClassify navClassify);

    /**
     * 新增分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    public int insertNavClassify(NavClassify navClassify);

    /**
     * 修改分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    public int updateNavClassify(NavClassify navClassify);

    /**
     * 删除分类
     * 
     * @param id 分类主键
     * @return 结果
     */
    public int deleteNavClassifyById(Integer id);

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNavClassifyByIds(Integer[] ids);
}
