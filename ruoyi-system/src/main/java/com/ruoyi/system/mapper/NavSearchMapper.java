package com.ruoyi.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ruoyi.system.domain.NavSearch;

import java.util.List;

/**
 * 搜索列表Mapper接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface NavSearchMapper extends BaseMapper<NavSearch>
{
    /**
     * 查询搜索列表
     * 
     * @param id 搜索列表主键
     * @return 搜索列表
     */
    public NavSearch selectNavSearchById(Integer id);

    /**
     * 查询搜索列表列表
     * 
     * @param navSearch 搜索列表
     * @return 搜索列表集合
     */
    public List<NavSearch> selectNavSearchList(NavSearch navSearch);

    /**
     * 新增搜索列表
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    public int insertNavSearch(NavSearch navSearch);

    /**
     * 修改搜索列表
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    public int updateNavSearch(NavSearch navSearch);

    /**
     * 删除搜索列表
     * 
     * @param id 搜索列表主键
     * @return 结果
     */
    public int deleteNavSearchById(Integer id);

    /**
     * 批量删除搜索列表
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteNavSearchByIds(Integer[] ids);
}
