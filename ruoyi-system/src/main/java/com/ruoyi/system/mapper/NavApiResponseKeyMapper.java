package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NavApiResponseKey;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ZouYangMing
* @description 针对表【nav_api_response_key】的数据库操作Mapper
* @createDate 2022-10-12 16:13:48
* @Entity com.ruoyi.system.domain.NavApiResponseKey
*/
public interface NavApiResponseKeyMapper extends BaseMapper<NavApiResponseKey> {

}




