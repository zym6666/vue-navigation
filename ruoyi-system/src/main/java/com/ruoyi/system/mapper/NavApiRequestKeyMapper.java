package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.NavApiRequestKey;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author ZouYangMing
* @description 针对表【nav_api_request_key】的数据库操作Mapper
* @createDate 2022-10-12 16:05:21
* @Entity com.ruoyi.system.domain.NavApiRequestKey
*/
public interface NavApiRequestKeyMapper extends BaseMapper<NavApiRequestKey> {

}




