package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 搜索列表对象 nav_search
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavSearch extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /** 排序 */
    @Excel(name = "排序")
    private Integer searchSort;

    /** 搜索名称 */
    @Excel(name = "搜索名称")
    private String searchName;

    /** 搜索图标 */
    @Excel(name = "搜索图标")
    private String searchIcon;

    /** 搜索链接 */
    @Excel(name = "搜索链接")
    private String searchUrl;

    /** 状态 */
    @Excel(name = "状态")
    private String searchStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
