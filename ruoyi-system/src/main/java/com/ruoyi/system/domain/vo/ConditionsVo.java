package com.ruoyi.system.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 前台筛选条件
 *
 * @author ZouYangMing
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ConditionsVo {

    /**
     * 页码
     */
    Integer pageNum;

    /**
     * 每页显示数量
     */
    Integer listSize;

    /**
     * time按时间排序
     * comments按评论排序
     * browse按浏览量排序
     * collection按收藏量排序
     */
    String sort;

    /**
     * 标签id
     */
    String label;
}
