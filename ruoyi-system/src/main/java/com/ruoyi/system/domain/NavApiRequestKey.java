package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName nav_api_request_key
 */
@TableName(value ="nav_api_request_key")
@Data
public class NavApiRequestKey implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer apiId;

    private String requestName;

    private String requestRequired;

    private String requestType;

    private String requestText;
}