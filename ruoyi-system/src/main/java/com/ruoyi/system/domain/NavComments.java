package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 评论列表对象 nav_comments
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NavComments extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /** 上级评论ID */
    private Integer commentsId;

    /** 被回复者 */
    @TableField(value = "b_username")
    private String btUsername;

    /** 被回复者ID */
    @TableField(exist = false)
    private Integer btUserId;

    /** 链接id */
    @Excel(name = "链接id")
    private Integer webId;

    /** 内容 */
    @Excel(name = "内容")
    private String commentsText;

    /** 用户id */
    @TableField(exist = false)
    @Excel(name = "用户id")
    private Long userId;

    /** 昵称 */
    @TableField(exist = false)
    @Excel(name = "昵称")
    private String commentsNickName;

    /** 头像 */
    @TableField(exist = false)
    @Excel(name = "头像")
    private String commentsAvatar;

    /** 邮箱 */
    @TableField(exist = false)
    @Excel(name = "邮箱")
    private String commentsEmail;

    /** IP地址 */
    @Excel(name = "IP地址")
    private String commentsIp;

    /** IP归属地 */
    @Excel(name = "IP归属地")
    private String commentsIpadderss;

    /** 浏览器 */
    @Excel(name = "浏览器")
    private String commentsBrowser;

    /** 系统 */
    @Excel(name = "系统")
    private String commentsSystem;

    /** 状态 */
    @Excel(name = "状态")
    private String commentsStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 子评论列表 */
    @TableField(exist = false)
    private List<NavComments> children;
}
