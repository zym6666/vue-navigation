package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 密钥列表对象 nav_user_key
 * 
 * @author ZouYangMing
 * @date 2022-10-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavUserKey extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    private Long id;

    /** 用户名 */
    @Excel(name = "用户名")
    private String userName;

    /** 密钥 */
    @Excel(name = "密钥")
    private String keyNumber;

    /** 密钥状态 */
    @Excel(name = "密钥状态")
    private String keyStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
