package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 网址关联标签对象 nav_web_label
 *
 * @author zouyangming
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavWebLabel extends BaseEntity {

    /**
     * ID主键
     */
    @TableId(type = IdType.AUTO)
    Long id;

    /**
     * 网址ID
     */
    Long webId;

    /**
     * 标签ID
     */
    Long labelId;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
