package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 网址列表对象 nav_web
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavWeb extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 分类id */
    @Excel(name = "分类id")
    private Long classifyId;

    /** 网站名称 */
    @Excel(name = "网站名称")
    private String webName;

    /** 网站地址 */
    @Excel(name = "网站地址")
    private String webUrl;

    /** 网站图标 */
    @Excel(name = "网站图标")
    private String webIcon;

    /** 网站详情 */
    @Excel(name = "网站详情")
    private String webDetails;

    /** 浏览量 */
    @Excel(name = "浏览量")
    private Integer webViews;

    /** 收藏量 */
    @Excel(name = "收藏量")
    private Integer webCollection;

    /** 状态 */
    @Excel(name = "状态")
    private String webStatus;

    /** 审核状态 */
    private String auditStatus;

    /** 未通过理由 */
    private String auditReason;

    /** 分类名称 */
    @TableField(exist = false)
    String classifyName;

    /** 标签列表 */
    @TableField(exist = false)
    List<String> labelList;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
