package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 接口列表对象 nav_api
 * 
 * @author ZouYangMing
 * @date 2022-10-10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavApi extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @TableId(type = IdType.AUTO)
    private Long id;

    /** 接口名称 */
    @Excel(name = "接口名称")
    private String apiName;

    /** 接口介绍 */
    @Excel(name = "接口介绍")
    private String apiIntroduce;

    /** 请求地址 */
    @Excel(name = "请求地址")
    private String apiUrl;

    /** 请求格式 */
    @Excel(name = "请求格式")
    private String apiRequestType;

    /** 返回格式 */
    @Excel(name = "返回格式")
    private String apiResponseType;

    /** 请求示例 */
    @Excel(name = "请求示例")
    private String apiUrlExample;

    /** 返回示例 */
    @Excel(name = "返回示例")
    private String apiResponseExample;

    /** 接口状态 */
    @Excel(name = "接口状态")
    private String apiStatus;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    /** 更新者 */
    private String updateBy;

    /** 更新时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 请求参数 */
    @TableField(exist = false)
    private List<NavApiRequestKey> apiRequestKeyList;

    /** 返回参数 */
    @TableField(exist = false)
    private List<NavApiResponseKey> apiResponseKeyList;
}
