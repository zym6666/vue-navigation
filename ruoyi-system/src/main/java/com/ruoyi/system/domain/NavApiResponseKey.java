package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName nav_api_response_key
 */
@TableName(value ="nav_api_response_key")
@Data
public class NavApiResponseKey implements Serializable {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer apiId;

    private String responseName;

    private String responseType;

    private String responseText;
}