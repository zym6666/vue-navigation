package com.ruoyi.system.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 收藏表对象 nav_web_collect
 *
 * @author zouyangming
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavWebCollect {
    /**
     * ID主键
     */
    @TableId(type = IdType.AUTO)
    Long id;

    /**
     * 网址ID
     */
    Long webId;

    /**
     * 用户ID
     */
    Long userId;

    /** 创建者 */
    private String createBy;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
