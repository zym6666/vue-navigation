package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 菜单列表对象 nav_menu
 * 
 * @author ZouYangMing
 * @date 2022-09-14
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class NavMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 菜单名称 */
    @Excel(name = "菜单名称")
    private String menuName;

    /** 路由地址 */
    @Excel(name = "路由地址")
    private String menuUrl;

    /** 菜单状态 */
    @Excel(name = "菜单状态")
    private String menuStatus;

    /** 系统内置 */
    @Excel(name = "系统内置")
    private String menuType;

    /** 排序 */
    @Excel(name = "排序")
    private Integer menuSort;
}
