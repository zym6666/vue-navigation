package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavMenu;
import com.ruoyi.system.mapper.NavMenuMapper;
import com.ruoyi.system.service.INavMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.constant.CacheConstants.NAV_MENU_KEY;

/**
 * 菜单列表Service业务层处理
 * 
 * @author ZouYangMing
 * @date 2022-09-14
 */
@Service
public class NavMenuServiceImpl implements INavMenuService 
{
    @Autowired
    private NavMenuMapper navMenuMapper;

    @Autowired
    private RedisCache redisCache;

    /**
     * 查询菜单
     * 
     * @param id 菜单列表主键
     * @return 菜单列表
     */
    @Override
    public NavMenu selectNavMenuById(Long id)
    {
        return navMenuMapper.selectNavMenuById(id);
    }

    /**
     * 查询菜单列表
     * 
     * @param navMenu 菜单列表
     * @return 菜单列表
     */
    @Override
    public List<NavMenu> selectNavMenuList(NavMenu navMenu)
    {
        return navMenuMapper.selectNavMenuList(navMenu);
    }

    /**
     * 前台查询菜单列表
     *
     * @return 菜单列表集合
     */
    @Override
    public List<NavMenu> selectNavMenuNoLimitList() {
        List<NavMenu> menu = redisCache.getCacheObject(NAV_MENU_KEY + "list");
        if(menu != null){
            return menu;
        }
        menu = navMenuMapper.selectNavMenuList(new NavMenu());
        redisCache.setCacheObject(NAV_MENU_KEY + "list", menu);
        return menu;
    }

    /**
     * 新增菜单
     * 
     * @param navMenu 菜单列表
     * @return 结果
     */
    @Override
    public int insertNavMenu(NavMenu navMenu)
    {
        navMenu.setCreateBy(SecurityUtils.getUsername());
        navMenu.setCreateTime(DateUtils.getNowDate());
        navMenu.setCreateTime(DateUtils.getNowDate());
        int i = navMenuMapper.insertNavMenu(navMenu);
        clearCache();
        return i;
    }

    /**
     * 修改菜单
     * 
     * @param navMenu 菜单列表
     * @return 结果
     */
    @Override
    public int updateNavMenu(NavMenu navMenu)
    {
        navMenu.setUpdateBy(SecurityUtils.getUsername());
        navMenu.setUpdateTime(DateUtils.getNowDate());
        navMenu.setUpdateTime(DateUtils.getNowDate());
        int i = navMenuMapper.updateNavMenu(navMenu);
        clearCache();
        return i;
    }

    /**
     * 批量删除菜单
     * 
     * @param ids 需要删除的菜单列表主键
     * @return 结果
     */
    @Override
    public int deleteNavMenuByIds(Long[] ids)
    {
        int i = navMenuMapper.deleteNavMenuByIds(ids);
        clearCache();
        return i;
    }

    /**
     * 删除菜单
     * 
     * @param id 菜单列表主键
     * @return 结果
     */
    @Override
    public int deleteNavMenuById(Long id)
    {
        int i = navMenuMapper.deleteNavMenuById(id);
        clearCache();
        return i;
    }

    /**
     * 更新菜单缓存
     */
    public void clearCache() {
        //更新缓存
        redisCache.deleteObject(NAV_MENU_KEY + "list");
        redisCache.setCacheObject(NAV_MENU_KEY + "list", navMenuMapper.selectNavMenuList(new NavMenu()));
    }
}
