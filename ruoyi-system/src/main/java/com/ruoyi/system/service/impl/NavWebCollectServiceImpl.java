package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.NavWebCollect;
import com.ruoyi.system.mapper.NavWebCollectMapper;
import com.ruoyi.system.service.INavWebCollectService;
import org.springframework.stereotype.Service;

/**
 * 收藏Service业务层处理
 *
 * @author zouyangming
 */
@Service
public class NavWebCollectServiceImpl extends ServiceImpl<NavWebCollectMapper, NavWebCollect> implements INavWebCollectService {

}