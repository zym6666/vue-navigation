package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavWebLabel;

/**
 * 网址关联标签Service接口
 *
 * @author zouyangming
 */
public interface INavWebLabelService extends IService<NavWebLabel> {

}
