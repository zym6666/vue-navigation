package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavUserKey;
import com.ruoyi.system.mapper.NavUserKeyMapper;
import com.ruoyi.system.service.INavUserKeyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 密钥列表Service业务层处理
 * 
 * @author ZouYangMing
 * @date 2022-10-17
 */
@Service
public class NavUserKeyServiceImpl implements INavUserKeyService 
{
    @Autowired
    private NavUserKeyMapper navUserKeyMapper;

    /**
     * 查询密钥列表
     * 
     * @param id 密钥列表主键
     * @return 密钥列表
     */
    @Override
    public NavUserKey selectNavUserKeyById(Long id)
    {
        return navUserKeyMapper.selectNavUserKeyById(id);
    }

    /**
     * 查询密钥列表列表
     * 
     * @param navUserKey 密钥列表
     * @return 密钥列表
     */
    @Override
    public List<NavUserKey> selectNavUserKeyList(NavUserKey navUserKey)
    {
        return navUserKeyMapper.selectNavUserKeyList(navUserKey);
    }

    /**
     * 查询密钥是否存在
     *
     * @param key 密钥
     * @return 结果
     */
    @Override
    public NavUserKey selectNavUserKeyByKey(String key) {
        return navUserKeyMapper.selectNavUserKeyByKey(key);
    }

    /**
     * 根据用户名查询密钥
     *
     * @param userName 用户名
     * @return 结果
     */
    @Override
    public NavUserKey selectNavUserKeyByUserName(String userName) {
        return navUserKeyMapper.selectNavUserKeyByUserName(userName);
    }

    /**
     * 新增密钥列表
     * 
     * @param navUserKey 密钥列表
     * @return 结果
     */
    @Override
    public int insertNavUserKey(NavUserKey navUserKey)
    {
        navUserKey.setCreateBy(SecurityUtils.getUsername());
        navUserKey.setCreateTime(DateUtils.getNowDate());
        return navUserKeyMapper.insertNavUserKey(navUserKey);
    }

    /**
     * 修改密钥列表
     * 
     * @param navUserKey 密钥列表
     * @return 结果
     */
    @Override
    public int updateNavUserKey(NavUserKey navUserKey)
    {
        navUserKey.setUpdateBy(SecurityUtils.getUsername());
        navUserKey.setUpdateTime(DateUtils.getNowDate());
        return navUserKeyMapper.updateNavUserKey(navUserKey);
    }

    /**
     * 批量删除密钥列表
     * 
     * @param ids 需要删除的密钥列表主键
     * @return 结果
     */
    @Override
    public int deleteNavUserKeyByIds(Long[] ids)
    {
        return navUserKeyMapper.deleteNavUserKeyByIds(ids);
    }

    /**
     * 删除密钥列表信息
     * 
     * @param id 密钥列表主键
     * @return 结果
     */
    @Override
    public int deleteNavUserKeyById(Long id)
    {
        return navUserKeyMapper.deleteNavUserKeyById(id);
    }
}
