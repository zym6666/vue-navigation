package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.NavWebLabel;
import com.ruoyi.system.mapper.NavWebLabelMapper;
import com.ruoyi.system.service.INavWebLabelService;
import org.springframework.stereotype.Service;

/**
 * 网站标签Service业务层处理
 *
 * @author ZouYangMing
 */
@Service
public class NavWebLabelServiceImpl extends ServiceImpl<NavWebLabelMapper, NavWebLabel> implements INavWebLabelService {

}
