package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.NavApiRequestKey;
import com.ruoyi.system.service.INavApiRequestKeyService;
import com.ruoyi.system.mapper.NavApiRequestKeyMapper;
import org.springframework.stereotype.Service;

/**
* @author ZouYangMing
* @description 针对表【nav_api_request_key】的数据库操作Service实现
* @createDate 2022-10-12 16:05:21
*/
@Service
public class NavApiRequestKeyServiceImpl extends ServiceImpl<NavApiRequestKeyMapper, NavApiRequestKey>
    implements INavApiRequestKeyService {

}




