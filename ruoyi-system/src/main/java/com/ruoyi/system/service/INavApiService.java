package com.ruoyi.system.service;

import com.ruoyi.system.domain.NavApi;

import java.util.List;

/**
 * 接口列表Service
 */
public interface INavApiService {
    /**
     * 查询接口列表
     *
     * @param id 接口列表主键
     * @return 接口列表
     */
    public NavApi selectNavApiById(Long id);

    /**
     * 查询接口列表列表
     *
     * @param navApi 接口列表
     * @return 接口列表
     */
    public List<NavApi> selectNavApiList(NavApi navApi);

    /**
     * 新增接口列表
     *
     * @param navApi 接口列表
     * @return 结果
     */
    public int insertNavApi(NavApi navApi);

    /**
     * 修改接口列表
     *
     * @param navApi 接口列表
     * @return 结果
     */
    public int updateNavApi(NavApi navApi);

    /**
     * 批量删除接口列表
     *
     * @param ids 需要删除的接口列表主键
     * @return 结果
     */
    public int deleteNavApiByIds(Long[] ids);

    /**
     * 删除接口列表信息
     *
     * @param id 接口列表主键
     * @return 结果
     */
    public int deleteNavApiById(Long id);
}
