package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavClassify;
import com.ruoyi.system.mapper.NavClassifyMapper;
import com.ruoyi.system.service.INavClassifyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 分类Service业务层处理
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Service
public class NavClassifyServiceImpl extends ServiceImpl<NavClassifyMapper, NavClassify> implements INavClassifyService
{
    @Autowired
    private NavClassifyMapper navClassifyMapper;

    /**
     * 查询分类
     * 
     * @param id 分类主键
     * @return 分类
     */
    @Override
    public NavClassify selectNavClassifyById(Integer id)
    {
        return navClassifyMapper.selectNavClassifyById(id);
    }

    /**
     * 查询分类列表
     * 
     * @param navClassify 分类
     * @return 分类
     */
    @Override
    public List<NavClassify> selectNavClassifyList(NavClassify navClassify)
    {
        return navClassifyMapper.selectNavClassifyList(navClassify);
    }

    /**
     * 新增分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    @Override
    public int insertNavClassify(NavClassify navClassify)
    {
        navClassify.setCreateBy(SecurityUtils.getUsername());
        navClassify.setCreateTime(DateUtils.getNowDate());
        return navClassifyMapper.insertNavClassify(navClassify);
    }

    /**
     * 修改分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    @Override
    public int updateNavClassify(NavClassify navClassify)
    {
        navClassify.setUpdateBy(SecurityUtils.getUsername());
        navClassify.setUpdateTime(DateUtils.getNowDate());
        return navClassifyMapper.updateNavClassify(navClassify);
    }

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的分类主键
     * @return 结果
     */
    @Override
    public int deleteNavClassifyByIds(Integer[] ids)
    {
        return navClassifyMapper.deleteNavClassifyByIds(ids);
    }

    /**
     * 删除分类信息
     * 
     * @param id 分类主键
     * @return 结果
     */
    @Override
    public int deleteNavClassifyById(Integer id)
    {
        return navClassifyMapper.deleteNavClassifyById(id);
    }
}
