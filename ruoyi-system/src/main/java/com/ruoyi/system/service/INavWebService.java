package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavWeb;

import java.util.List;

/**
 * 网址列表Service接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface INavWebService extends IService<NavWeb>
{
    /**
     * 随机取N条数据
     */
    public List<NavWeb> selectNavWebListByRandom(Integer number);

    /**
     * 查询网址列表
     * 
     * @param id 网址列表主键
     * @return 网址列表
     */
    public NavWeb selectNavWebById(Long id);

    /**
     * 查询非审核网址列表
     *
     * @return 网址列表
     */
    public List<NavWeb> selectNoAuditNavWebList(NavWeb navWeb);

    /**
     * 筛选非审核网址列表
     *
     * @return 网址列表
     */
    public List<NavWeb> selectSortNoAuditNavWebList(NavWeb navWeb);

    /**
     * 查询审核网址列表
     *
     * @return 网址列表
     */
    public List<NavWeb> selectAuditNavWebList(NavWeb navWeb);

    /**
     * 查询网址列表列表
     * 
     * @param navWeb 网址列表
     * @return 网址列表集合
     */
    public List<NavWeb> selectNavWebList(NavWeb navWeb);

    /**
     * 新增网址列表
     * 
     * @param navWeb 网址列表
     * @return 结果
     */
    public int insertNavWeb(NavWeb navWeb);

    /**
     * 修改网址列表
     * 
     * @param navWeb 网址列表
     * @return 结果
     */
    public int updateNavWeb(NavWeb navWeb);

    /**
     * 批量删除网址列表
     * 
     * @param ids 需要删除的网址列表主键集合
     * @return 结果
     */
    public int deleteNavWebByIds(Long[] ids);

    /**
     * 删除网址列表信息
     * 
     * @param id 网址列表主键
     * @return 结果
     */
    public int deleteNavWebById(Long id);
}
