package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavLabel;
import com.ruoyi.system.mapper.NavLabelMapper;
import com.ruoyi.system.service.INavLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 标签列表Service业务层处理
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Service
public class NavLabelServiceImpl extends ServiceImpl<NavLabelMapper, NavLabel> implements INavLabelService
{
    @Autowired
    private NavLabelMapper navLabelMapper;

    /**
     * 查询标签列表
     * 
     * @param id 标签列表主键
     * @return 标签列表
     */
    @Override
    public NavLabel selectNavLabelById(Long id)
    {
        return navLabelMapper.selectNavLabelById(id);
    }

    /**
     * 查询标签列表
     * 
     * @param navLabel 标签列表
     * @return 标签列表
     */
    @Override
    public List<NavLabel> selectNavLabelList(NavLabel navLabel)
    {
        return navLabelMapper.selectNavLabelList(navLabel);
    }

    /**
     * 新增标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    @Override
    public int insertNavLabel(NavLabel navLabel)
    {
        navLabel.setCreateBy(SecurityUtils.getUsername());
        navLabel.setCreateTime(DateUtils.getNowDate());
        return navLabelMapper.insertNavLabel(navLabel);
    }

    /**
     * 修改标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    @Override
    public int updateNavLabel(NavLabel navLabel)
    {
        navLabel.setUpdateBy(SecurityUtils.getUsername());
        navLabel.setUpdateTime(DateUtils.getNowDate());
        return navLabelMapper.updateNavLabel(navLabel);
    }

    /**
     * 批量删除标签列表
     * 
     * @param ids 需要删除的标签列表主键
     * @return 结果
     */
    @Override
    public int deleteNavLabelByIds(Long[] ids)
    {
        return navLabelMapper.deleteNavLabelByIds(ids);
    }

    /**
     * 删除标签列表信息
     * 
     * @param id 标签列表主键
     * @return 结果
     */
    @Override
    public int deleteNavLabelById(Long id)
    {
        return navLabelMapper.deleteNavLabelById(id);
    }

}
