package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.ip.AddressUtils;
import com.ruoyi.common.utils.ip.IpUtils;
import com.ruoyi.system.domain.NavComments;
import com.ruoyi.system.mapper.NavCommentsMapper;
import com.ruoyi.system.service.INavCommentsService;
import com.ruoyi.system.service.ISysUserService;
import eu.bitwalker.useragentutils.UserAgent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 评论列表Service业务层处理
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Service
public class NavCommentsServiceImpl extends ServiceImpl<NavCommentsMapper, NavComments> implements INavCommentsService
{
    @Autowired
    private NavCommentsMapper navCommentsMapper;

    @Autowired
    private ISysUserService iSysUserService;

    /**
     * 查询评论列表
     * 
     * @param id 评论列表主键
     * @return 评论列表
     */
    @Override
    public NavComments selectNavCommentsById(Integer id)
    {
        NavComments comments = navCommentsMapper.selectById(id);
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_name", comments.getCreateBy());
        SysUser one = iSysUserService.getOne(queryWrapper);
        comments.setCommentsEmail(one.getEmail());
        comments.setCommentsAvatar(one.getAvatar());
        return comments;
    }

    /**
     * 查询评论列表列表
     * 
     * @param navComments 评论列表
     * @return 评论列表
     */
    @Override
    public List<NavComments> selectNavCommentsList(NavComments navComments)
    {
        List<NavComments> navCommentsList = navCommentsMapper.selectNavCommentsList(navComments);
        for (NavComments comments : navCommentsList) {
            QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_name", comments.getCreateBy());
            SysUser one = iSysUserService.getOne(queryWrapper);
            comments.setUserId(one.getUserId());
            comments.setCommentsNickName(one.getNickName());
            comments.setCommentsEmail(one.getEmail());
            comments.setCommentsAvatar(one.getAvatar());
        }
        return navCommentsList;
    }

    /**
     * 前台查询评论列表
     *
     * @param navComments 评论列表
     * @return 评论列表集合
     */
    @Override
    public List<NavComments> selectPidNavCommentsList(NavComments navComments) {
        List<NavComments> navCommentsList = navCommentsMapper.selectPidNavCommentsList(navComments);
        for (NavComments comments : navCommentsList) {
            QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_name", comments.getCreateBy());
            SysUser one = iSysUserService.getOne(queryWrapper);
            comments.setUserId(one.getUserId());
            comments.setCommentsNickName(one.getNickName());
            comments.setCommentsEmail(one.getEmail());
            comments.setCommentsAvatar(one.getAvatar());
            if(comments.getBtUsername() != null){
                SysUser user = iSysUserService.selectUserByUserName(comments.getBtUsername());
                if (user != null) {
                    comments.setBtUserId(Math.toIntExact(user.getUserId()));
                }
            }
        }
        return navCommentsList;
    }

    /**
     * 新增评论列表
     * 
     * @param navComments 评论列表
     * @return 结果
     */
    @Override
    public int insertNavComments(NavComments navComments)
    {
        navComments.setCommentsAvatar(SecurityUtils.getLoginUser().getUser().getAvatar());
        navComments.setCommentsEmail(SecurityUtils.getLoginUser().getUser().getEmail());
        navComments.setCommentsIp(IpUtils.getIpAddr(ServletUtils.getRequest()));
        navComments.setCommentsIpadderss(AddressUtils.getRealAddressByIP(IpUtils.getIpAddr(ServletUtils.getRequest())));
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        // 获取客户端操作系统
        String os = userAgent.getOperatingSystem().getName();
        // 获取客户端浏览器
        String browser = userAgent.getBrowser().getName();
        navComments.setCommentsSystem(os);
        navComments.setCommentsBrowser(browser);
        navComments.setCreateBy(SecurityUtils.getUsername());
        navComments.setCreateTime(DateUtils.getNowDate());
        return navCommentsMapper.insertNavComments(navComments);
    }

    /**
     * 修改评论列表
     * 
     * @param navComments 评论列表
     * @return 结果
     */
    @Override
    public int updateNavComments(NavComments navComments)
    {
        navComments.setUpdateBy(SecurityUtils.getUsername());
        navComments.setUpdateTime(DateUtils.getNowDate());
        return navCommentsMapper.updateNavComments(navComments);
    }

    /**
     * 批量删除评论列表
     * 
     * @param ids 需要删除的评论列表主键
     * @return 结果
     */
    @Override
    public int deleteNavCommentsByIds(Integer[] ids)
    {
        for (Integer id : ids) {
            NavComments navComments = navCommentsMapper.selectById(id);
            if (navComments != null && navComments.getBtUsername() == null) {
                NavComments comments = new NavComments();
                comments.setCommentsId(id);
                selectNavCommentsList(comments).forEach(item -> {
                    navCommentsMapper.deleteById(item.getId());
                });
            }
        }
        return navCommentsMapper.deleteNavCommentsByIds(ids);
    }

    /**
     * 删除评论列表信息
     * 
     * @param id 评论列表主键
     * @return 结果
     */
    @Override
    public int deleteNavCommentsById(Integer id)
    {
        NavComments navComments = navCommentsMapper.selectById(id);
        if (navComments != null && navComments.getBtUsername() == null) {
            NavComments comments = new NavComments();
            comments.setCommentsId(id);
            selectNavCommentsList(comments).forEach(item -> {
                navCommentsMapper.deleteById(item.getId());
            });
        }
        return navCommentsMapper.deleteNavCommentsById(id);
    }
}
