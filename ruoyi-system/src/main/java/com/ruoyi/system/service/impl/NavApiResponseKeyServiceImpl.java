package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.system.domain.NavApiResponseKey;
import com.ruoyi.system.service.INavApiResponseKeyService;
import com.ruoyi.system.mapper.NavApiResponseKeyMapper;
import org.springframework.stereotype.Service;

/**
* @author ZouYangMing
* @description 针对表【nav_api_response_key】的数据库操作Service实现
* @createDate 2022-10-12 16:13:48
*/
@Service
public class NavApiResponseKeyServiceImpl extends ServiceImpl<NavApiResponseKeyMapper, NavApiResponseKey>
    implements INavApiResponseKeyService {

}




