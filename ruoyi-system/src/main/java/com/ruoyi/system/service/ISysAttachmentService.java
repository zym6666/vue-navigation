package com.ruoyi.system.service;

import com.ruoyi.system.domain.SysAttachment;

import java.util.List;

/**
 * 附件管理 服务层
 */
public interface ISysAttachmentService {

    /**
     * 获取附件列表
     *
     * @return 附件列表
     */
    public List<SysAttachment> getFileList(SysAttachment sysAttachment);

    /**
     * 查询附件列表
     *
     * @return 附件列表
     */
    public List<SysAttachment> queryFileList(SysAttachment sysAttachment);

    /**
     * 批量删除附件
     *
     * @param fileList 附件列表
     */
    public void deleteFileList(String[] fileList);

    /**
     * 删除附件
     *
     * @param filePath 附件路径
     */
    public void deleteFile(String filePath);
}
