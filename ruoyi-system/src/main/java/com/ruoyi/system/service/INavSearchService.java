package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavSearch;

import java.util.List;

/**
 * 搜索Service接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface INavSearchService extends IService<NavSearch>
{
    /**
     * 查询搜索
     * 
     * @param id 搜索列表主键
     * @return 搜索列表
     */
    public NavSearch selectNavSearchById(Integer id);

    /**
     * 查询搜索列表
     * 
     * @param navSearch 搜索列表
     * @return 搜索列表集合
     */
    public List<NavSearch> selectNavSearchList(NavSearch navSearch);

    /**
     * 获取搜索列表
     *
     * @return 搜索列表
     */
    public List<NavSearch> getNavSearchList();

    /**
     * 新增搜索
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    public int insertNavSearch(NavSearch navSearch);

    /**
     * 修改搜索
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    public int updateNavSearch(NavSearch navSearch);

    /**
     * 批量删除搜索
     * 
     * @param ids 需要删除的搜索列表主键集合
     * @return 结果
     */
    public int deleteNavSearchByIds(Integer[] ids);

    /**
     * 删除搜索
     * 
     * @param id 搜索列表主键
     * @return 结果
     */
    public int deleteNavSearchById(Integer id);
}
