package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavWebCollect;

/**
 * 收藏Service接口
 *
 * @author ZouYangMing
 */
public interface INavWebCollectService extends IService<NavWebCollect> {

}
