package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavSearch;
import com.ruoyi.system.mapper.NavSearchMapper;
import com.ruoyi.system.service.INavSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.ruoyi.common.constant.CacheConstants.NAV_SEARCH_KEY;

/**
 * 搜索Service业务层处理
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Service
public class NavSearchServiceImpl extends ServiceImpl<NavSearchMapper, NavSearch> implements INavSearchService
{
    @Autowired
    private NavSearchMapper navSearchMapper;

    @Autowired
    private RedisCache redisCache;

    /**
     * 查询搜索
     * 
     * @param id 搜索列表主键
     * @return 搜索列表
     */
    @Override
    public NavSearch selectNavSearchById(Integer id)
    {
        return navSearchMapper.selectNavSearchById(id);
    }

    /**
     * 查询搜索列表
     * 
     * @param navSearch 搜索列表
     * @return 搜索列表
     */
    @Override
    public List<NavSearch> selectNavSearchList(NavSearch navSearch)
    {
        return navSearchMapper.selectNavSearchList(navSearch);
    }

    /**
     * 获取搜索列表
     *
     * @return 搜索列表
     */
    @Override
    public List<NavSearch> getNavSearchList()
    {
        List<NavSearch> searchList = redisCache.getCacheObject(NAV_SEARCH_KEY + "list");
        if(searchList != null){
            return searchList;
        }
        searchList = navSearchMapper.selectNavSearchList(new NavSearch());
        redisCache.setCacheObject(NAV_SEARCH_KEY + "list", searchList);
        return searchList;
    }

    /**
     * 新增搜索
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    @Override
    public int insertNavSearch(NavSearch navSearch)
    {
        navSearch.setCreateBy(SecurityUtils.getUsername());
        navSearch.setCreateTime(DateUtils.getNowDate());
        int i = navSearchMapper.insertNavSearch(navSearch);
        clearCache();
        return i;
    }

    /**
     * 修改搜索
     * 
     * @param navSearch 搜索列表
     * @return 结果
     */
    @Override
    public int updateNavSearch(NavSearch navSearch)
    {
        navSearch.setUpdateBy(SecurityUtils.getUsername());
        navSearch.setUpdateTime(DateUtils.getNowDate());
        int i = navSearchMapper.updateNavSearch(navSearch);
        clearCache();
        return i;
    }

    /**
     * 批量删除搜索
     * 
     * @param ids 需要删除的搜索列表主键
     * @return 结果
     */
    @Override
    public int deleteNavSearchByIds(Integer[] ids)
    {
        int i = navSearchMapper.deleteNavSearchByIds(ids);
        clearCache();
        return i;
    }

    /**
     * 删除搜索
     * 
     * @param id 搜索列表主键
     * @return 结果
     */
    @Override
    public int deleteNavSearchById(Integer id)
    {
        int i = navSearchMapper.deleteNavSearchById(id);
        clearCache();
        return i;
    }

    /**
     * 更新搜索列表缓存
     */
    public void clearCache()
    {
        redisCache.deleteObject(NAV_SEARCH_KEY + "list");
        redisCache.setCacheObject(NAV_SEARCH_KEY + "list", navSearchMapper.selectNavSearchList(new NavSearch()));
    }
}
