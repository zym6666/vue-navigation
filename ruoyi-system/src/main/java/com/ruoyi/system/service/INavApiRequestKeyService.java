package com.ruoyi.system.service;

import com.ruoyi.system.domain.NavApiRequestKey;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ZouYangMing
* @description 针对表【nav_api_request_key】的数据库操作Service
* @createDate 2022-10-12 16:05:21
*/
public interface INavApiRequestKeyService extends IService<NavApiRequestKey> {

}
