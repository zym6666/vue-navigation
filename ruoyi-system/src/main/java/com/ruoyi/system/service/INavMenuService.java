package com.ruoyi.system.service;

import com.ruoyi.system.domain.NavMenu;

import java.util.List;

/**
 * 菜单列表Service接口
 * 
 * @author ZouYangMing
 * @date 2022-09-14
 */
public interface INavMenuService 
{
    /**
     * 查询菜单列表
     * 
     * @param id 菜单列表主键
     * @return 菜单列表
     */
    public NavMenu selectNavMenuById(Long id);

    /**
     * 查询菜单列表列表
     * 
     * @param navMenu 菜单列表
     * @return 菜单列表集合
     */
    public List<NavMenu> selectNavMenuList(NavMenu navMenu);

    /**
     * 前台查询菜单列表
     *
     * @return 菜单列表集合
     */
    public List<NavMenu> selectNavMenuNoLimitList();

    /**
     * 新增菜单列表
     * 
     * @param navMenu 菜单列表
     * @return 结果
     */
    public int insertNavMenu(NavMenu navMenu);

    /**
     * 修改菜单列表
     * 
     * @param navMenu 菜单列表
     * @return 结果
     */
    public int updateNavMenu(NavMenu navMenu);

    /**
     * 批量删除菜单列表
     * 
     * @param ids 需要删除的菜单列表主键集合
     * @return 结果
     */
    public int deleteNavMenuByIds(Long[] ids);

    /**
     * 删除菜单列表信息
     * 
     * @param id 菜单列表主键
     * @return 结果
     */
    public int deleteNavMenuById(Long id);
}
