package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavClassify;

import java.util.List;

/**
 * 分类Service接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface INavClassifyService extends IService<NavClassify>
{
    /**
     * 查询分类
     * 
     * @param id 分类主键
     * @return 分类
     */
    public NavClassify selectNavClassifyById(Integer id);

    /**
     * 查询分类列表
     * 
     * @param navClassify 分类
     * @return 分类集合
     */
    public List<NavClassify> selectNavClassifyList(NavClassify navClassify);

    /**
     * 新增分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    public int insertNavClassify(NavClassify navClassify);

    /**
     * 修改分类
     * 
     * @param navClassify 分类
     * @return 结果
     */
    public int updateNavClassify(NavClassify navClassify);

    /**
     * 批量删除分类
     * 
     * @param ids 需要删除的分类主键集合
     * @return 结果
     */
    public int deleteNavClassifyByIds(Integer[] ids);

    /**
     * 删除分类信息
     * 
     * @param id 分类主键
     * @return 结果
     */
    public int deleteNavClassifyById(Integer id);
}
