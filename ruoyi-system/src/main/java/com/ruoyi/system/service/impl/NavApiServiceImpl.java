package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.system.domain.NavApi;
import com.ruoyi.system.domain.NavApiRequestKey;
import com.ruoyi.system.domain.NavApiResponseKey;
import com.ruoyi.system.mapper.NavApiMapper;
import com.ruoyi.system.mapper.NavApiRequestKeyMapper;
import com.ruoyi.system.mapper.NavApiResponseKeyMapper;
import com.ruoyi.system.service.INavApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 接口列表Service业务层处理
 * 
 * @author ZouYangMing
 * @date 2022-10-10
 */
@Service
public class NavApiServiceImpl implements INavApiService 
{
    @Autowired
    private NavApiMapper navApiMapper;

    @Autowired
    private NavApiRequestKeyMapper navApiRequestKeyMapper;

    @Autowired
    private NavApiResponseKeyMapper navApiResponseKeyMapper;

    /**
     * 查询接口列表
     * 
     * @param id 接口列表主键
     * @return 接口列表
     */
    @Override
    public NavApi selectNavApiById(Long id)
    {
        NavApi navApi = navApiMapper.selectNavApiById(id);
        QueryWrapper<NavApiRequestKey> requestKeyQueryWrapper = new QueryWrapper<>();
        requestKeyQueryWrapper.eq("api_id", id);
        navApi.setApiRequestKeyList(navApiRequestKeyMapper.selectList(requestKeyQueryWrapper));
        QueryWrapper<NavApiResponseKey> responseKeyQueryWrapper = new QueryWrapper<>();
        responseKeyQueryWrapper.eq("api_id", id);
        navApi.setApiResponseKeyList(navApiResponseKeyMapper.selectList(responseKeyQueryWrapper));
        return navApi;
    }

    /**
     * 查询接口列表列表
     * 
     * @param navApi 接口列表
     * @return 接口列表
     */
    @Override
    public List<NavApi> selectNavApiList(NavApi navApi)
    {
        List<NavApi> navApiList = navApiMapper.selectNavApiList(navApi);
        for (NavApi api : navApiList) {
            QueryWrapper<NavApiRequestKey> requestKeyQueryWrapper = new QueryWrapper<>();
            requestKeyQueryWrapper.eq("api_id", api.getId());
            api.setApiRequestKeyList(navApiRequestKeyMapper.selectList(requestKeyQueryWrapper));
            QueryWrapper<NavApiResponseKey> responseKeyQueryWrapper = new QueryWrapper<>();
            requestKeyQueryWrapper.eq("api_id", api.getId());
            api.setApiResponseKeyList(navApiResponseKeyMapper.selectList(responseKeyQueryWrapper));
        }
        return navApiList;
    }

    /**
     * 新增接口列表
     * 
     * @param navApi 接口列表
     * @return 结果
     */
    @Override
    public int insertNavApi(NavApi navApi)
    {
        navApi.setCreateBy(SecurityUtils.getUsername());
        navApi.setCreateTime(DateUtils.getNowDate());
        int result = navApiMapper.insertNavApi(navApi);
        //新增对应接口的参数
        for (NavApiRequestKey navApiRequestKey : navApi.getApiRequestKeyList()) {
            //请求参数
            navApiRequestKey.setApiId(Math.toIntExact(navApi.getId()));
            navApiRequestKeyMapper.insert(navApiRequestKey);
        }
        for (NavApiResponseKey navApiResponseKey : navApi.getApiResponseKeyList()) {
            //响应参数
            navApiResponseKey.setApiId(Math.toIntExact(navApi.getId()));
            navApiResponseKeyMapper.insert(navApiResponseKey);
        }
        return result;
    }

    /**
     * 修改接口列表
     * 
     * @param navApi 接口列表
     * @return 结果
     */
    @Override
    public int updateNavApi(NavApi navApi)
    {
        navApi.setUpdateBy(SecurityUtils.getUsername());
        navApi.setUpdateTime(DateUtils.getNowDate());
        //删除原有的参数
        QueryWrapper<NavApiRequestKey> requestKeyQueryWrapper = new QueryWrapper<>();
        requestKeyQueryWrapper.eq("api_id", navApi.getId());
        navApiRequestKeyMapper.delete(requestKeyQueryWrapper);
        QueryWrapper<NavApiResponseKey> responseKeyQueryWrapper = new QueryWrapper<>();
        responseKeyQueryWrapper.eq("api_id", navApi.getId());
        navApiResponseKeyMapper.delete(responseKeyQueryWrapper);
        //新增对应接口的参数
        for (NavApiRequestKey navApiRequestKey : navApi.getApiRequestKeyList()) {
            //请求参数
            navApiRequestKey.setId(null);
            navApiRequestKey.setApiId(Math.toIntExact(navApi.getId()));
            navApiRequestKeyMapper.insert(navApiRequestKey);
        }
        for (NavApiResponseKey navApiResponseKey : navApi.getApiResponseKeyList()) {
            //响应参数
            navApiResponseKey.setId(null);
            navApiResponseKey.setApiId(Math.toIntExact(navApi.getId()));
            navApiResponseKeyMapper.insert(navApiResponseKey);
        }
        return navApiMapper.updateNavApi(navApi);
    }

    /**
     * 批量删除接口列表
     * 
     * @param ids 需要删除的接口列表主键
     * @return 结果
     */
    @Override
    public int deleteNavApiByIds(Long[] ids)
    {
        for (Long id : ids) {
            //删除参数
            QueryWrapper<NavApiRequestKey> requestKeyQueryWrapper = new QueryWrapper<>();
            requestKeyQueryWrapper.eq("api_id", id);
            navApiRequestKeyMapper.delete(requestKeyQueryWrapper);
            QueryWrapper<NavApiResponseKey> responseKeyQueryWrapper = new QueryWrapper<>();
            responseKeyQueryWrapper.eq("api_id", id);
            navApiResponseKeyMapper.delete(responseKeyQueryWrapper);
        }
        return navApiMapper.deleteNavApiByIds(ids);
    }

    /**
     * 删除接口列表信息
     * 
     * @param id 接口列表主键
     * @return 结果
     */
    @Override
    public int deleteNavApiById(Long id)
    {
        //删除参数
        QueryWrapper<NavApiRequestKey> requestKeyQueryWrapper = new QueryWrapper<>();
        requestKeyQueryWrapper.eq("api_id", id);
        navApiRequestKeyMapper.delete(requestKeyQueryWrapper);
        QueryWrapper<NavApiResponseKey> responseKeyQueryWrapper = new QueryWrapper<>();
        responseKeyQueryWrapper.eq("api_id", id);
        navApiResponseKeyMapper.delete(responseKeyQueryWrapper);
        return navApiMapper.deleteNavApiById(id);
    }
}
