package com.ruoyi.system.service;

import com.ruoyi.system.domain.NavApiResponseKey;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author ZouYangMing
* @description 针对表【nav_api_response_key】的数据库操作Service
* @createDate 2022-10-12 16:13:48
*/
public interface INavApiResponseKeyService extends IService<NavApiResponseKey> {

}
