package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavComments;

import java.util.List;

/**
 * 评论列表Service接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface INavCommentsService extends IService<NavComments>
{
    /**
     * 查询评论列表
     * 
     * @param id 评论列表主键
     * @return 评论列表
     */
    public NavComments selectNavCommentsById(Integer id);

    /**
     * 查询评论列表列表
     * 
     * @param navComments 评论列表
     * @return 评论列表集合
     */
    public List<NavComments> selectNavCommentsList(NavComments navComments);

    /**
     * 前台查询评论列表
     *
     * @param navComments 评论列表
     * @return 评论列表集合
     */
    public List<NavComments> selectPidNavCommentsList(NavComments navComments);

    /**
     * 新增评论列表
     * 
     * @param navComments 评论列表
     * @return 结果
     */
    public int insertNavComments(NavComments navComments);

    /**
     * 修改评论列表
     * 
     * @param navComments 评论列表
     * @return 结果
     */
    public int updateNavComments(NavComments navComments);

    /**
     * 批量删除评论列表
     * 
     * @param ids 需要删除的评论列表主键集合
     * @return 结果
     */
    public int deleteNavCommentsByIds(Integer[] ids);

    /**
     * 删除评论列表信息
     * 
     * @param id 评论列表主键
     * @return 结果
     */
    public int deleteNavCommentsById(Integer id);
}
