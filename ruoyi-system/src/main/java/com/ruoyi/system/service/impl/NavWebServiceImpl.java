package com.ruoyi.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.NavWebMapper;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.ruoyi.common.constant.CacheConstants.NAV_WEB_KEY;
import static com.ruoyi.common.constant.Constants.REDIS_CACHE_TIME;

/**
 * 网址列表Service业务层处理
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@Service
public class NavWebServiceImpl extends ServiceImpl<NavWebMapper, NavWeb> implements INavWebService
{
    @Autowired
    private NavWebMapper navWebMapper;

    @Autowired
    private INavClassifyService iNavClassifyService;

    @Autowired
    private INavWebLabelService iNavWebLabelService;

    @Autowired
    private INavLabelService iNavLabelService;

    @Autowired
    private INavWebCollectService iNavWebCollectService;

    @Autowired
    private INavCommentsService iNavCommentsService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 随机取N条数据
     *
     * @param number 数量
     */
    @Override
    public List<NavWeb> selectNavWebListByRandom(Integer number) {
        return navWebMapper.selectNavWebListByRandom(number);
    }

    /**
     * 查询网址信息
     * 
     * @param id 网址列表主键
     * @return 网址列表
     */
    @Override
    public NavWeb selectNavWebById(Long id)
    {
        // 如果redis中有缓存
        NavWeb navWeb = redisCache.getCacheObject(NAV_WEB_KEY + id);
        if(navWeb != null){
            return navWeb;
        }
        // 判断命中是否为空值
        if(redisCache.exists(NAV_WEB_KEY + id)){
            return null;
        }
        // 否则查询数据库
        navWeb = navWebMapper.selectNavWebById(id);
        if(navWeb == null){
            // 解决缓存穿透 将空值写入redis
            redisCache.setCacheObject(NAV_WEB_KEY + id, null, REDIS_CACHE_TIME , TimeUnit.MINUTES);
            return null;
        }
        // 取得分类名称
        QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
        classifyQueryWrapper.eq("id", navWeb.getClassifyId());
        navWeb.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
        // 取得标签名称
        QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
        webLabelQueryWrapper.eq("web_id", navWeb.getId());
        List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
        List<String> labelNameList = new ArrayList<>();
        for (NavWebLabel navWebLabel : labelList) {
            QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
            labelQueryWrapper.eq("id", navWebLabel.getLabelId());
            labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
        }
        navWeb.setLabelList(labelNameList);
        // 将数据存入redis
        redisCache.setCacheObject(NAV_WEB_KEY + id, navWeb);
        return navWeb;
    }

    /**
     * 查询非审核网址列表
     *
     * @return 网址列表
     */
    @Override
    public List<NavWeb> selectNoAuditNavWebList(NavWeb navWeb) {
        List<NavWeb> navWebList = navWebMapper.selectNoAuditNavWebList(navWeb);
        for (NavWeb web : navWebList) {
            //遍历取得分类名称
            QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
            classifyQueryWrapper.eq("id", web.getClassifyId());
            web.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", web.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            web.setLabelList(labelNameList);
        }
        return navWebList;
    }

    /**
     * 筛选非审核网址列表
     *
     * @param navWeb 网址信息
     * @return 网址列表
     */
    @Override
    public List<NavWeb> selectSortNoAuditNavWebList(NavWeb navWeb) {
        List<NavWeb> navWebList = navWebMapper.selectSortNoAuditNavWebList(navWeb);
        for (NavWeb web : navWebList) {
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.select("label_id");
            webLabelQueryWrapper.eq("web_id", web.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.select("label_name");
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            web.setLabelList(labelNameList);
        }
        return navWebList;
    }

    /**
     * 查询审核网址列表
     *
     * @return 网址列表
     */
    @Override
    public List<NavWeb> selectAuditNavWebList(NavWeb navWeb) {
        List<NavWeb> navWebList = navWebMapper.selectAuditNavWebList(navWeb);
        for (NavWeb web : navWebList) {
            //遍历取得分类名称
            QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
            classifyQueryWrapper.eq("id", web.getClassifyId());
            web.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", web.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            web.setLabelList(labelNameList);
        }
        return navWebList;
    }

    /**
     * 查询网址列表列表
     * 
     * @param navWeb 网址列表
     * @return 网址列表
     */
    @Override
    public List<NavWeb> selectNavWebList(NavWeb navWeb)
    {
        List<NavWeb> navWebList = navWebMapper.selectNavWebList(navWeb);
        for (NavWeb web : navWebList) {
            //遍历取得分类名称
            QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
            classifyQueryWrapper.eq("id", web.getClassifyId());
            web.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", web.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            web.setLabelList(labelNameList);
        }
        return navWebList;
    }

    /**
     * 新增网址列表
     * 
     * @param navWeb 网址列表
     * @return 结果
     */
    @Override
    public int insertNavWeb(NavWeb navWeb)
    {
        navWeb.setCreateBy(SecurityUtils.getUsername());
        navWeb.setCreateTime(DateUtils.getNowDate());
        int rows = navWebMapper.insertNavWeb(navWeb);
        if(rows > 0){
            //处理标签
            List<String> labelList = navWeb.getLabelList();
            for (String s : labelList) {
                //去除空格
                s = s.replace(" ","");
                if(!StringUtils.isEmpty(s)){
                    //判断标签是否存在
                    QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                    labelQueryWrapper.eq("label_name", s);
                    NavLabel navLabel = iNavLabelService.getOne(labelQueryWrapper);
                    NavWebLabel navWebLabel1 = new NavWebLabel();
                    if(navLabel == null){
                        //检测到标签不存在，新增标签
                        NavLabel label = new NavLabel();
                        label.setLabelName(s);
                        label.setCreateBy(SecurityUtils.getUsername());
                        label.setCreateTime(DateUtils.getNowDate());
                        iNavLabelService.save(label);
                        //把现有的标签和链接关联
                        navWebLabel1.setLabelId(label.getId());
                    }else{
                        //把现有的标签和链接关联
                        navWebLabel1.setLabelId(navLabel.getId());
                    }
                    navWebLabel1.setWebId(navWeb.getId());
                    navWebLabel1.setCreateBy(SecurityUtils.getUsername());
                    navWebLabel1.setCreateTime(DateUtils.getNowDate());
                    iNavWebLabelService.save(navWebLabel1);
                }
            }
        }
        // 缓存数据
        redisCache.deleteObject(NAV_WEB_KEY + navWeb.getId());
        redisCache.setCacheObject(NAV_WEB_KEY + navWeb.getId(), navWeb);
        return rows;
    }

    /**
     * 修改网址列表
     * 
     * @param navWeb 网址列表
     * @return 结果
     */
    @Override
    public int updateNavWeb(NavWeb navWeb)
    {
        navWeb.setUpdateBy(SecurityUtils.getUsername());
        navWeb.setUpdateTime(DateUtils.getNowDate());
        //修改链接关联标签
        if (StringUtils.isNotEmpty(navWeb.getLabelList())) {
            NavWebLabel navWebLabel = new NavWebLabel();
            navWebLabel.setWebId(navWeb.getId());
            //先把原来的标签全删除
            List<String> labelList1 = navWeb.getLabelList();
            for (String s : labelList1) {
                QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
                webLabelQueryWrapper.eq("web_id", navWeb.getId());
                iNavWebLabelService.remove(webLabelQueryWrapper);
            }
            List<String> labelList = navWeb.getLabelList();
            for (String s : labelList) {
                //去除空格
                s = s.replace(" ","");
                if(!StringUtils.isEmpty(s)){
                    //判断标签是否存在
                    QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                    labelQueryWrapper.eq("label_name", s);
                    NavLabel navLabel = iNavLabelService.getOne(labelQueryWrapper);
                    NavWebLabel navWebLabel1 = new NavWebLabel();
                    if(navLabel == null){
                        //检测到标签不存在，新增标签
                        NavLabel label = new NavLabel();
                        label.setLabelName(s);
                        label.setCreateBy(SecurityUtils.getUsername());
                        label.setCreateTime(DateUtils.getNowDate());
                        iNavLabelService.save(label);
                        //把现有的标签和链接关联
                        navWebLabel1.setLabelId(label.getId());
                    }else{
                        //把现有的标签和链接关联
                        navWebLabel1.setLabelId(navLabel.getId());
                    }
                    navWebLabel1.setWebId(navWeb.getId());
                    navWebLabel1.setCreateBy(SecurityUtils.getUsername());
                    navWebLabel1.setCreateTime(DateUtils.getNowDate());
                    iNavWebLabelService.save(navWebLabel1);
                }
            }
        }
        // 缓存数据
        redisCache.deleteObject(NAV_WEB_KEY + navWeb.getId());
        redisCache.setCacheObject(NAV_WEB_KEY + navWeb.getId(), navWeb);
        return navWebMapper.updateNavWeb(navWeb);
    }

    /**
     * 批量删除网址列表
     * 
     * @param ids 需要删除的网址列表主键
     * @return 结果
     */
    @Override
    public int deleteNavWebByIds(Long[] ids)
    {
        int rows =  navWebMapper.deleteNavWebByIds(ids);
        for (Long id : ids) {
            //删除链接关联标签
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", id);
            iNavWebLabelService.remove(webLabelQueryWrapper);
            //删除此链接有关的收藏
            QueryWrapper<NavWebCollect> collectionQueryWrapper = new QueryWrapper<>();
            collectionQueryWrapper.eq("web_id", id);
            iNavWebCollectService.remove(collectionQueryWrapper);
            //删除此链接有关的评论
            QueryWrapper<NavComments> commentsQueryWrapper = new QueryWrapper<>();
            commentsQueryWrapper.eq("web_id", id);
            iNavCommentsService.remove(commentsQueryWrapper);
            // 删除缓存数据
            redisCache.deleteObject(NAV_WEB_KEY + id);
        }
        return rows;
    }

    /**
     * 删除网址列表信息
     * 
     * @param id 网址列表主键
     * @return 结果
     */
    @Override
    public int deleteNavWebById(Long id)
    {
        //删除链接关联标签
        QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
        webLabelQueryWrapper.eq("web_id", id);
        iNavWebLabelService.remove(webLabelQueryWrapper);
        //删除此链接有关的收藏
        QueryWrapper<NavWebCollect> collectionQueryWrapper = new QueryWrapper<>();
        collectionQueryWrapper.eq("web_id", id);
        iNavWebCollectService.remove(collectionQueryWrapper);
        //删除此链接有关的评论
        QueryWrapper<NavComments> commentsQueryWrapper = new QueryWrapper<>();
        commentsQueryWrapper.eq("web_id", id);
        iNavCommentsService.remove(commentsQueryWrapper);
        int rows = navWebMapper.deleteNavWebById(id);
        // 删除缓存数据
        redisCache.deleteObject(NAV_WEB_KEY + id);
        return rows;
    }
}
