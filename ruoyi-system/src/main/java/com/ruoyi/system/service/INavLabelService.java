package com.ruoyi.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.ruoyi.system.domain.NavLabel;

import java.util.List;

/**
 * 标签列表Service接口
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
public interface INavLabelService extends IService<NavLabel>
{
    /**
     * 查询标签列表
     * 
     * @param id 标签列表主键
     * @return 标签列表
     */
    public NavLabel selectNavLabelById(Long id);

    /**
     * 查询标签列表列表
     * 
     * @param navLabel 标签列表
     * @return 标签列表集合
     */
    public List<NavLabel> selectNavLabelList(NavLabel navLabel);

    /**
     * 新增标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    public int insertNavLabel(NavLabel navLabel);

    /**
     * 修改标签列表
     * 
     * @param navLabel 标签列表
     * @return 结果
     */
    public int updateNavLabel(NavLabel navLabel);

    /**
     * 批量删除标签列表
     * 
     * @param ids 需要删除的标签列表主键集合
     * @return 结果
     */
    public int deleteNavLabelByIds(Long[] ids);

    /**
     * 删除标签列表信息
     * 
     * @param id 标签列表主键
     * @return 结果
     */
    public int deleteNavLabelById(Long id);
}
