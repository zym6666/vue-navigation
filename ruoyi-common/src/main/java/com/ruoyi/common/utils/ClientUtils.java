package com.ruoyi.common.utils;

import eu.bitwalker.useragentutils.UserAgent;

/**
 * 获取客户端信息类
 *
 * @author ZouYangMing
 */
public class ClientUtils {

    /**
     * 获取客户端浏览器
     *
     * @return 客户端浏览器
     */
    public static String getBrowser(){
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        return userAgent.getBrowser().getName();
    }

    /**
     * 获取客户端操作系统
     *
     * @return 客户端操作系统
     */
    public static String getOs(){
        UserAgent userAgent = UserAgent.parseUserAgentString(ServletUtils.getRequest().getHeader("User-Agent"));
        return userAgent.getOperatingSystem().getName();
    }

}
