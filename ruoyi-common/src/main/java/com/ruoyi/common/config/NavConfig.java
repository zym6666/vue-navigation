package com.ruoyi.common.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 读取项目相关配置
 *
 * @author ZouYangMing
 */
@Component
@ConfigurationProperties(prefix = "navigation")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NavConfig {

    /** 项目名称 */
    private String name;

    /** 版本 */
    private String version;

    /** 版权年份 */
    private String copyrightYear;

}
