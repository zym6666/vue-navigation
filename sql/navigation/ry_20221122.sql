-- ----------------------------
-- 1、部门表
-- ----------------------------
drop table if exists sys_dept;
create table sys_dept (
  dept_id           bigint(20)      not null auto_increment    comment '部门id',
  parent_id         bigint(20)      default 0                  comment '父部门id',
  ancestors         varchar(50)     default ''                 comment '祖级列表',
  dept_name         varchar(30)     default ''                 comment '部门名称',
  order_num         int(4)          default 0                  comment '显示顺序',
  leader            varchar(20)     default null               comment '负责人',
  phone             varchar(11)     default null               comment '联系电话',
  email             varchar(50)     default null               comment '邮箱',
  status            char(1)         default '0'                comment '部门状态（0正常 1停用）',
  del_flag          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  primary key (dept_id)
) engine=innodb auto_increment=200 comment = '部门表';

-- ----------------------------
-- 初始化-部门表数据
-- ----------------------------
insert into sys_dept values(100,  0,   '0',          '导航程序',    0, '',    '', '', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(101,  100, '0,100',      '研发部门',    1, '',    '', '', '0', '0', 'admin', sysdate(), '', null);
insert into sys_dept values(102,  100, '0,100',      '普通用户',    2, '',    '', '', '0', '0', 'admin', sysdate(), '', null);


-- ----------------------------
-- 2、用户信息表
-- ----------------------------
drop table if exists sys_user;
create table sys_user (
  user_id           bigint(20)      not null auto_increment    comment '用户ID',
  dept_id           bigint(20)      default null               comment '部门ID',
  user_name         varchar(30)     not null                   comment '用户账号',
  nick_name         varchar(30)     not null                   comment '用户昵称',
  user_type         varchar(2)      default '00'               comment '用户类型（00系统用户）',
  email             varchar(50)     default ''                 comment '用户邮箱',
  phonenumber       varchar(11)     default ''                 comment '手机号码',
  sex               char(1)         default '0'                comment '用户性别（0男 1女 2未知）',
  avatar            varchar(100)    default ''                 comment '头像地址',
  password          varchar(100)    default ''                 comment '密码',
  status            char(1)         default '0'                comment '帐号状态（0正常 1停用）',
  del_flag          char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  login_ip          varchar(128)    default ''                 comment '最后登录IP',
  login_date        datetime                                   comment '最后登录时间',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (user_id)
) engine=innodb auto_increment=100 comment = '用户信息表';

-- ----------------------------
-- 初始化-用户信息表数据
-- ----------------------------
insert into sys_user values(1,  103, 'admin', '明明', '00', 'zouyangming2022@qq.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', sysdate(), 'admin', sysdate(), '', null, '管理员');


-- ----------------------------
-- 3、岗位信息表
-- ----------------------------
drop table if exists sys_post;
create table sys_post
(
  post_id       bigint(20)      not null auto_increment    comment '岗位ID',
  post_code     varchar(64)     not null                   comment '岗位编码',
  post_name     varchar(50)     not null                   comment '岗位名称',
  post_sort     int(4)          not null                   comment '显示顺序',
  status        char(1)         not null                   comment '状态（0正常 1停用）',
  create_by     varchar(64)     default ''                 comment '创建者',
  create_time   datetime                                   comment '创建时间',
  update_by     varchar(64)     default ''			       comment '更新者',
  update_time   datetime                                   comment '更新时间',
  remark        varchar(500)    default null               comment '备注',
  primary key (post_id)
) engine=innodb comment = '岗位信息表';

-- ----------------------------
-- 初始化-岗位信息表数据
-- ----------------------------
insert into sys_post values(1, 	'ceo',	 '超级管理员', 	1, 		'0', 		'admin', 		sysdate(), 		'admin', 		null, '');
insert into sys_post values(2, 	'user',	 '普通用户', 	2, 	    '0', 		'admin', 		sysdate(), 		'admin', 		null, '');


-- ----------------------------
-- 4、角色信息表
-- ----------------------------
drop table if exists sys_role;
create table sys_role (
  role_id              bigint(20)      not null auto_increment    comment '角色ID',
  role_name            varchar(30)     not null                   comment '角色名称',
  role_key             varchar(100)    not null                   comment '角色权限字符串',
  role_sort            int(4)          not null                   comment '显示顺序',
  data_scope           char(1)         default '1'                comment '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  menu_check_strictly  tinyint(1)      default 1                  comment '菜单树选择项是否关联显示',
  dept_check_strictly  tinyint(1)      default 1                  comment '部门树选择项是否关联显示',
  status               char(1)         not null                   comment '角色状态（0正常 1停用）',
  del_flag             char(1)         default '0'                comment '删除标志（0代表存在 2代表删除）',
  create_by            varchar(64)     default ''                 comment '创建者',
  create_time          datetime                                   comment '创建时间',
  update_by            varchar(64)     default ''                 comment '更新者',
  update_time          datetime                                   comment '更新时间',
  remark               varchar(500)    default null               comment '备注',
  primary key (role_id)
) engine=innodb auto_increment=100 comment = '角色信息表';

-- ----------------------------
-- 初始化-角色信息表数据
-- ----------------------------
insert into sys_role values('1', '超级管理员',  'admin',  1, 1, 1, 1, '0', '0', 'admin', sysdate(), '', null, '超级管理员');
insert into sys_role values('2', '管理员',     'op',     2, 2, 1, 1, '0', '0', 'admin', sysdate(), '', null, '管理员');
insert into sys_role values('3', '普通用户',    'user',  3, 1, 1, 1, '0', '0', 'admin', sysdate(), '', null, '普通角色');


-- ----------------------------
-- 5、菜单权限表
-- ----------------------------
drop table if exists sys_menu;
create table sys_menu (
  menu_id           bigint(20)      not null auto_increment    comment '菜单ID',
  menu_name         varchar(50)     not null                   comment '菜单名称',
  parent_id         bigint(20)      default 0                  comment '父菜单ID',
  order_num         int(4)          default 0                  comment '显示顺序',
  path              varchar(200)    default ''                 comment '路由地址',
  component         varchar(255)    default null               comment '组件路径',
  query             varchar(255)    default null               comment '路由参数',
  is_frame          int(1)          default 1                  comment '是否为外链（0是 1否）',
  is_cache          int(1)          default 0                  comment '是否缓存（0缓存 1不缓存）',
  menu_type         char(1)         default ''                 comment '菜单类型（M目录 C菜单 F按钮）',
  visible           char(1)         default 0                  comment '菜单状态（0显示 1隐藏）',
  status            char(1)         default 0                  comment '菜单状态（0正常 1停用）',
  perms             varchar(100)    default null               comment '权限标识',
  icon              varchar(100)    default '#'                comment '菜单图标',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default ''                 comment '备注',
  primary key (menu_id)
) engine=innodb auto_increment=2000 comment = '菜单权限表';

-- ----------------------------
-- 初始化-菜单信息表数据
-- ----------------------------
-- 一级菜单
insert into sys_menu values('1', '系统管理', '0', '1', 'system',           null, '', 1, 0, 'M', '0', '0', '', 'system',   'admin', sysdate(), '', null, '系统管理目录');
insert into sys_menu values('2', '系统监控', '0', '2', 'monitor',          null, '', 1, 0, 'M', '0', '0', '', 'monitor',  'admin', sysdate(), '', null, '系统监控目录');
insert into sys_menu values('3', '系统工具', '0', '3', 'tool',             null, '', 1, 0, 'M', '0', '0', '', 'tool',     'admin', sysdate(), '', null, '系统工具目录');
-- 二级菜单
insert into sys_menu values('100',  '用户管理', '1',   '1', 'user',       'system/user/index',        '', 1, 0, 'C', '0', '0', 'system:user:list',        'user',          'admin', sysdate(), '', null, '用户管理菜单');
insert into sys_menu values('101',  '角色管理', '1',   '2', 'role',       'system/role/index',        '', 1, 0, 'C', '0', '0', 'system:role:list',        'peoples',       'admin', sysdate(), '', null, '角色管理菜单');
insert into sys_menu values('102',  '菜单管理', '1',   '3', 'menu',       'system/menu/index',        '', 1, 0, 'C', '0', '0', 'system:menu:list',        'tree-table',    'admin', sysdate(), '', null, '菜单管理菜单');
insert into sys_menu values('103',  '部门管理', '1',   '4', 'dept',       'system/dept/index',        '', 1, 0, 'C', '0', '0', 'system:dept:list',        'tree',          'admin', sysdate(), '', null, '部门管理菜单');
insert into sys_menu values('104',  '岗位管理', '1',   '5', 'post',       'system/post/index',        '', 1, 0, 'C', '0', '0', 'system:post:list',        'post',          'admin', sysdate(), '', null, '岗位管理菜单');
insert into sys_menu values('105',  '字典管理', '1',   '6', 'dict',       'system/dict/index',        '', 1, 0, 'C', '0', '0', 'system:dict:list',        'dict',          'admin', sysdate(), '', null, '字典管理菜单');
insert into sys_menu values('106',  '参数设置', '1',   '7', 'config',     'system/config/index',      '', 1, 0, 'C', '0', '0', 'system:config:list',      'edit',          'admin', sysdate(), '', null, '参数设置菜单');
insert into sys_menu values('107',  '通知公告', '1',   '8', 'notice',     'system/notice/index',      '', 1, 0, 'C', '0', '0', 'system:notice:list',      'message',       'admin', sysdate(), '', null, '通知公告菜单');
insert into sys_menu values('108',  '日志管理', '1',   '9', 'log',        '',                         '', 1, 0, 'M', '0', '0', '',                        'log',           'admin', sysdate(), '', null, '日志管理菜单');
insert into sys_menu values('109',  '在线用户', '2',   '1', 'online',     'monitor/online/index',     '', 1, 0, 'C', '0', '0', 'monitor:online:list',     'online',        'admin', sysdate(), '', null, '在线用户菜单');
insert into sys_menu values('110',  '定时任务', '2',   '2', 'job',        'monitor/job/index',        '', 1, 0, 'C', '0', '0', 'monitor:job:list',        'job',           'admin', sysdate(), '', null, '定时任务菜单');
insert into sys_menu values('111',  '数据监控', '2',   '3', 'druid',      'monitor/druid/index',      '', 1, 0, 'C', '0', '0', 'monitor:druid:list',      'druid',         'admin', sysdate(), '', null, '数据监控菜单');
insert into sys_menu values('112',  '服务监控', '2',   '4', 'server',     'monitor/server/index',     '', 1, 0, 'C', '0', '0', 'monitor:server:list',     'server',        'admin', sysdate(), '', null, '服务监控菜单');
insert into sys_menu values('113',  '缓存监控', '2',   '5', 'cache',      'monitor/cache/index',      '', 1, 0, 'C', '0', '0', 'monitor:cache:list',      'redis',         'admin', sysdate(), '', null, '缓存监控菜单');
insert into sys_menu values('114',  '缓存列表', '2',   '6', 'cacheList',  'monitor/cache/list',       '', 1, 0, 'C', '0', '0', 'monitor:cache:list',      'redis-list',    'admin', sysdate(), '', null, '缓存列表菜单');
insert into sys_menu values('115',  '表单构建', '3',   '1', 'build',      'tool/build/index',         '', 1, 0, 'C', '0', '0', 'tool:build:list',         'build',         'admin', sysdate(), '', null, '表单构建菜单');
insert into sys_menu values('116',  '代码生成', '3',   '2', 'gen',        'tool/gen/index',           '', 1, 0, 'C', '0', '0', 'tool:gen:list',           'code',          'admin', sysdate(), '', null, '代码生成菜单');
insert into sys_menu values('117',  '系统接口', '3',   '3', 'swagger',    'tool/swagger/index',       '', 1, 0, 'C', '0', '0', 'tool:swagger:list',       'swagger',       'admin', sysdate(), '', null, '系统接口菜单');
-- 三级菜单
insert into sys_menu values('500',  '操作日志', '108', '1', 'operlog',    'monitor/operlog/index',    '', 1, 0, 'C', '0', '0', 'monitor:operlog:list',    'form',          'admin', sysdate(), '', null, '操作日志菜单');
insert into sys_menu values('501',  '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', '', 1, 0, 'C', '0', '0', 'monitor:logininfor:list', 'logininfor',    'admin', sysdate(), '', null, '登录日志菜单');
-- 用户管理按钮
insert into sys_menu values('1000', '用户查询', '100', '1',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1001', '用户新增', '100', '2',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1002', '用户修改', '100', '3',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1003', '用户删除', '100', '4',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:remove',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1004', '用户导出', '100', '5',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:export',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1005', '用户导入', '100', '6',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:import',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1006', '重置密码', '100', '7',  '', '', '', 1, 0, 'F', '0', '0', 'system:user:resetPwd',       '#', 'admin', sysdate(), '', null, '');
-- 角色管理按钮
insert into sys_menu values('1007', '角色查询', '101', '1',  '', '', '', 1, 0, 'F', '0', '0', 'system:role:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1008', '角色新增', '101', '2',  '', '', '', 1, 0, 'F', '0', '0', 'system:role:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1009', '角色修改', '101', '3',  '', '', '', 1, 0, 'F', '0', '0', 'system:role:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1010', '角色删除', '101', '4',  '', '', '', 1, 0, 'F', '0', '0', 'system:role:remove',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1011', '角色导出', '101', '5',  '', '', '', 1, 0, 'F', '0', '0', 'system:role:export',         '#', 'admin', sysdate(), '', null, '');
-- 菜单管理按钮
insert into sys_menu values('1012', '菜单查询', '102', '1',  '', '', '', 1, 0, 'F', '0', '0', 'system:menu:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1013', '菜单新增', '102', '2',  '', '', '', 1, 0, 'F', '0', '0', 'system:menu:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1014', '菜单修改', '102', '3',  '', '', '', 1, 0, 'F', '0', '0', 'system:menu:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1015', '菜单删除', '102', '4',  '', '', '', 1, 0, 'F', '0', '0', 'system:menu:remove',         '#', 'admin', sysdate(), '', null, '');
-- 部门管理按钮
insert into sys_menu values('1016', '部门查询', '103', '1',  '', '', '', 1, 0, 'F', '0', '0', 'system:dept:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1017', '部门新增', '103', '2',  '', '', '', 1, 0, 'F', '0', '0', 'system:dept:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1018', '部门修改', '103', '3',  '', '', '', 1, 0, 'F', '0', '0', 'system:dept:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1019', '部门删除', '103', '4',  '', '', '', 1, 0, 'F', '0', '0', 'system:dept:remove',         '#', 'admin', sysdate(), '', null, '');
-- 岗位管理按钮
insert into sys_menu values('1020', '岗位查询', '104', '1',  '', '', '', 1, 0, 'F', '0', '0', 'system:post:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1021', '岗位新增', '104', '2',  '', '', '', 1, 0, 'F', '0', '0', 'system:post:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1022', '岗位修改', '104', '3',  '', '', '', 1, 0, 'F', '0', '0', 'system:post:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1023', '岗位删除', '104', '4',  '', '', '', 1, 0, 'F', '0', '0', 'system:post:remove',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1024', '岗位导出', '104', '5',  '', '', '', 1, 0, 'F', '0', '0', 'system:post:export',         '#', 'admin', sysdate(), '', null, '');
-- 字典管理按钮
insert into sys_menu values('1025', '字典查询', '105', '1', '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1026', '字典新增', '105', '2', '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1027', '字典修改', '105', '3', '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1028', '字典删除', '105', '4', '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:remove',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1029', '字典导出', '105', '5', '#', '', '', 1, 0, 'F', '0', '0', 'system:dict:export',         '#', 'admin', sysdate(), '', null, '');
-- 参数设置按钮
insert into sys_menu values('1030', '参数查询', '106', '1', '#', '', '', 1, 0, 'F', '0', '0', 'system:config:query',        '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1031', '参数新增', '106', '2', '#', '', '', 1, 0, 'F', '0', '0', 'system:config:add',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1032', '参数修改', '106', '3', '#', '', '', 1, 0, 'F', '0', '0', 'system:config:edit',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1033', '参数删除', '106', '4', '#', '', '', 1, 0, 'F', '0', '0', 'system:config:remove',       '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1034', '参数导出', '106', '5', '#', '', '', 1, 0, 'F', '0', '0', 'system:config:export',       '#', 'admin', sysdate(), '', null, '');
-- 通知公告按钮
insert into sys_menu values('1035', '公告查询', '107', '1', '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:query',        '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1036', '公告新增', '107', '2', '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:add',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1037', '公告修改', '107', '3', '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:edit',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1038', '公告删除', '107', '4', '#', '', '', 1, 0, 'F', '0', '0', 'system:notice:remove',       '#', 'admin', sysdate(), '', null, '');
-- 操作日志按钮
insert into sys_menu values('1039', '操作查询', '500', '1', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:query',      '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1040', '操作删除', '500', '2', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:remove',     '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1041', '日志导出', '500', '3', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:operlog:export',     '#', 'admin', sysdate(), '', null, '');
-- 登录日志按钮
insert into sys_menu values('1042', '登录查询', '501', '1', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:query',   '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1043', '登录删除', '501', '2', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:remove',  '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1044', '日志导出', '501', '3', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:export',  '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1045', '账户解锁', '501', '4', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:logininfor:unlock',  '#', 'admin', sysdate(), '', null, '');
-- 在线用户按钮
insert into sys_menu values('1046', '在线查询', '109', '1', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:query',       '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1047', '批量强退', '109', '2', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1048', '单条强退', '109', '3', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', sysdate(), '', null, '');
-- 定时任务按钮
insert into sys_menu values('1049', '任务查询', '110', '1', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:query',          '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1050', '任务新增', '110', '2', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:add',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1051', '任务修改', '110', '3', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:edit',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1052', '任务删除', '110', '4', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:remove',         '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1053', '状态修改', '110', '5', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:changeStatus',   '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1054', '任务导出', '110', '6', '#', '', '', 1, 0, 'F', '0', '0', 'monitor:job:export',         '#', 'admin', sysdate(), '', null, '');
-- 代码生成按钮
insert into sys_menu values('1055', '生成查询', '116', '1', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:query',             '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1056', '生成修改', '116', '2', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:edit',              '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1057', '生成删除', '116', '3', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:remove',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1058', '导入代码', '116', '4', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:import',            '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1059', '预览代码', '116', '5', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:preview',           '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values('1060', '生成代码', '116', '6', '#', '', '', 1, 0, 'F', '0', '0', 'tool:gen:code',              '#', 'admin', sysdate(), '', null, '');
-- 父菜单
insert into sys_menu values(1061, '网址管理', 0, 10, 'linksmanage', null, null, 1, 0, 'M', '0', '0', '', 'international', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1062, '搜索管理', 0, 20, 'searchmanage', null, null, 1, 0, 'M', '0', '0', null, 'search', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1063, '评论管理', 0, 30, 'commentsmanage', null, null, 1, 0, 'M', '0', '0', null, 'message', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1064, '标签管理', 0, 40, 'labelmanage', null, null, 1, 0, 'M', '0', '0', '', 'guide', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1089, '分类管理', 0, 5, 'classifymanage', null, null, 1, 0, 'M', '0', '0', '', 'list', 'admin', sysdate(), '', null, '');
-- 标签列表菜单
insert into sys_menu values(1097, '标签列表', 1064, 1, 'label', 'navigation/label/index', null, 1, 0, 'C', '0', '0', 'navigation:label:list', '#', 'admin', sysdate(), '', null, '标签列表菜单');
insert into sys_menu values(1098, '标签列表查询', 1097, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:label:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1099, '标签列表新增', 1097, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:label:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1100, '标签列表修改', 1097, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:label:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1101, '标签列表删除', 1097, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:label:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1102, '标签列表导出', 1097, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:label:export', '#', 'admin', sysdate(), '', null, '');
-- 搜索列表菜单
insert into sys_menu values(1109, '搜索列表', 1062, 1, 'search', 'navigation/search/index', null, 1, 0, 'C', '0', '0', 'navigation:search:list', '#', 'admin', sysdate(), '', null, '搜索列表菜单');
insert into sys_menu values(1110, '搜索列表查询', 1109, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:search:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1111, '搜索列表新增', 1109, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:search:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1112, '搜索列表修改', 1109, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:search:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1113, '搜索列表删除', 1109, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:search:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1114, '搜索列表导出', 1109, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:search:export', '#', 'admin', sysdate(), '', null, '');
-- 分类菜单
insert into sys_menu values(1115, '分类列表', 1089, 1, 'classify', 'navigation/classify/index', null, 1, 0, 'C', '0', '0', 'navigation:classify:list', '#', 'admin', sysdate(), '', null, '分类菜单');
insert into sys_menu values(1116, '分类查询', 1115, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:classify:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1117, '分类新增', 1115, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:classify:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1118, '分类修改', 1115, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:classify:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1119, '分类删除', 1115, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:classify:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1120, '分类导出', 1115, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:classify:export', '#', 'admin', sysdate(), '', null, '');
-- 评论列表菜单
insert into sys_menu values(1121, '评论列表', 1063, 1, 'comments', 'navigation/comments/index', null, 1, 0, 'C', '0', '0', 'navigation:comments:list', '#', 'admin', sysdate(), '', null, '评论列表菜单');
insert into sys_menu values(1122, '评论列表查询', 1121, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:comments:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1123, '评论列表新增', 1121, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:comments:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1124, '评论列表修改', 1121, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:comments:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1125, '评论列表删除', 1121, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:comments:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1126, '评论列表导出', 1121, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:comments:export', '#', 'admin', sysdate(), '', null, '');
-- 网址列表菜单
insert into sys_menu values(1127, '网址列表', 1061, 1, 'web', 'navigation/web/index', null, 1, 0, 'C', '0', '0', 'navigation:web:list', '#', 'admin', sysdate(), '', null, '网址列表菜单');
insert into sys_menu values(1128, '网址列表查询', 1127, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:web:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1129, '网址列表新增', 1127, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:web:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1130, '网址列表修改', 1127, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:web:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1131, '网址列表删除', 1127, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:web:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1132, '网址列表导出', 1127, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:web:export', '#', 'admin', sysdate(), '', null, '');
-- 审核列表菜单
insert into sys_menu values(1133, '审核列表', 1061, 1, 'audit_web', 'navigation/audit_web/index', null, 1, 0, 'C', '0', '0', 'navigation:audit_web:list', '#', 'admin', sysdate(), '', null, '审核列表菜单');
insert into sys_menu values(1134, '审核列表查询', 1133, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:audit_web:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1135, '审核列表新增', 1133, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:audit_web:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1136, '审核列表修改', 1133, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:audit_web:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1137, '审核列表删除', 1133, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:audit_web:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1138, '审核列表导出', 1133, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:audit_web:export', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1139, '菜单管理', 0, 4, 'menumanage', null, null, 1, 0, 'M', '0', '0', null, 'tree-table', 'admin', sysdate(), '', null, '');
-- 菜单列表菜单
insert into sys_menu values(1140, '菜单列表', 1139, 1, 'menu', 'navigation/menu/index', null, 1, 0, 'C', '0', '0', 'navigation:menu:list', '#', 'admin', sysdate(), '', null, '菜单列表菜单');
insert into sys_menu values(1141, '菜单列表查询', 1140, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:menu:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1142, '菜单列表新增', 1140, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:menu:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1143, '菜单列表修改', 1140, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:menu:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1144, '菜单列表删除', 1140, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:menu:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1145, '菜单列表导出', 1140, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:menu:export', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1146, '接口管理', 0, 50, 'apimanage', null, null, 1, 0, 'M', '0', '0', null, 'tree', 'admin', sysdate(), '', null, '');
-- 接口列表菜单
insert into sys_menu values(1147, '接口列表', 1146, 1, 'api', 'navigation/api/index', null, 1, 0, 'C', '0', '0', 'navigation:api:list', '#', 'admin', sysdate(), '', null, '接口列表菜单');
insert into sys_menu values(1148, '接口列表查询', 1147, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:api:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1149, '接口列表新增', 1147, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:api:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1150, '接口列表修改', 1147, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:api:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1151, '接口列表删除', 1147, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:api:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1152, '接口列表导出', 1147, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:api:export', '#', 'admin', sysdate(), '', null, '');
-- 密钥列表菜单
insert into sys_menu values(1153, '密钥列表', 1146, 1, 'secretkey', 'navigation/secretkey/index', null, 1, 0, 'C', '0', '0', 'navigation:secretkey:list', '#', 'admin', sysdate(), '', null, '密钥列表菜单');
insert into sys_menu values(1154, '密钥列表查询', 1153, 1, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:secretkey:query', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1155, '密钥列表新增', 1153, 2, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:secretkey:add', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1156, '密钥列表修改', 1153, 3, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:secretkey:edit', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1157, '密钥列表删除', 1153, 4, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:secretkey:remove', '#', 'admin', sysdate(), '', null, '');
insert into sys_menu values(1158, '密钥列表导出', 1153, 5, '#', '', null, 1, 0, 'F', '0', '0', 'navigation:secretkey:export', '#', 'admin', sysdate(), '', null, '');
-- 附件管理
INSERT INTO sys_menu VALUES(1159, '附件管理', 1, 10, 'attachment', 'system/attachment/index', NULL, 1, 0, 'C', '0', '0', 'system:attachment:list', 'clipboard', 'admin', sysdate(), '', null, '');
INSERT INTO sys_menu VALUES(1160, '删除附件', 1159, 1, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:attachment:remove', '#', 'admin', sysdate(), '', null, '');
INSERT INTO sys_menu VALUES(1161, '查询附件', 1159, 2, '', NULL, NULL, 1, 0, 'F', '0', '0', 'system:attachment:query', '#', 'admin', sysdate(), '', NULL, '');



-- ----------------------------
-- 6、用户和角色关联表  用户N-1角色
-- ----------------------------
drop table if exists sys_user_role;
create table sys_user_role (
  user_id   bigint(20) not null comment '用户ID',
  role_id   bigint(20) not null comment '角色ID',
  primary key(user_id, role_id)
) engine=innodb comment = '用户和角色关联表';

-- ----------------------------
-- 初始化-用户和角色关联表数据
-- ----------------------------
insert into sys_user_role values ('1', '1');


-- ----------------------------
-- 7、角色和菜单关联表  角色1-N菜单
-- ----------------------------
drop table if exists sys_role_menu;
create table sys_role_menu (
  role_id   bigint(20) not null comment '角色ID',
  menu_id   bigint(20) not null comment '菜单ID',
  primary key(role_id, menu_id)
) engine=innodb comment = '角色和菜单关联表';

-- ----------------------------
-- 初始化-角色和菜单关联表数据
-- ----------------------------
insert into sys_role_menu values(2, 1061);
insert into sys_role_menu values(2, 1062);
insert into sys_role_menu values(2, 1063);
insert into sys_role_menu values(2, 1064);
insert into sys_role_menu values(2, 1089);
insert into sys_role_menu values(2, 1097);
insert into sys_role_menu values(2, 1098);
insert into sys_role_menu values(2, 1099);
insert into sys_role_menu values(2, 1100);
insert into sys_role_menu values(2, 1101);
insert into sys_role_menu values(2, 1102);
insert into sys_role_menu values(2, 1109);
insert into sys_role_menu values(2, 1110);
insert into sys_role_menu values(2, 1111);
insert into sys_role_menu values(2, 1112);
insert into sys_role_menu values(2, 1113);
insert into sys_role_menu values(2, 1114);
insert into sys_role_menu values(2, 1115);
insert into sys_role_menu values(2, 1116);
insert into sys_role_menu values(2, 1117);
insert into sys_role_menu values(2, 1118);
insert into sys_role_menu values(2, 1119);
insert into sys_role_menu values(2, 1120);
insert into sys_role_menu values(2, 1121);
insert into sys_role_menu values(2, 1122);
insert into sys_role_menu values(2, 1123);
insert into sys_role_menu values(2, 1124);
insert into sys_role_menu values(2, 1125);
insert into sys_role_menu values(2, 1126);
insert into sys_role_menu values(2, 1127);
insert into sys_role_menu values(2, 1128);
insert into sys_role_menu values(2, 1129);
insert into sys_role_menu values(2, 1130);
insert into sys_role_menu values(2, 1131);
insert into sys_role_menu values(2, 1132);
insert into sys_role_menu values(2, 1133);
insert into sys_role_menu values(2, 1134);
insert into sys_role_menu values(2, 1135);
insert into sys_role_menu values(2, 1136);
insert into sys_role_menu values(2, 1137);
insert into sys_role_menu values(2, 1138);

-- ----------------------------
-- 8、角色和部门关联表  角色1-N部门
-- ----------------------------
drop table if exists sys_role_dept;
create table sys_role_dept (
  role_id   bigint(20) not null comment '角色ID',
  dept_id   bigint(20) not null comment '部门ID',
  primary key(role_id, dept_id)
) engine=innodb comment = '角色和部门关联表';

-- ----------------------------
-- 初始化-角色和部门关联表数据
-- ----------------------------



-- ----------------------------
-- 9、用户与岗位关联表  用户1-N岗位
-- ----------------------------
drop table if exists sys_user_post;
create table sys_user_post
(
  user_id   bigint(20) not null comment '用户ID',
  post_id   bigint(20) not null comment '岗位ID',
  primary key (user_id, post_id)
) engine=innodb comment = '用户与岗位关联表';

-- ----------------------------
-- 初始化-用户与岗位关联表数据
-- ----------------------------
insert into sys_user_post values ('1', '1');


-- ----------------------------
-- 10、操作日志记录
-- ----------------------------
drop table if exists sys_oper_log;
create table sys_oper_log (
  oper_id           bigint(20)      not null auto_increment    comment '日志主键',
  title             varchar(50)     default ''                 comment '模块标题',
  business_type     int(2)          default 0                  comment '业务类型（0其它 1新增 2修改 3删除）',
  method            varchar(100)    default ''                 comment '方法名称',
  request_method    varchar(10)     default ''                 comment '请求方式',
  operator_type     int(1)          default 0                  comment '操作类别（0其它 1后台用户 2手机端用户）',
  oper_name         varchar(50)     default ''                 comment '操作人员',
  dept_name         varchar(50)     default ''                 comment '部门名称',
  oper_url          varchar(255)    default ''                 comment '请求URL',
  oper_ip           varchar(128)    default ''                 comment '主机地址',
  oper_location     varchar(255)    default ''                 comment '操作地点',
  oper_param        varchar(2000)   default ''                 comment '请求参数',
  json_result       varchar(2000)   default ''                 comment '返回参数',
  status            int(1)          default 0                  comment '操作状态（0正常 1异常）',
  error_msg         varchar(2000)   default ''                 comment '错误消息',
  oper_time         datetime                                   comment '操作时间',
  primary key (oper_id)
) engine=innodb auto_increment=100 comment = '操作日志记录';


-- ----------------------------
-- 11、字典类型表
-- ----------------------------
drop table if exists sys_dict_type;
create table sys_dict_type
(
  dict_id          bigint(20)      not null auto_increment    comment '字典主键',
  dict_name        varchar(100)    default ''                 comment '字典名称',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  status           char(1)         default '0'                comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64)     default ''                 comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark           varchar(500)    default null               comment '备注',
  primary key (dict_id),
  unique (dict_type)
) engine=innodb auto_increment=100 comment = '字典类型表';

insert into sys_dict_type values(1,  '用户性别',        'sys_user_sex',        '0', 'admin', sysdate(), '', null, '用户性别列表');
insert into sys_dict_type values(2,  '菜单状态',        'sys_show_hide',       '0', 'admin', sysdate(), '', null, '菜单状态列表');
insert into sys_dict_type values(3,  '系统开关',        'sys_normal_disable',  '0', 'admin', sysdate(), '', null, '系统开关列表');
insert into sys_dict_type values(4,  '任务状态',        'sys_job_status',      '0', 'admin', sysdate(), '', null, '任务状态列表');
insert into sys_dict_type values(5,  '任务分组',        'sys_job_group',       '0', 'admin', sysdate(), '', null, '任务分组列表');
insert into sys_dict_type values(6,  '系统是否',        'sys_yes_no',          '0', 'admin', sysdate(), '', null, '系统是否列表');
insert into sys_dict_type values(7,  '通知类型',        'sys_notice_type',     '0', 'admin', sysdate(), '', null, '通知类型列表');
insert into sys_dict_type values(8,  '通知状态',        'sys_notice_status',   '0', 'admin', sysdate(), '', null, '通知状态列表');
insert into sys_dict_type values(9,  '操作类型',        'sys_oper_type',       '0', 'admin', sysdate(), '', null, '操作类型列表');
insert into sys_dict_type values(10, '系统状态',        'sys_common_status',   '0', 'admin', sysdate(), '', null, '登录状态列表');
insert into sys_dict_type values(11, '显示或隐藏',      'nav_is_show',          '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type values(12, '审核状态',        'nav_audit_status',     '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(13, '参数的数据类型',   'sys_config_data_type', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(14, '返回值类型',      'nav_response_type',     '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(15, '请求类型',        'nav_request_type',     '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(16, '数据类型',        'nav_data_type',        '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(17, '接口状态',        'nav_api_status',       '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_type VALUES(18, '密钥状态',        'nav_key_status',       '0', 'admin', sysdate(), '', null, null);
INSERT INTO sys_dict_type VALUES (19, '联系方式', 'nav_contact_information', '0', 'admin', sysdate(), '', NULL, NULL);


-- ----------------------------
-- 12、字典数据表
-- ----------------------------
drop table if exists sys_dict_data;
create table sys_dict_data
(
  dict_code        bigint(20)      not null auto_increment    comment '字典编码',
  dict_sort        int(4)          default 0                  comment '字典排序',
  dict_label       varchar(100)    default ''                 comment '字典标签',
  dict_value       varchar(100)    default ''                 comment '字典键值',
  dict_type        varchar(100)    default ''                 comment '字典类型',
  css_class        varchar(100)    default null               comment '样式属性（其他样式扩展）',
  list_class       varchar(100)    default null               comment '表格回显样式',
  is_default       char(1)         default 'N'                comment '是否默认（Y是 N否）',
  status           char(1)         default '0'                comment '状态（0正常 1停用）',
  create_by        varchar(64)     default ''                 comment '创建者',
  create_time      datetime                                   comment '创建时间',
  update_by        varchar(64)     default ''                 comment '更新者',
  update_time      datetime                                   comment '更新时间',
  remark           varchar(500)    default null               comment '备注',
  primary key (dict_code)
) engine=innodb auto_increment=100 comment = '字典数据表';

insert into sys_dict_data values(1,  1,  '男',       '0',       'sys_user_sex',        '',   '',        'Y', '0', 'admin', sysdate(), '', null, '性别男');
insert into sys_dict_data values(2,  2,  '女',       '1',       'sys_user_sex',        '',   '',        'N', '0', 'admin', sysdate(), '', null, '性别女');
insert into sys_dict_data values(3,  3,  '未知',     '2',       'sys_user_sex',        '',   '',        'N', '0', 'admin', sysdate(), '', null, '性别未知');
insert into sys_dict_data values(4,  1,  '显示',     '0',       'sys_show_hide',       '',   'primary', 'Y', '0', 'admin', sysdate(), '', null, '显示菜单');
insert into sys_dict_data values(5,  2,  '隐藏',     '1',       'sys_show_hide',       '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '隐藏菜单');
insert into sys_dict_data values(6,  1,  '正常',     '0',       'sys_normal_disable',  '',   'primary', 'Y', '0', 'admin', sysdate(), '', null, '正常状态');
insert into sys_dict_data values(7,  2,  '停用',     '1',       'sys_normal_disable',  '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '停用状态');
insert into sys_dict_data values(8,  1,  '正常',     '0',       'sys_job_status',      '',   'primary', 'Y', '0', 'admin', sysdate(), '', null, '正常状态');
insert into sys_dict_data values(9,  2,  '暂停',     '1',       'sys_job_status',      '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '停用状态');
insert into sys_dict_data values(10, 1,  '默认',     'DEFAULT', 'sys_job_group',       '',   '',        'Y', '0', 'admin', sysdate(), '', null, '默认分组');
insert into sys_dict_data values(11, 2,  '系统',     'SYSTEM',  'sys_job_group',       '',   '',        'N', '0', 'admin', sysdate(), '', null, '系统分组');
insert into sys_dict_data values(12, 1,  '是',       'Y',       'sys_yes_no',          '',   'primary', 'Y', '0', 'admin', sysdate(), '', null, '系统默认是');
insert into sys_dict_data values(13, 2,  '否',       'N',       'sys_yes_no',          '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '系统默认否');
insert into sys_dict_data values(14, 1,  '通知',     '1',       'sys_notice_type',     '',   'warning', 'Y', '0', 'admin', sysdate(), '', null, '通知');
insert into sys_dict_data values(15, 2,  '公告',     '2',       'sys_notice_type',     '',   'success', 'N', '0', 'admin', sysdate(), '', null, '公告');
insert into sys_dict_data values(16, 1,  '正常',     '0',       'sys_notice_status',   '',   'primary', 'Y', '0', 'admin', sysdate(), '', null, '正常状态');
insert into sys_dict_data values(17, 2,  '关闭',     '1',       'sys_notice_status',   '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '关闭状态');
insert into sys_dict_data values(18, 99, '其他',     '0',       'sys_oper_type',       '',   'info',    'N', '0', 'admin', sysdate(), '', null, '其他操作');
insert into sys_dict_data values(19, 1,  '新增',     '1',       'sys_oper_type',       '',   'info',    'N', '0', 'admin', sysdate(), '', null, '新增操作');
insert into sys_dict_data values(20, 2,  '修改',     '2',       'sys_oper_type',       '',   'info',    'N', '0', 'admin', sysdate(), '', null, '修改操作');
insert into sys_dict_data values(21, 3,  '删除',     '3',       'sys_oper_type',       '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '删除操作');
insert into sys_dict_data values(22, 4,  '授权',     '4',       'sys_oper_type',       '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '授权操作');
insert into sys_dict_data values(23, 5,  '导出',     '5',       'sys_oper_type',       '',   'warning', 'N', '0', 'admin', sysdate(), '', null, '导出操作');
insert into sys_dict_data values(24, 6,  '导入',     '6',       'sys_oper_type',       '',   'warning', 'N', '0', 'admin', sysdate(), '', null, '导入操作');
insert into sys_dict_data values(25, 7,  '强退',     '7',       'sys_oper_type',       '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '强退操作');
insert into sys_dict_data values(26, 8,  '生成代码', '8',       'sys_oper_type',       '',   'warning', 'N', '0', 'admin', sysdate(), '', null, '生成操作');
insert into sys_dict_data values(27, 9,  '清空数据', '9',       'sys_oper_type',       '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '清空操作');
insert into sys_dict_data values(28, 1,  '成功',     '0',       'sys_common_status',   '',   'primary', 'N', '0', 'admin', sysdate(), '', null, '正常状态');
insert into sys_dict_data values(29, 2,  '失败',     '1',       'sys_common_status',   '',   'danger',  'N', '0', 'admin', sysdate(), '', null, '停用状态');
insert into sys_dict_data values(30, 0,  '显示',     '1',       'nav_is_show',         '', 'success', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(31, 1,  '隐藏',     '2',       'nav_is_show',         '', 'warning', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(32, 0,  '未审核',    '1',      'nav_audit_status',    '', 'warning', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(33, 1, '已审核',    '2',       'nav_audit_status',    '', 'success', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(34, 2, '审核未通过', '3',       'nav_audit_status',    '', 'danger', 'N', '0', 'admin', sysdate(), '',  null, null);
insert into sys_dict_data values(35, 0, '字符串（String）', '1', 'sys_config_data_type', '', 'default', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(36, 1, '列表（List）', '2', 'sys_config_data_type', '', 'default', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(37, 2, '布尔（Boolean）', '3', 'sys_config_data_type', '', 'default', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(38, 3, '数字（int）', '4', 'sys_config_data_type', '', 'default', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(39, 0, 'Json', '1', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values(40, 0, 'Xml', '2', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (41, 0, 'Html', '3', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (42, 0, 'Script', '4', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (43, 0, 'Jsonp', '5', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (44, 0, 'Text', '6', 'nav_response_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (45, 0, 'GET', '1', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (46, 0, 'POST', '2', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (47, 0, 'PUT', '3', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (48, 0, 'DELETE', '4', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (49, 0, 'OPTIONS', '5', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (50, 0, 'HEAD', '6', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (51, 0, 'TRACE', '7', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (52, 0, 'CONNECT', '8', 'nav_request_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (53, 0, 'Text', '1', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (54, 0, 'String', '2', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (55, 0, 'Number', '3', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (56, 0, 'Integer', '4', 'nav_data_type', '', 'primary', 'N', '0', 'admin',sysdate(), '', null, null);
insert into sys_dict_data values (57, 0, 'Float', '5', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (58, 0, 'Double', '6', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (59, 0, 'Date', '7', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (60, 0, 'DateTime', '8', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (61, 0, 'TimeStamp', '9', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (62, 0, 'Boolean', '10', 'nav_data_type', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (63, 0, '正常', '1', 'nav_api_status', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (64, 0, '维护', '2', 'nav_api_status', '', 'warning', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (65, 0, '停用', '3', 'nav_api_status', '', 'danger', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (66, 0, '正常', '1', 'nav_key_status', '', 'primary', 'N', '0', 'admin', sysdate(), '', null, null);
insert into sys_dict_data values (67, 0, '停用', '2', 'nav_key_status', '', 'danger', 'N', '0', 'admin', sysdate(), '', null, null);
INSERT INTO sys_dict_data VALUES (68, 0, 'QQ', '1', 'nav_contact_information', NULL, 'primary', 'N', '0', 'admin', sysdate(), '', NULL, NULL);
INSERT INTO sys_dict_data VALUES (69, 0, '微信', '2', 'nav_contact_information', NULL, 'primary', 'N', '0', 'admin', sysdate(), '', NULL, NULL);
INSERT INTO sys_dict_data VALUES (70, 0, '邮箱', '3', 'nav_contact_information', NULL, 'primary', 'N', '0', 'admin', sysdate(), '', NULL, NULL);


-- ----------------------------
-- 13、参数配置表
-- ----------------------------
drop table if exists sys_config;
create table sys_config (
  config_id         int(5)          not null auto_increment    comment '参数主键',
  config_name       varchar(100)    default ''                 comment '参数名称',
  config_key        varchar(100)    default ''                 comment '参数键名',
  config_value      varchar(500)    default ''                 comment '参数键值',
  config_type       char(1)         default 'N'                comment '系统内置（Y是 N否）',
  config_data_type  char(1)         default '1'                comment '数据类型  1=String 2=List 3=Boolean 4=int',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (config_id)
) engine=innodb auto_increment=100 comment = '参数配置表';

insert into sys_config values(1, '主框架页-默认皮肤样式名称',      'sys.index.skinName',            'skin-blue',     'Y', '1', 'admin', sysdate(), '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow' );
insert into sys_config values(2, '用户管理-账号初始密码',         'sys.user.initPassword',          '123456',       'Y', '1', 'admin', sysdate(), '', null, '初始化密码 123456' );
insert into sys_config values(3, '主框架页-侧边栏主题',           'sys.index.sideTheme',           'theme-dark',    'Y', '1', 'admin', sysdate(), '', null, '深色主题theme-dark，浅色主题theme-light' );
insert into sys_config values(4, '账号自助-验证码开关',           'sys.account.captchaEnabled',    'true',          'Y', '3', 'admin', sysdate(), '', null, '是否开启验证码功能（true开启，false关闭）');
insert into sys_config values(5, '账号自助-是否开启用户注册功能',   'sys.account.registerUser',      'false',         'Y', '3', 'admin', sysdate(), '', null, '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('6', '网站名称', 'web.name', '聚合导航', 'Y', '1', 'admin', sysdate(), '', null, '网站名称');
INSERT INTO `sys_config` VALUES ('7', '网站地址', 'web.url', 'www.zym88.cn', 'Y', '1', 'admin', sysdate(), '', null, '不带http协议的网址');
INSERT INTO `sys_config` VALUES ('8', '网站底部信息', 'web.footer.info', '@2022 聚合导航', 'Y', '1', 'admin', sysdate(), '', null, '用于显示网站底部的信息');
INSERT INTO `sys_config` VALUES ('9', '网站SEO关键词', 'web.seo.keywords', '关键词1,关键词2,关键词3', 'Y', '2', 'admin', sysdate(), '', null, '以|分隔，关键词数量一般控制在3-5个左右');
INSERT INTO `sys_config` VALUES ('10', '网站SEO描述', 'web.seo.description', '这是一个很方便的导航', 'Y', '1', 'admin', sysdate(), '', null, 'SEO描述一般不超过200个字符');
INSERT INTO `sys_config` VALUES ('11', '禁用关键词', 'web.disable.keywords', '艹,尼玛,死了,操你,操她,操他', 'Y', '2', 'admin', sysdate(), '', null, '全局通用禁用词');
INSERT INTO `sys_config` VALUES ('13', '网站SEO标题分割符号', 'nav.seo.separate', ' - ', 'Y', '1', 'admin', sysdate(), '', null, '一般为\"-\"或者\"|\"');
INSERT INTO `sys_config` VALUES ('14', '首页链接显示数量', 'nav.index.link.number', '16', 'Y', '4', 'admin', sysdate(), '', null, NULL);


-- ----------------------------
-- 14、系统访问记录
-- ----------------------------
drop table if exists sys_logininfor;
create table sys_logininfor (
  info_id        bigint(20)     not null auto_increment   comment '访问ID',
  user_name      varchar(50)    default ''                comment '用户账号',
  ipaddr         varchar(128)   default ''                comment '登录IP地址',
  login_location varchar(255)   default ''                comment '登录地点',
  browser        varchar(50)    default ''                comment '浏览器类型',
  os             varchar(50)    default ''                comment '操作系统',
  status         char(1)        default '0'               comment '登录状态（0成功 1失败）',
  msg            varchar(255)   default ''                comment '提示消息',
  login_time     datetime                                 comment '访问时间',
  primary key (info_id)
) engine=innodb auto_increment=100 comment = '系统访问记录';


-- ----------------------------
-- 15、定时任务调度表
-- ----------------------------
drop table if exists sys_job;
create table sys_job (
  job_id              bigint(20)    not null auto_increment    comment '任务ID',
  job_name            varchar(64)   default ''                 comment '任务名称',
  job_group           varchar(64)   default 'DEFAULT'          comment '任务组名',
  invoke_target       varchar(500)  not null                   comment '调用目标字符串',
  cron_expression     varchar(255)  default ''                 comment 'cron执行表达式',
  misfire_policy      varchar(20)   default '3'                comment '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  concurrent          char(1)       default '1'                comment '是否并发执行（0允许 1禁止）',
  status              char(1)       default '0'                comment '状态（0正常 1暂停）',
  create_by           varchar(64)   default ''                 comment '创建者',
  create_time         datetime                                 comment '创建时间',
  update_by           varchar(64)   default ''                 comment '更新者',
  update_time         datetime                                 comment '更新时间',
  remark              varchar(500)  default ''                 comment '备注信息',
  primary key (job_id, job_name, job_group)
) engine=innodb auto_increment=100 comment = '定时任务调度表';

insert into sys_job values(1, '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams',        '0/10 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');
insert into sys_job values(2, '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')',  '0/15 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');
insert into sys_job values(3, '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)',  '0/20 * * * * ?', '3', '1', '1', 'admin', sysdate(), '', null, '');


-- ----------------------------
-- 16、定时任务调度日志表
-- ----------------------------
drop table if exists sys_job_log;
create table sys_job_log (
  job_log_id          bigint(20)     not null auto_increment    comment '任务日志ID',
  job_name            varchar(64)    not null                   comment '任务名称',
  job_group           varchar(64)    not null                   comment '任务组名',
  invoke_target       varchar(500)   not null                   comment '调用目标字符串',
  job_message         varchar(500)                              comment '日志信息',
  status              char(1)        default '0'                comment '执行状态（0正常 1失败）',
  exception_info      varchar(2000)  default ''                 comment '异常信息',
  create_time         datetime                                  comment '创建时间',
  primary key (job_log_id)
) engine=innodb comment = '定时任务调度日志表';


-- ----------------------------
-- 17、通知公告表
-- ----------------------------
drop table if exists sys_notice;
create table sys_notice (
  notice_id         int(4)          not null auto_increment    comment '公告ID',
  notice_title      varchar(50)     not null                   comment '公告标题',
  notice_type       char(1)         not null                   comment '公告类型（1通知 2公告）',
  notice_content    longblob        default null               comment '公告内容',
  status            char(1)         default '0'                comment '公告状态（0正常 1关闭）',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time       datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(255)    default null               comment '备注',
  primary key (notice_id)
) engine=innodb auto_increment=10 comment = '通知公告表';

-- ----------------------------
-- 初始化-公告信息表数据
-- ----------------------------


-- ----------------------------
-- 18、代码生成业务表
-- ----------------------------
drop table if exists gen_table;
create table gen_table (
  table_id          bigint(20)      not null auto_increment    comment '编号',
  table_name        varchar(200)    default ''                 comment '表名称',
  table_comment     varchar(500)    default ''                 comment '表描述',
  sub_table_name    varchar(64)     default null               comment '关联子表的表名',
  sub_table_fk_name varchar(64)     default null               comment '子表关联的外键名',
  class_name        varchar(100)    default ''                 comment '实体类名称',
  tpl_category      varchar(200)    default 'crud'             comment '使用的模板（crud单表操作 tree树表操作）',
  package_name      varchar(100)                               comment '生成包路径',
  module_name       varchar(30)                                comment '生成模块名',
  business_name     varchar(30)                                comment '生成业务名',
  function_name     varchar(50)                                comment '生成功能名',
  function_author   varchar(50)                                comment '生成功能作者',
  gen_type          char(1)         default '0'                comment '生成代码方式（0zip压缩包 1自定义路径）',
  gen_path          varchar(200)    default '/'                comment '生成路径（不填默认项目路径）',
  options           varchar(1000)                              comment '其它生成选项',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  remark            varchar(500)    default null               comment '备注',
  primary key (table_id)
) engine=innodb auto_increment=1 comment = '代码生成业务表';


-- ----------------------------
-- 19、代码生成业务表字段
-- ----------------------------
drop table if exists gen_table_column;
create table gen_table_column (
  column_id         bigint(20)      not null auto_increment    comment '编号',
  table_id          varchar(64)                                comment '归属表编号',
  column_name       varchar(200)                               comment '列名称',
  column_comment    varchar(500)                               comment '列描述',
  column_type       varchar(100)                               comment '列类型',
  java_type         varchar(500)                               comment 'JAVA类型',
  java_field        varchar(200)                               comment 'JAVA字段名',
  is_pk             char(1)                                    comment '是否主键（1是）',
  is_increment      char(1)                                    comment '是否自增（1是）',
  is_required       char(1)                                    comment '是否必填（1是）',
  is_insert         char(1)                                    comment '是否为插入字段（1是）',
  is_edit           char(1)                                    comment '是否编辑字段（1是）',
  is_list           char(1)                                    comment '是否列表字段（1是）',
  is_query          char(1)                                    comment '是否查询字段（1是）',
  query_type        varchar(200)    default 'EQ'               comment '查询方式（等于、不等于、大于、小于、范围）',
  html_type         varchar(200)                               comment '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  dict_type         varchar(200)    default ''                 comment '字典类型',
  sort              int                                        comment '排序',
  create_by         varchar(64)     default ''                 comment '创建者',
  create_time 	    datetime                                   comment '创建时间',
  update_by         varchar(64)     default ''                 comment '更新者',
  update_time       datetime                                   comment '更新时间',
  primary key (column_id)
) engine=innodb auto_increment=1 comment = '代码生成业务表字段';