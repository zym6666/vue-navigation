-- ----------------------------
-- API接口表
-- ----------------------------
drop table if exists `nav_api`;
create table `nav_api`  (
  `id` int(11) not null auto_increment comment 'id',
  `api_name` varchar(30) not null comment '接口名称',
  `api_introduce` longtext not null comment '接口介绍',
  `api_url` varchar(150) not null comment '请求地址',
  `api_request_type` varchar(10) not null comment '请求格式',
  `api_response_type` varchar(10) not null comment '返回格式',
  `api_url_example` varchar(150) not null comment '请求示例',
  `api_response_example` longtext null default null comment '返回示例',
  `api_status` char(2) not null default '1' comment '接口状态 1=>正常;2=>维护;3=>停用',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = 'api管理表';

-- ----------------------------
-- 接口请求值表
-- ----------------------------
drop table if exists `nav_api_request_key`;
create table `nav_api_request_key`  (
  `id` int(11) not null auto_increment comment 'id',
  `api_id` int(11) not null comment '接口id',
  `request_name` varchar(20) not null comment '参数名称',
  `request_required` char(2) not null default 'y' comment '是否必填',
  `request_type` char(2) not null default '1' comment '参数类型 1=>String;2=>List;3=>Boolean;4=>int',
  `request_text` varchar(50) null default null comment '参数说明',
  primary key (`id`) using btree
) engine = innodb comment = '接口请求值表';

-- ----------------------------
-- 接口返回值表
-- ----------------------------
drop table if exists `nav_api_response_key`;
create table `nav_api_response_key`  (
  `id` int(11) not null auto_increment comment 'id',
  `api_id` int(11) not null comment '接口id',
  `response_name` varchar(20) not null comment '参数名称',
  `response_type` char(2) not null default '1' comment '参数类型 1=>Json;2=>Xml;3=>Html;4=>Script;5=>Jsonp;6=>Text',
  `response_text` varchar(50) null default null comment '参数说明',
  primary key (`id`) using btree
) engine = innodb comment = '接口返回值表';

-- ----------------------------
-- 分类表
-- ----------------------------
drop table if exists `nav_classify`;
create table `nav_classify`  (
  `id` int(10) not null auto_increment comment '主键',
  `classify_sort` int(10) not null default 1 comment '排序',
  `classify_name` varchar(20) not null comment '分类名称',
  `classify_status` char(1) not null default '1' comment '状态 1=>显示;2=>隐藏',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '分类表';

-- ----------------------------
-- 评论表
-- ----------------------------
drop table if exists `nav_comments`;
create table `nav_comments`  (
  `id` int(10) not null auto_increment comment '主键',
  `b_username` varchar(64) null default null comment '被回复者',
  `comments_id` int(10) null default null comment '上级评论id',
  `web_id` int(10) not null comment '网站id',
  `comments_text` longtext null default null comment '评论内容',
  `comments_ip` varchar(15) not null comment '评论ip地址',
  `comments_ipadderss` varchar(20) not null comment '评论ip归属地',
  `comments_browser` varchar(20) not null comment '评论浏览器',
  `comments_system` varchar(20) not null comment '评论系统',
  `comments_status` char(2) not null default '1' comment '评论状态 1=>显示;2=>隐藏',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '评论表';

-- ----------------------------
-- 标签表
-- ----------------------------
drop table if exists `nav_label`;
create table `nav_label`  (
  `id` int(11) not null auto_increment comment '主键',
  `label_name` varchar(20) not null comment '标签名称',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '标签表';

-- ----------------------------
-- 菜单表
-- ----------------------------
drop table if exists `nav_menu`;
create table `nav_menu`  (
  `id` int(11) not null auto_increment comment '主键',
  `menu_name` varchar(20) not null comment '菜单名称',
  `menu_url` varchar(150) character set latin1 collate latin1_swedish_ci not null comment '路由地址',
  `menu_status` char(1) not null default '1' comment '状态 1=>显示;2=>隐藏',
  `menu_type` char(1) character set latin1 collate latin1_swedish_ci not null comment '系统内置',
  `menu_sort` int(10) not null default 1 comment '排序',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb auto_increment=100 comment = '菜单表';

INSERT INTO `nav_menu` VALUES (1, '网站分类', '/classify', '1', 'Y', 900, 'admin', sysdate(), '', null);
INSERT INTO `nav_menu` VALUES (2, '首页', '/', '1', 'Y', 1000, 'admin', sysdate(), '', null);
INSERT INTO `nav_menu` VALUES (3, '标签页', '/tag', '1', 'Y', 800, 'admin', sysdate(), '', NULL);
INSERT INTO `nav_menu` VALUES (4, '网站提交', '/push', '1', 'Y', 700, 'admin', sysdate(), '', NULL);
INSERT INTO `nav_menu` VALUES (5, 'API接口', '/interface', '1', 'Y', 600, 'admin', sysdate(), '', null);

-- ----------------------------
-- 搜索引擎表
-- ----------------------------
drop table if exists `nav_search`;
create table `nav_search`  (
  `id` int(10) not null auto_increment comment '主键',
  `search_sort` int(10) not null comment '排序',
  `search_name` varchar(20) not null comment '搜索名称',
  `search_icon` varchar(150) null default null comment '搜索图标',
  `search_url` varchar(100) not null comment '搜索链接',
  `search_status` char(1) not null default '1' comment '状态 1=>显示;2=>隐藏',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '搜索表';

-- ----------------------------
-- 用户密钥表
-- ----------------------------
drop table if exists `nav_user_key`;
create table `nav_user_key`  (
  `id` int(11) not null auto_increment comment 'id',
  `user_name` varchar(30) not null comment '用户名',
  `key_number` varchar(50) not null comment '密钥',
  `key_status` char(2) not null default '1' comment '密钥状态 1=>正常;2=>停用',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '用户密钥表';

-- ----------------------------
-- 网站表
-- ----------------------------
drop table if exists `nav_web`;
create table `nav_web`  (
  `id` int(11) not null auto_increment comment '主键',
  `classify_id` int(11) not null comment '分类id',
  `web_name` varchar(50) not null comment '网站名称',
  `web_url` varchar(150) not null comment '网站地址',
  `web_icon` varchar(150) null default null comment '网站图标',
  `web_details` longtext not null comment '网站详情',
  `web_views` int(10) not null default 0 comment '浏览量',
  `web_collection` int(10) not null default 0 comment '收藏量',
  `web_status` char(2) not null default '1' comment '状态 1=>显示;2=>隐藏',
  `audit_status` char(2) not null default '0' comment '审核状态;0=>非审核链接;1=>未审核;2=>已审核;3=>审核未通过',
  `audit_reason` varchar(100) null default null comment '未通过理由',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  `update_by` varchar(64) null default '' comment '更新者',
  `update_time` datetime null default null comment '更新时间',
  primary key (`id`) using btree
) engine = innodb comment = '网站表';

-- ----------------------------
-- 收藏表
-- ----------------------------
drop table if exists `nav_web_collect`;
create table `nav_web_collect`  (
  `id` int(10) not null auto_increment comment '主键',
  `web_id` int(10) not null comment '网站id',
  `user_id` int(11) not null comment '用户id',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  primary key (`id`) using btree
) engine = innodb comment = '收藏表';

-- ----------------------------
-- 标签表
-- ----------------------------
drop table if exists `nav_web_label`;
create table `nav_web_label`  (
  `id` int(10) not null auto_increment comment '主键',
  `web_id` int(10) not null comment '网站id',
  `label_id` int(11) not null comment '用户id',
  `create_by` varchar(64) not null default '' comment '创建者',
  `create_time` datetime not null comment '创建时间',
  primary key (`id`) using btree
) engine = innodb comment = '标签关联网站表';
