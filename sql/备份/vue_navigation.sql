/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50734
Source Host           : localhost:3306
Source Database       : test_vue_navigation

Target Server Type    : MYSQL
Target Server Version : 50734
File Encoding         : 65001

Date: 2022-09-15 16:21:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=211 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------

-- ----------------------------
-- Table structure for nav_classify
-- ----------------------------
DROP TABLE IF EXISTS `nav_classify`;
CREATE TABLE `nav_classify` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classify_sort` int(10) NOT NULL DEFAULT '1' COMMENT '排序',
  `classify_name` varchar(20) NOT NULL COMMENT '分类名称',
  `classify_status` char(1) NOT NULL DEFAULT '1' COMMENT '状态 1=显示 2=隐藏',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of nav_classify
-- ----------------------------

-- ----------------------------
-- Table structure for nav_comments
-- ----------------------------
DROP TABLE IF EXISTS `nav_comments`;
CREATE TABLE `nav_comments` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `b_username` varchar(64) DEFAULT NULL COMMENT '被回复者',
  `comments_id` int(10) DEFAULT NULL COMMENT '上级评论ID',
  `web_id` int(10) NOT NULL COMMENT '网站ID',
  `comments_text` longtext COMMENT '评论内容',
  `comments_ip` varchar(15) NOT NULL COMMENT '评论IP地址',
  `comments_ipadderss` varchar(20) NOT NULL COMMENT '评论IP归属地',
  `comments_browser` varchar(20) NOT NULL COMMENT '评论浏览器',
  `comments_system` varchar(20) NOT NULL COMMENT '评论系统',
  `comments_status` char(1) NOT NULL DEFAULT '1' COMMENT '评论状态 1=显示 2=隐藏',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of nav_comments
-- ----------------------------

-- ----------------------------
-- Table structure for nav_label
-- ----------------------------
DROP TABLE IF EXISTS `nav_label`;
CREATE TABLE `nav_label` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `label_name` varchar(20) NOT NULL COMMENT '标签名称',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签表';

-- ----------------------------
-- Records of nav_label
-- ----------------------------

-- ----------------------------
-- Table structure for nav_menu
-- ----------------------------
DROP TABLE IF EXISTS `nav_menu`;
CREATE TABLE `nav_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `menu_name` varchar(20) CHARACTER SET utf8 NOT NULL COMMENT '菜单名称',
  `menu_url` varchar(150) NOT NULL COMMENT '路由地址',
  `menu_status` char(1) CHARACTER SET utf8 NOT NULL DEFAULT '1' COMMENT '菜单状态 1显示 2隐藏',
  `menu_type` char(1) NOT NULL COMMENT '系统内置',
  `menu_sort` int(10) NOT NULL DEFAULT '1' COMMENT '排序',
  `create_by` varchar(64) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8 DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='菜单表';

-- ----------------------------
-- Records of nav_menu
-- ----------------------------
INSERT INTO `nav_menu` VALUES ('1', '网站分类', '/classify', '1', 'Y', '900', 'admin', '2022-09-14 23:05:13', 'admin', '2022-09-14 23:43:01');
INSERT INTO `nav_menu` VALUES ('2', '首页', '/', '1', 'Y', '1000', 'admin', '2022-09-14 23:06:55', 'admin', '2022-09-14 23:42:34');
INSERT INTO `nav_menu` VALUES ('3', '标签页', '/tag', '1', 'Y', '800', 'admin', '2022-09-14 23:43:22', 'admin', null);
INSERT INTO `nav_menu` VALUES ('4', '网站提交', '/push', '1', 'Y', '700', 'admin', '2022-09-14 23:43:38', 'admin', null);
INSERT INTO `nav_menu` VALUES ('5', 'API接口', '/interface', '1', 'Y', '600', 'admin', '2022-10-17 14:38:44', 'admin', '2022-10-17 14:38:53');

-- ----------------------------
-- Table structure for nav_search
-- ----------------------------
DROP TABLE IF EXISTS `nav_search`;
CREATE TABLE `nav_search` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `search_sort` int(10) NOT NULL COMMENT '排序',
  `search_name` varchar(20) NOT NULL COMMENT '搜索名称',
  `search_icon` varchar(150) DEFAULT NULL COMMENT '搜索图标',
  `search_url` varchar(100) NOT NULL COMMENT '搜索链接',
  `search_status` char(1) NOT NULL DEFAULT '1' COMMENT '状态 1=显示 2=隐藏',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='搜索表';

-- ----------------------------
-- Records of nav_search
-- ----------------------------
INSERT INTO `nav_search` VALUES ('1', '100', '百度', null, 'www.baidu.com/s?wd=', '1', 'admin', '2022-09-07 13:44:17', 'admin', '2022-09-08 13:15:00');
INSERT INTO `nav_search` VALUES ('2', '20', 'Bing', null, 'cn.bing.com/search?q=', '1', 'admin', '2022-09-08 13:14:16', 'admin', '2022-09-08 13:17:30');
INSERT INTO `nav_search` VALUES ('3', '10', 'Google', null, 'www.google.com/search?q=', '1', 'admin', '2022-09-15 15:07:46', 'admin', '2022-09-15 15:07:57');

-- ----------------------------
-- Table structure for nav_web
-- ----------------------------
DROP TABLE IF EXISTS `nav_web`;
CREATE TABLE `nav_web` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `classify_id` int(11) NOT NULL COMMENT '分类id',
  `web_name` varchar(50) NOT NULL COMMENT '网站名称',
  `web_url` varchar(150) NOT NULL COMMENT '网站地址',
  `web_icon` varchar(150) DEFAULT NULL COMMENT '网站图标',
  `web_details` longtext NOT NULL COMMENT '网站详情',
  `web_views` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `web_collection` int(10) NOT NULL DEFAULT '0' COMMENT '收藏量',
  `web_status` char(1) NOT NULL DEFAULT '1' COMMENT '状态 1=显示 2=隐藏',
  `audit_status` char(1) NOT NULL DEFAULT '0' COMMENT '审核状态 0=非审核链接 1=未审核 2=已审核 3=审核未通过',
  `audit_reason` varchar(100) DEFAULT NULL COMMENT '未通过理由',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='网站表';

-- ----------------------------
-- Records of nav_web
-- ----------------------------

-- ----------------------------
-- Table structure for nav_web_collect
-- ----------------------------
DROP TABLE IF EXISTS `nav_web_collect`;
CREATE TABLE `nav_web_collect` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `web_id` int(10) NOT NULL COMMENT '网站id',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='收藏表';

-- ----------------------------
-- Records of nav_web_collect
-- ----------------------------

-- ----------------------------
-- Table structure for nav_web_label
-- ----------------------------
DROP TABLE IF EXISTS `nav_web_label`;
CREATE TABLE `nav_web_label` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `web_id` int(10) NOT NULL COMMENT '网站id',
  `label_id` int(11) NOT NULL COMMENT '用户id',
  `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='标签关联网站表';

-- ----------------------------
-- Records of nav_web_label
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blob类型的触发器表';

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日历信息表';

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron类型的触发器表';

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) NOT NULL COMMENT '状态',
  `job_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='已触发的触发器表';

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务详细信息表';

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储的悲观锁信息表';

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='暂停的触发器表';

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调度器状态表';

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='简单触发器的信息表';

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='同步机制的行锁表';

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='触发器详细信息表';

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `config_data_type` char(1) DEFAULT NULL COMMENT '数据类型  1=String 2=List 3=Boolean 3=int',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', '1', 'admin', '2022-08-22 18:42:22', 'admin', '2022-09-14 19:29:34', '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', '1', 'admin', '2022-08-22 18:42:22', '', null, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', '1', 'admin', '2022-08-22 18:42:22', '', null, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES ('4', '账号自助-验证码开关', 'sys.account.captchaEnabled', 'true', 'Y', '3', 'admin', '2022-08-22 18:42:22', 'admin', '2022-09-14 20:09:45', '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', '1', 'admin', '2022-08-22 18:42:22', 'admin', '2022-09-14 20:22:12', '是否开启注册用户功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('6', '网站名称', 'web.name', '聚合导航', 'Y', '1', 'admin', '2022-09-14 18:40:07', 'admin', '2022-09-15 15:09:36', '网站名称');
INSERT INTO `sys_config` VALUES ('7', '网站地址', 'web.url', 'www.zym88.cn', 'Y', '1', 'admin', '2022-09-14 18:41:37', '', null, '不带http协议的网址');
INSERT INTO `sys_config` VALUES ('8', '网站底部信息', 'web.footer.info', '@2022 聚合导航', 'Y', '1', 'admin', '2022-09-14 18:43:21', 'admin', '2022-09-15 15:09:51', '用于显示网站底部的信息');
INSERT INTO `sys_config` VALUES ('9', '网站SEO关键词', 'web.seo.keywords', '关键词1,关键词2,关键词3', 'Y', '2', 'admin', '2022-09-14 18:46:44', 'admin', '2022-09-14 20:00:34', '以|分隔，关键词数量一般控制在3-5个左右');
INSERT INTO `sys_config` VALUES ('10', '网站SEO描述', 'web.seo.description', '这是一个很方便的导航', 'Y', '1', 'admin', '2022-09-14 18:48:58', 'admin', '2022-09-14 19:01:50', 'SEO描述一般不超过200个字符');
INSERT INTO `sys_config` VALUES ('11', '禁用关键词', 'web.disable.keywords', '艹,尼玛,死了,操你,操她,操他', 'Y', '2', 'admin', '2022-09-14 19:08:07', 'admin', '2022-09-14 19:34:00', '全局通用禁用词');
INSERT INTO `sys_config` VALUES ('13', '网站SEO标题分割符号', 'nav.seo.separate', ' - ', 'Y', '1', 'admin', '2022-09-14 21:19:28', 'admin', '2022-09-14 21:37:49', '一般为\"-\"或者\"|\"');
INSERT INTO `sys_config` VALUES ('14', '首页链接显示数量', 'nav.index.link.number', '16', 'Y', '4', 'admin', '2022-11-22 08:47:58', 'admin', '2022-11-22 08:55:47', NULL);


-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '导航程序', '0', '', '', '', '0', '0', 'admin', '2022-08-22 18:42:22', 'admin', '2022-08-23 16:39:43');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '深圳总公司', '1', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '长沙分公司', '2', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('103', '100', '0,100', '研发部门', '1', '', '', '', '0', '0', 'admin', '2022-08-22 18:42:22', 'admin', '2022-08-23 16:40:27');
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '市场部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '测试部门', '3', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('106', '101', '0,100,101', '财务部门', '4', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('107', '101', '0,100,101', '运维部门', '5', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('108', '102', '0,100,102', '市场部门', '1', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('109', '102', '0,100,102', '财务部门', '2', '若依', '15888888888', 'ry@qq.com', '0', '2', 'admin', '2022-08-22 18:42:22', '', null);
INSERT INTO `sys_dept` VALUES ('110', '100', '0,100', '普通用户', '2', null, null, null, '0', '0', 'admin', '2022-09-09 18:04:40', '', null);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-08-22 18:42:22', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '99', '其他', '0', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '其他操作');
INSERT INTO `sys_dict_data` VALUES ('19', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('20', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('21', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('22', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('23', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('24', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('25', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('26', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('27', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('28', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('29', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-08-22 18:42:22', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('30', '0', '显示', '1', 'nav_is_show', null, 'success', 'N', '0', 'admin', '2022-08-23 16:03:31', 'admin', '2022-08-23 16:03:36', null);
INSERT INTO `sys_dict_data` VALUES ('31', '1', '隐藏', '2', 'nav_is_show', null, 'warning', 'N', '0', 'admin', '2022-08-23 16:03:52', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('32', '0', '未审核', '1', 'nav_audit_status', null, 'warning', 'N', '0', 'admin', '2022-08-23 16:19:32', 'admin', '2022-08-23 16:19:39', null);
INSERT INTO `sys_dict_data` VALUES ('33', '1', '已审核', '2', 'nav_audit_status', null, 'success', 'N', '0', 'admin', '2022-08-23 16:19:59', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('34', '2', '审核未通过', '3', 'nav_audit_status', null, 'danger', 'N', '0', 'admin', '2022-08-23 16:20:31', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('35', '0', '字符串（String）', '1', 'sys_config_data_type', null, 'default', 'N', '0', 'admin', '2022-09-14 19:21:01', 'admin', '2022-09-14 19:21:47', null);
INSERT INTO `sys_dict_data` VALUES ('36', '1', '列表（List）', '2', 'sys_config_data_type', null, 'default', 'N', '0', 'admin', '2022-09-14 19:21:39', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('37', '2', '布尔（Boolean）', '3', 'sys_config_data_type', null, 'default', 'N', '0', 'admin', '2022-09-14 19:22:07', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('38', '3', '数字（int）', '4', 'sys_config_data_type', null, 'default', 'N', '0', 'admin', '2022-09-14 20:07:05', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('39', '0', 'Json', '1', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:16:32', 'admin', '2022-10-10 19:16:38', null);
INSERT INTO `sys_dict_data` VALUES ('40', '0', 'Xml', '2', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:16:56', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('41', '0', 'Html', '3', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:17:12', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('42', '0', 'Script', '4', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:17:30', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('43', '0', 'Jsonp', '5', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:17:46', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('44', '0', 'Text', '6', 'nav_response_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:18:05', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('45', '0', 'GET', '1', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:56:25', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('46', '0', 'POST', '2', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:56:35', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('47', '0', 'PUT', '3', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:56:48', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('48', '0', 'DELETE', '4', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:56:59', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('49', '0', 'OPTIONS', '5', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:57:37', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('50', '0', 'HEAD', '6', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:57:52', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('51', '0', 'TRACE', '7', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:58:07', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('52', '0', 'CONNECT', '8', 'nav_request_type', null, 'primary', 'N', '0', 'admin', '2022-10-10 19:58:23', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('53', '0', 'Text', '1', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:09:56', 'admin', '2022-10-12 20:12:05', null);
INSERT INTO `sys_dict_data` VALUES ('54', '0', 'String', '2', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:10:10', 'admin', '2022-10-12 20:12:09', null);
INSERT INTO `sys_dict_data` VALUES ('55', '0', 'Number', '3', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:10:30', 'admin', '2022-10-12 20:12:13', null);
INSERT INTO `sys_dict_data` VALUES ('56', '0', 'Integer', '4', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:10:38', 'admin', '2022-10-12 20:12:17', null);
INSERT INTO `sys_dict_data` VALUES ('57', '0', 'Float', '5', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:10:49', 'admin', '2022-10-12 20:12:20', null);
INSERT INTO `sys_dict_data` VALUES ('58', '0', 'Double', '6', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:10:57', 'admin', '2022-10-12 20:12:23', null);
INSERT INTO `sys_dict_data` VALUES ('59', '0', 'Date', '7', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:11:06', 'admin', '2022-10-12 20:12:26', null);
INSERT INTO `sys_dict_data` VALUES ('60', '0', 'DateTime', '8', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:11:11', 'admin', '2022-10-12 20:12:33', null);
INSERT INTO `sys_dict_data` VALUES ('61', '0', 'TimeStamp', '9', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:11:17', 'admin', '2022-10-12 20:12:29', null);
INSERT INTO `sys_dict_data` VALUES ('62', '0', 'Boolean', '10', 'nav_data_type', null, 'primary', 'N', '0', 'admin', '2022-10-12 20:11:22', 'admin', '2022-10-12 20:12:36', null);
INSERT INTO `sys_dict_data` VALUES ('63', '0', '正常', '1', 'nav_api_status', null, 'primary', 'N', '0', 'admin', '2022-10-17 08:03:46', 'admin', '2022-10-17 08:04:01', null);
INSERT INTO `sys_dict_data` VALUES ('64', '0', '维护', '2', 'nav_api_status', null, 'warning', 'N', '0', 'admin', '2022-10-17 08:04:13', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('65', '0', '停用', '3', 'nav_api_status', null, 'danger', 'N', '0', 'admin', '2022-10-17 08:04:23', '', null, null);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2022-08-22 18:42:22', '', null, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2022-08-22 18:42:22', '', null, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2022-08-22 18:42:22', '', null, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2022-08-22 18:42:22', '', null, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2022-08-22 18:42:22', '', null, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2022-08-22 18:42:22', '', null, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2022-08-22 18:42:22', '', null, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2022-08-22 18:42:22', '', null, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2022-08-22 18:42:22', '', null, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2022-08-22 18:42:22', '', null, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('11', '显示或隐藏', 'nav_is_show', '0', 'admin', '2022-08-23 16:03:11', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('12', '审核状态', 'nav_audit_status', '0', 'admin', '2022-08-23 16:18:54', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('13', '参数的数据类型', 'sys_config_data_type', '0', 'admin', '2022-09-14 19:20:38', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('14', '返回值类型', 'nav_response_type', '0', 'admin', '2022-10-10 19:14:40', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('15', '请求类型', 'nav_request_type', '0', 'admin', '2022-10-10 19:55:56', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('16', '数据类型', 'nav_data_type', '0', 'admin', '2022-10-12 20:08:41', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('17', '接口状态', 'nav_api_status', '0', 'admin', '2022-10-17 08:03:14', '', null, null);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-08-22 18:42:22', '', null, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1146 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', 'system', null, '', '1', '0', 'M', '0', '0', '', 'system', 'admin', '2022-08-22 18:42:22', '', null, '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', 'monitor', null, '', '1', '0', 'M', '0', '0', '', 'monitor', 'admin', '2022-08-22 18:42:22', '', null, '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '3', 'tool', null, '', '1', '0', 'M', '0', '0', '', 'tool', 'admin', '2022-08-22 18:42:22', '', null, '系统工具目录');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', 'user', 'system/user/index', '', '1', '0', 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-08-22 18:42:22', '', null, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', 'role', 'system/role/index', '', '1', '0', 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-08-22 18:42:22', '', null, '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', 'menu', 'system/menu/index', '', '1', '0', 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-08-22 18:42:22', '', null, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', 'dept', 'system/dept/index', '', '1', '0', 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-08-22 18:42:22', '', null, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', 'post', 'system/post/index', '', '1', '0', 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-08-22 18:42:22', '', null, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', 'dict', 'system/dict/index', '', '1', '0', 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-08-22 18:42:22', '', null, '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', 'config', 'system/config/index', '', '1', '0', 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-08-22 18:42:22', '', null, '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', 'notice', 'system/notice/index', '', '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-08-22 18:42:22', '', null, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', 'log', '', '', '1', '0', 'M', '0', '0', '', 'log', 'admin', '2022-08-22 18:42:22', '', null, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', 'online', 'monitor/online/index', '', '1', '0', 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-08-22 18:42:22', '', null, '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', 'job', 'monitor/job/index', '', '1', '0', 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-08-22 18:42:22', '', null, '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', 'druid', 'monitor/druid/index', '', '1', '0', 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-08-22 18:42:22', '', null, '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', 'server', 'monitor/server/index', '', '1', '0', 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-08-22 18:42:22', '', null, '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '缓存监控', '2', '5', 'cache', 'monitor/cache/index', '', '1', '0', 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-08-22 18:42:22', '', null, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '缓存列表', '2', '6', 'cacheList', 'monitor/cache/list', '', '1', '0', 'C', '0', '0', 'monitor:cache:list', 'redis-list', 'admin', '2022-08-22 18:42:22', '', null, '缓存列表菜单');
INSERT INTO `sys_menu` VALUES ('115', '表单构建', '3', '1', 'build', 'tool/build/index', '', '1', '0', 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-08-22 18:42:22', '', null, '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('116', '代码生成', '3', '2', 'gen', 'tool/gen/index', '', '1', '0', 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-08-22 18:42:22', '', null, '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('117', '系统接口', '3', '3', 'swagger', 'tool/swagger/index', '', '1', '0', 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-08-22 18:42:22', '', null, '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', 'operlog', 'monitor/operlog/index', '', '1', '0', 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-08-22 18:42:22', '', null, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', '', '1', '0', 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-08-22 18:42:22', '', null, '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1000', '用户查询', '100', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1001', '用户新增', '100', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户修改', '100', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户删除', '100', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户导出', '100', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导入', '100', '6', '', '', '', '1', '0', 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1006', '重置密码', '100', '7', '', '', '', '1', '0', 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1007', '角色查询', '101', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色新增', '101', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色修改', '101', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1010', '角色删除', '101', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色导出', '101', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1012', '菜单查询', '102', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单新增', '102', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单修改', '102', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单删除', '102', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1016', '部门查询', '103', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门新增', '103', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门修改', '103', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门删除', '103', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1020', '岗位查询', '104', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位新增', '104', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位修改', '104', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位删除', '104', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位导出', '104', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1025', '字典查询', '105', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1026', '字典新增', '105', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1027', '字典修改', '105', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1028', '字典删除', '105', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1029', '字典导出', '105', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1030', '参数查询', '106', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1031', '参数新增', '106', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1032', '参数修改', '106', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1033', '参数删除', '106', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1034', '参数导出', '106', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1035', '公告查询', '107', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告新增', '107', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告修改', '107', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告删除', '107', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1039', '操作查询', '500', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1040', '操作删除', '500', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1041', '日志导出', '500', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1042', '登录查询', '501', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录删除', '501', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1044', '日志导出', '501', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1045', '账户解锁', '501', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:unlock', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1046', '在线查询', '109', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1047', '批量强退', '109', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1048', '单条强退', '109', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1049', '任务查询', '110', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1050', '任务新增', '110', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1051', '任务修改', '110', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1052', '任务删除', '110', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1053', '状态修改', '110', '5', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1054', '任务导出', '110', '6', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1055', '生成查询', '116', '1', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1056', '生成修改', '116', '2', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1057', '生成删除', '116', '3', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1058', '导入代码', '116', '4', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '预览代码', '116', '5', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '生成代码', '116', '6', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-08-22 18:42:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1061', '网址管理', '0', '10', 'linksManage', null, null, '1', '0', 'M', '0', '0', '', 'international', 'admin', '2022-08-23 16:29:29', 'admin', '2022-08-23 16:59:22', '');
INSERT INTO `sys_menu` VALUES ('1062', '搜索管理', '0', '20', 'searchManage', null, null, '1', '0', 'M', '0', '0', null, 'search', 'admin', '2022-08-23 16:31:31', '', null, '');
INSERT INTO `sys_menu` VALUES ('1063', '评论管理', '0', '30', 'commentsManage', null, null, '1', '0', 'M', '0', '0', null, 'message', 'admin', '2022-08-23 16:35:19', '', null, '');
INSERT INTO `sys_menu` VALUES ('1064', '标签管理', '0', '40', 'labelManage', null, null, '1', '0', 'M', '0', '0', '', 'guide', 'admin', '2022-08-23 16:36:30', 'admin', '2022-08-23 16:36:37', '');
INSERT INTO `sys_menu` VALUES ('1089', '分类管理', '0', '5', 'classifyManage', null, null, '1', '0', 'M', '0', '0', '', 'list', 'admin', '2022-08-23 17:13:08', 'admin', '2022-08-24 23:37:38', '');
INSERT INTO `sys_menu` VALUES ('1097', '标签列表', '1064', '1', 'label', 'navigation/label/index', null, '1', '0', 'C', '0', '0', 'navigation:label:list', '#', 'admin', '2022-09-07 13:13:15', '', null, '标签列表菜单');
INSERT INTO `sys_menu` VALUES ('1098', '标签列表查询', '1097', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:label:query', '#', 'admin', '2022-09-07 13:13:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1099', '标签列表新增', '1097', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:label:add', '#', 'admin', '2022-09-07 13:13:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1100', '标签列表修改', '1097', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:label:edit', '#', 'admin', '2022-09-07 13:13:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1101', '标签列表删除', '1097', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:label:remove', '#', 'admin', '2022-09-07 13:13:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1102', '标签列表导出', '1097', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:label:export', '#', 'admin', '2022-09-07 13:13:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1109', '搜索列表', '1062', '1', 'search', 'navigation/search/index', null, '1', '0', 'C', '0', '0', 'navigation:search:list', '#', 'admin', '2022-09-07 13:22:55', '', null, '搜索列表菜单');
INSERT INTO `sys_menu` VALUES ('1110', '搜索列表查询', '1109', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:search:query', '#', 'admin', '2022-09-07 13:22:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('1111', '搜索列表新增', '1109', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:search:add', '#', 'admin', '2022-09-07 13:22:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('1112', '搜索列表修改', '1109', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:search:edit', '#', 'admin', '2022-09-07 13:22:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('1113', '搜索列表删除', '1109', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:search:remove', '#', 'admin', '2022-09-07 13:22:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('1114', '搜索列表导出', '1109', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:search:export', '#', 'admin', '2022-09-07 13:22:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('1115', '分类列表', '1089', '1', 'classify', 'navigation/classify/index', null, '1', '0', 'C', '0', '0', 'navigation:classify:list', '#', 'admin', '2022-09-07 13:24:15', 'admin', '2022-09-07 13:38:55', '分类菜单');
INSERT INTO `sys_menu` VALUES ('1116', '分类查询', '1115', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:classify:query', '#', 'admin', '2022-09-07 13:24:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1117', '分类新增', '1115', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:classify:add', '#', 'admin', '2022-09-07 13:24:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1118', '分类修改', '1115', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:classify:edit', '#', 'admin', '2022-09-07 13:24:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1119', '分类删除', '1115', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:classify:remove', '#', 'admin', '2022-09-07 13:24:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1120', '分类导出', '1115', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:classify:export', '#', 'admin', '2022-09-07 13:24:15', '', null, '');
INSERT INTO `sys_menu` VALUES ('1121', '评论列表', '1063', '1', 'comments', 'navigation/comments/index', null, '1', '0', 'C', '0', '0', 'navigation:comments:list', '#', 'admin', '2022-09-07 13:30:22', '', null, '评论列表菜单');
INSERT INTO `sys_menu` VALUES ('1122', '评论列表查询', '1121', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:comments:query', '#', 'admin', '2022-09-07 13:30:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1123', '评论列表新增', '1121', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:comments:add', '#', 'admin', '2022-09-07 13:30:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1124', '评论列表修改', '1121', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:comments:edit', '#', 'admin', '2022-09-07 13:30:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1125', '评论列表删除', '1121', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:comments:remove', '#', 'admin', '2022-09-07 13:30:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1126', '评论列表导出', '1121', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:comments:export', '#', 'admin', '2022-09-07 13:30:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('1127', '网址列表', '1061', '1', 'web', 'navigation/web/index', null, '1', '0', 'C', '0', '0', 'navigation:web:list', '#', 'admin', '2022-09-07 13:35:24', '', null, '网址列表菜单');
INSERT INTO `sys_menu` VALUES ('1128', '网址列表查询', '1127', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:web:query', '#', 'admin', '2022-09-07 13:35:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('1129', '网址列表新增', '1127', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:web:add', '#', 'admin', '2022-09-07 13:35:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('1130', '网址列表修改', '1127', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:web:edit', '#', 'admin', '2022-09-07 13:35:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('1131', '网址列表删除', '1127', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:web:remove', '#', 'admin', '2022-09-07 13:35:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('1132', '网址列表导出', '1127', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:web:export', '#', 'admin', '2022-09-07 13:35:24', '', null, '');
INSERT INTO `sys_menu` VALUES ('1133', '审核列表', '1061', '1', 'audit_web', 'navigation/audit_web/index', null, '1', '0', 'C', '0', '0', 'navigation:audit_web:list', '#', 'admin', '2022-09-07 13:49:25', '', null, '审核列表菜单');
INSERT INTO `sys_menu` VALUES ('1134', '审核列表查询', '1133', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:audit_web:query', '#', 'admin', '2022-09-07 13:49:25', '', null, '');
INSERT INTO `sys_menu` VALUES ('1135', '审核列表新增', '1133', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:audit_web:add', '#', 'admin', '2022-09-07 13:49:25', '', null, '');
INSERT INTO `sys_menu` VALUES ('1136', '审核列表修改', '1133', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:audit_web:edit', '#', 'admin', '2022-09-07 13:49:25', '', null, '');
INSERT INTO `sys_menu` VALUES ('1137', '审核列表删除', '1133', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:audit_web:remove', '#', 'admin', '2022-09-07 13:49:25', '', null, '');
INSERT INTO `sys_menu` VALUES ('1138', '审核列表导出', '1133', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:audit_web:export', '#', 'admin', '2022-09-07 13:49:25', '', null, '');
INSERT INTO `sys_menu` VALUES ('1139', '菜单管理', '0', '4', 'menuManage', null, null, '1', '0', 'M', '0', '0', null, 'tree-table', 'admin', '2022-09-14 22:52:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('1140', '菜单列表', '1139', '1', 'menu', 'navigation/menu/index', null, '1', '0', 'C', '0', '0', 'navigation:menu:list', '#', 'admin', '2022-09-14 22:54:38', '', null, '菜单列表菜单');
INSERT INTO `sys_menu` VALUES ('1141', '菜单列表查询', '1140', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:menu:query', '#', 'admin', '2022-09-14 22:54:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('1142', '菜单列表新增', '1140', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:menu:add', '#', 'admin', '2022-09-14 22:54:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('1143', '菜单列表修改', '1140', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:menu:edit', '#', 'admin', '2022-09-14 22:54:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('1144', '菜单列表删除', '1140', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:menu:remove', '#', 'admin', '2022-09-14 22:54:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('1145', '菜单列表导出', '1140', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:menu:export', '#', 'admin', '2022-09-14 22:54:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('1146', '接口管理', '0', '50', 'ApiManage', null, null, '1', '0', 'M', '0', '0', null, 'tree', 'admin', '2022-10-10 19:23:27', '', null, '');
INSERT INTO `sys_menu` VALUES ('1147', '接口列表', '1146', '1', 'api', 'navigation/api/index', null, '1', '0', 'C', '0', '0', 'navigation:api:list', '#', 'admin', '2022-10-10 19:24:12', '', null, '接口列表菜单');
INSERT INTO `sys_menu` VALUES ('1148', '接口列表查询', '1147', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:api:query', '#', 'admin', '2022-10-10 19:24:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1149', '接口列表新增', '1147', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:api:add', '#', 'admin', '2022-10-10 19:24:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1150', '接口列表修改', '1147', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:api:edit', '#', 'admin', '2022-10-10 19:24:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1151', '接口列表删除', '1147', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:api:remove', '#', 'admin', '2022-10-10 19:24:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1152', '接口列表导出', '1147', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:api:export', '#', 'admin', '2022-10-10 19:24:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1153', '密钥列表', '1146', '1', 'secretkey', 'navigation/secretkey/index', null, '1', '0', 'C', '0', '0', 'navigation:secretkey:list', '#', 'admin', '2022-10-17 15:00:34', '', null, '密钥列表菜单');
INSERT INTO `sys_menu` VALUES ('1154', '密钥列表查询', '1153', '1', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:secretkey:query', '#', 'admin', '2022-10-17 15:00:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('1155', '密钥列表新增', '1153', '2', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:secretkey:add', '#', 'admin', '2022-10-17 15:00:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('1156', '密钥列表修改', '1153', '3', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:secretkey:edit', '#', 'admin', '2022-10-17 15:00:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('1157', '密钥列表删除', '1153', '4', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:secretkey:remove', '#', 'admin', '2022-10-17 15:00:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('1158', '密钥列表导出', '1153', '5', '#', '', null, '1', '0', 'F', '0', '0', 'navigation:secretkey:export', '#', 'admin', '2022-10-17 15:00:34', '', null, '');


-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '超级管理员', '1', '0', 'admin', '2022-08-22 18:42:22', 'admin', '2022-08-23 16:41:40', '');
INSERT INTO `sys_post` VALUES ('4', 'user', '普通用户', '2', '0', 'admin', '2022-08-22 18:42:22', 'admin', '2022-08-23 16:41:54', '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', '1', '1', '1', '1', '0', '0', 'admin', '2022-08-22 18:42:22', '', null, '超级管理员');
INSERT INTO `sys_role` VALUES ('2', '管理员', 'op', '2', '2', '1', '1', '0', '0', 'admin', '2022-08-22 18:42:22', 'admin', '2022-09-10 19:03:15', '管理员');
INSERT INTO `sys_role` VALUES ('3', '普通用户', 'user', '3', '1', '1', '1', '0', '0', 'admin', '2022-09-09 20:58:42', 'admin', '2022-09-09 20:59:05', null);

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1061');
INSERT INTO `sys_role_menu` VALUES ('2', '1062');
INSERT INTO `sys_role_menu` VALUES ('2', '1063');
INSERT INTO `sys_role_menu` VALUES ('2', '1064');
INSERT INTO `sys_role_menu` VALUES ('2', '1089');
INSERT INTO `sys_role_menu` VALUES ('2', '1097');
INSERT INTO `sys_role_menu` VALUES ('2', '1098');
INSERT INTO `sys_role_menu` VALUES ('2', '1099');
INSERT INTO `sys_role_menu` VALUES ('2', '1100');
INSERT INTO `sys_role_menu` VALUES ('2', '1101');
INSERT INTO `sys_role_menu` VALUES ('2', '1102');
INSERT INTO `sys_role_menu` VALUES ('2', '1109');
INSERT INTO `sys_role_menu` VALUES ('2', '1110');
INSERT INTO `sys_role_menu` VALUES ('2', '1111');
INSERT INTO `sys_role_menu` VALUES ('2', '1112');
INSERT INTO `sys_role_menu` VALUES ('2', '1113');
INSERT INTO `sys_role_menu` VALUES ('2', '1114');
INSERT INTO `sys_role_menu` VALUES ('2', '1115');
INSERT INTO `sys_role_menu` VALUES ('2', '1116');
INSERT INTO `sys_role_menu` VALUES ('2', '1117');
INSERT INTO `sys_role_menu` VALUES ('2', '1118');
INSERT INTO `sys_role_menu` VALUES ('2', '1119');
INSERT INTO `sys_role_menu` VALUES ('2', '1120');
INSERT INTO `sys_role_menu` VALUES ('2', '1121');
INSERT INTO `sys_role_menu` VALUES ('2', '1122');
INSERT INTO `sys_role_menu` VALUES ('2', '1123');
INSERT INTO `sys_role_menu` VALUES ('2', '1124');
INSERT INTO `sys_role_menu` VALUES ('2', '1125');
INSERT INTO `sys_role_menu` VALUES ('2', '1126');
INSERT INTO `sys_role_menu` VALUES ('2', '1127');
INSERT INTO `sys_role_menu` VALUES ('2', '1128');
INSERT INTO `sys_role_menu` VALUES ('2', '1129');
INSERT INTO `sys_role_menu` VALUES ('2', '1130');
INSERT INTO `sys_role_menu` VALUES ('2', '1131');
INSERT INTO `sys_role_menu` VALUES ('2', '1132');
INSERT INTO `sys_role_menu` VALUES ('2', '1133');
INSERT INTO `sys_role_menu` VALUES ('2', '1134');
INSERT INTO `sys_role_menu` VALUES ('2', '1135');
INSERT INTO `sys_role_menu` VALUES ('2', '1136');
INSERT INTO `sys_role_menu` VALUES ('2', '1137');
INSERT INTO `sys_role_menu` VALUES ('2', '1138');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', '昵称', '00', '123456@163.com', '15888888888', '1', '/profile/avatar/2022/09/14/blob_20220914183530A028.jpeg', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-09-15 15:52:39', 'admin', '2022-08-22 18:42:22', '', '2022-09-15 15:52:38', '管理员');

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');

-- ----------------------------
-- Table structure for nav_api
-- ----------------------------
DROP TABLE IF EXISTS `nav_api`;
CREATE TABLE `nav_api` (
   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `api_name` varchar(30) NOT NULL COMMENT '接口名称',
   `api_introduce` longtext NOT NULL COMMENT '接口介绍',
   `api_url` varchar(150) NOT NULL COMMENT '请求地址',
   `api_request_type` char(1) NOT NULL COMMENT '请求格式',
   `api_response_type` char(1) NOT NULL COMMENT '返回格式',
   `api_url_example` varchar(150) NOT NULL COMMENT '请求示例',
   `api_response_example` longtext COMMENT '返回示例',
   `api_status` char(2) NOT NULL DEFAULT '1' COMMENT '接口状态',
   `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
   `create_time` datetime NOT NULL COMMENT '创建时间',
   `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
   `update_time` datetime DEFAULT NULL COMMENT '更新时间',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='API管理表';

-- ----------------------------
-- Table structure for nav_api_request_key
-- ----------------------------
DROP TABLE IF EXISTS `nav_api_request_key`;
CREATE TABLE `nav_api_request_key` (
   `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
   `api_id` int(11) NOT NULL COMMENT '接口ID',
   `request_name` varchar(20) NOT NULL COMMENT '参数名称',
   `request_required` char(2) NOT NULL DEFAULT 'Y' COMMENT '是否必填',
   `request_type` char(2) NOT NULL DEFAULT '1' COMMENT '参数类型',
   `request_text` varchar(50) DEFAULT NULL COMMENT '参数说明',
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- ----------------------------
-- Table structure for nav_api_response_key
-- ----------------------------
DROP TABLE IF EXISTS `nav_api_response_key`;
CREATE TABLE `nav_api_response_key` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `api_id` int(11) NOT NULL COMMENT '接口ID',
    `response_name` varchar(20) NOT NULL COMMENT '参数名称',
    `response_type` char(2) NOT NULL DEFAULT '1' COMMENT '参数类型',
    `response_text` varchar(50) DEFAULT NULL COMMENT '参数说明',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for nav_user_key
-- ----------------------------
DROP TABLE IF EXISTS `nav_user_key`;
CREATE TABLE `nav_user_key` (
    `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
    `user_name` varchar(30) NOT NULL COMMENT '用户名',
    `key_number` varchar(50) NOT NULL COMMENT '密钥',
    `key_status` char(2) NOT NULL DEFAULT '1' COMMENT '密钥状态',
    `create_by` varchar(64) NOT NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime NOT NULL COMMENT '创建时间',
    `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
    `update_time` datetime DEFAULT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户密钥表';