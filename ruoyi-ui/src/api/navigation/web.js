import request from '@/utils/request'

// 查询非审核链接数量
export function noAuditCount() {
  return request({
    url: '/navigation/web/noAuditCount',
    method: 'get'
  })
}

// 查询审核链接数量
export function auditCount() {
  return request({
    url: '/navigation/web/auditCount',
    method: 'get'
  })
}

// 查询网址列表列表
export function listWeb(query) {
  return request({
    url: '/navigation/web/list',
    method: 'get',
    params: query
  })
}

// 查询非审核网址列表列表
export function noAuditList(query) {
  return request({
    url: '/navigation/web/noAuditList',
    method: 'get',
    params: query
  })
}

// 查询审核网址列表列表
export function auditList(query) {
  return request({
    url: '/navigation/web/auditList',
    method: 'get',
    params: query
  })
}

// 查询网址列表详细
export function getWeb(id) {
  return request({
    url: '/navigation/web/' + id,
    method: 'get'
  })
}

// 新增网址列表
export function addWeb(data) {
  return request({
    url: '/navigation/web',
    method: 'post',
    data: data
  })
}

// 修改网址列表
export function updateWeb(data) {
  return request({
    url: '/navigation/web',
    method: 'put',
    data: data
  })
}

// 删除网址列表
export function delWeb(id) {
  return request({
    url: '/navigation/web/' + id,
    method: 'delete'
  })
}
