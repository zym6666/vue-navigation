import request from '@/utils/request'

// 查询菜单列表列表
export function listMenu(query) {
  return request({
    url: '/navigation/menu/list',
    method: 'get',
    params: query
  })
}

// 查询菜单列表详细
export function getMenu(id) {
  return request({
    url: '/navigation/menu/' + id,
    method: 'get'
  })
}

// 新增菜单列表
export function addMenu(data) {
  return request({
    url: '/navigation/menu',
    method: 'post',
    data: data
  })
}

// 修改菜单列表
export function updateMenu(data) {
  return request({
    url: '/navigation/menu',
    method: 'put',
    data: data
  })
}

// 删除菜单列表
export function delMenu(id) {
  return request({
    url: '/navigation/menu/' + id,
    method: 'delete'
  })
}
