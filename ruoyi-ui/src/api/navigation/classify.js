import request from '@/utils/request'

// 查询分类列表
export function listClassify(query) {
  return request({
    url: '/navigation/classify/list',
    method: 'get',
    params: query
  })
}

// 查询分类详细
export function getClassify(id) {
  return request({
    url: '/navigation/classify/' + id,
    method: 'get'
  })
}

// 新增分类
export function addClassify(data) {
  return request({
    url: '/navigation/classify',
    method: 'post',
    data: data
  })
}

// 修改分类
export function updateClassify(data) {
  return request({
    url: '/navigation/classify',
    method: 'put',
    data: data
  })
}

// 删除分类
export function delClassify(id) {
  return request({
    url: '/navigation/classify/' + id,
    method: 'delete'
  })
}
