import request from '@/utils/request'

// 查询密钥列表列表
export function listSecretkey(query) {
  return request({
    url: '/navigation/secretkey/list',
    method: 'get',
    params: query
  })
}

// 查询密钥列表详细
export function getSecretkey(id) {
  return request({
    url: '/navigation/secretkey/' + id,
    method: 'get'
  })
}

// 新增密钥列表
export function addSecretkey(data) {
  return request({
    url: '/navigation/secretkey',
    method: 'post',
    data: data
  })
}

// 修改密钥列表
export function updateSecretkey(data) {
  return request({
    url: '/navigation/secretkey',
    method: 'put',
    data: data
  })
}

// 删除密钥列表
export function delSecretkey(id) {
  return request({
    url: '/navigation/secretkey/' + id,
    method: 'delete'
  })
}
