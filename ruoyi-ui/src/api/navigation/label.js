import request from '@/utils/request'

// 查询标签数量
export function labelCount() {
  return request({
    url: '/navigation/label/labelCount',
    method: 'get'
  })
}

// 查询标签列表列表
export function listLabel(query) {
  return request({
    url: '/navigation/label/list',
    method: 'get',
    params: query
  })
}

// 查询标签列表详细
export function getLabel(id) {
  return request({
    url: '/navigation/label/' + id,
    method: 'get'
  })
}

// 新增标签列表
export function addLabel(data) {
  return request({
    url: '/navigation/label',
    method: 'post',
    data: data
  })
}

// 修改标签列表
export function updateLabel(data) {
  return request({
    url: '/navigation/label',
    method: 'put',
    data: data
  })
}

// 删除标签列表
export function delLabel(id) {
  return request({
    url: '/navigation/label/' + id,
    method: 'delete'
  })
}
