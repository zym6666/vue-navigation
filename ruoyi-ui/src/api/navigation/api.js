import request from '@/utils/request'

// 查询接口数量
export function apiCount() {
  return request({
    url: '/navigation/api/apiCount',
    method: 'get'
  })
}

// 查询接口列表列表
export function listApi(query) {
  return request({
    url: '/navigation/api/list',
    method: 'get',
    params: query
  })
}

// 查询接口列表详细
export function getApi(id) {
  return request({
    url: '/navigation/api/' + id,
    method: 'get'
  })
}

// 新增接口列表
export function addApi(data) {
  return request({
    url: '/navigation/api',
    method: 'post',
    data: data
  })
}

// 修改接口列表
export function updateApi(data) {
  return request({
    url: '/navigation/api',
    method: 'put',
    data: data
  })
}

// 删除接口列表
export function delApi(id) {
  return request({
    url: '/navigation/api/' + id,
    method: 'delete'
  })
}
