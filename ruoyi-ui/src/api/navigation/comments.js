import request from '@/utils/request'

// 查询评论数量
export function commentsCount() {
  return request({
    url: '/navigation/comments/commentsCount',
    method: 'get'
  })
}

// 查询评论列表列表
export function listComments(query) {
  return request({
    url: '/navigation/comments/list',
    method: 'get',
    params: query
  })
}

// 查询评论列表详细
export function getComments(id) {
  return request({
    url: '/navigation/comments/' + id,
    method: 'get'
  })
}

// 新增评论列表
export function addComments(data) {
  return request({
    url: '/navigation/comments',
    method: 'post',
    data: data
  })
}

// 修改评论列表
export function updateComments(data) {
  return request({
    url: '/navigation/comments',
    method: 'put',
    data: data
  })
}

// 删除评论列表
export function delComments(id) {
  return request({
    url: '/navigation/comments/' + id,
    method: 'delete'
  })
}
