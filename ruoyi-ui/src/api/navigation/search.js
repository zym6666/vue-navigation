import request from '@/utils/request'

// 查询搜索列表列表
export function listSearch(query) {
  return request({
    url: '/navigation/search/list',
    method: 'get',
    params: query
  })
}

// 查询搜索列表详细
export function getSearch(id) {
  return request({
    url: '/navigation/search/' + id,
    method: 'get'
  })
}

// 新增搜索列表
export function addSearch(data) {
  return request({
    url: '/navigation/search',
    method: 'post',
    data: data
  })
}

// 修改搜索列表
export function updateSearch(data) {
  return request({
    url: '/navigation/search',
    method: 'put',
    data: data
  })
}

// 删除搜索列表
export function delSearch(id) {
  return request({
    url: '/navigation/search/' + id,
    method: 'delete'
  })
}
