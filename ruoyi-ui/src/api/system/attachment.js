import request from '@/utils/request'

// 获取附件列表
export function listAttachment(data) {
  return request({
    url: '/system/attachment/list',
    method: 'get',
    params: data
  })
}

// 查询附件列表
export function queryAttachment(data) {
  return request({
    url: '/system/attachment/query',
    method: 'get',
    params: data
  })
}

// 删除附件
export function delAttachment(data) {
  return request({
    url: '/system/attachment/removeAll',
    method: 'post',
    data: data
  })
}
