package com.ruoyi;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.system.domain.*;
import com.ruoyi.system.mapper.NavWebMapper;
import com.ruoyi.system.service.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@SpringBootTest
public class RuoYiApplicationTest extends BaseController {

    @Autowired
    private NavWebMapper navWebMapper;

    @Autowired
    private INavClassifyService iNavClassifyService;

    @Autowired
    private INavWebLabelService iNavWebLabelService;

    @Autowired
    private INavLabelService iNavLabelService;

    @Autowired
    private INavCommentsService iNavCommentsService;

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private INavApiRequestKeyService iNavApiRequestKeyService;

    @Autowired
    private INavApiResponseKeyService iNavApiResponseKeyService;

    @Autowired
    private RedisCache redisCache;

    /**
     * 查询非审核链接列表
     */
    @Test
    public void selectNoAuditNavWebById(){
        QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("audit_status", "0").or().eq("audit_status","2");
        List<NavWeb> navWebList = navWebMapper.selectList(queryWrapper);
        for (NavWeb navWeb : navWebList) {
            //遍历取得分类名称
            QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
            classifyQueryWrapper.eq("id", navWeb.getClassifyId());
            navWeb.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", navWeb.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            navWeb.setLabelList(labelNameList);
        }
        navWebList.forEach(System.out::println);
    }

    /**
     * 查询审核网址列表
     */
    @Test
    public void selectAuditNavWebById(){
        QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
        queryWrapper.ne("audit_status", "0");
        List<NavWeb> navWebList = navWebMapper.selectList(queryWrapper);
        for (NavWeb navWeb : navWebList) {
            //遍历取得分类名称
            QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
            classifyQueryWrapper.eq("id", navWeb.getClassifyId());
            navWeb.setClassifyName(iNavClassifyService.getOne(classifyQueryWrapper).getClassifyName());
            //遍历取得标签名称
            QueryWrapper<NavWebLabel> webLabelQueryWrapper = new QueryWrapper<>();
            webLabelQueryWrapper.eq("web_id", navWeb.getId());
            List<NavWebLabel> labelList = iNavWebLabelService.list(webLabelQueryWrapper);
            List<String> labelNameList = new ArrayList<>();
            for (NavWebLabel navWebLabel : labelList) {
                QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
                labelQueryWrapper.eq("id", navWebLabel.getLabelId());
                labelNameList.add(iNavLabelService.getOne(labelQueryWrapper).getLabelName());
            }
            navWeb.setLabelList(labelNameList);
        }
        navWebList.forEach(System.out::println);
    }

    /**
     * 新增标签
     */
    @Test
    void addLabel(){
        NavLabel label = new NavLabel();
        label.setLabelName("测试123");
        label.setCreateBy("admin");
        label.setCreateTime(DateUtils.getNowDate());
        iNavLabelService.save(label);
        System.out.println(label);
    }

    /**
     * 查询数量
     */
    @Test
    void selectCount(){
        //非审核链接数量
        QueryWrapper<NavWeb> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("audit_status", "0").or().eq("audit_status","2");
        Long noAuditCount = navWebMapper.selectCount(queryWrapper1);
        System.out.println(noAuditCount);
        //审核链接数量
        QueryWrapper<NavWeb> queryWrapper2 = new QueryWrapper<>();
        queryWrapper2.ne("audit_status", "0");
        Long auditCount = navWebMapper.selectCount(queryWrapper2);
        System.out.println(auditCount);
        //评论数量
        System.out.println(iNavCommentsService.count());
        //标签数量
        System.out.println(iNavLabelService.count());
    }

    /**
     * 查询指定分类的链接
     */
    @Test
    void selectNavWebByClassifyId(){
        NavWeb navWeb = new NavWeb();
        navWeb.setClassifyId(1L);
        List<NavWeb> navWebs = navWebMapper.selectNoAuditNavWebList(navWeb);
        navWebs.forEach(System.out::println);
    }

    /**
     * 增加标签
     */
    @Test
    void addNavWebLabel(){
        String[] list = "先入为主、答非所问、落地生根、骑马找马、莺歌燕舞、不由自主、十字路口、回天无力、星星点点、拔苗助长、万水千山、目中无人、桃红柳绿、一事无成、一路平安、春风化雨、柳绿花红、古往今来、先人后己、毛手毛脚、乐极生悲、天网恢恢、头头是道、明明白白、众多非一、张三李四、一往无前、南来北往、自由自在、张灯结彩、瓜田李下、千军万马、前思后想、热火朝天、目中无人、毛手毛脚、乐极生悲、一往无前、万里长城、天网恢恢、张三李四、百发百中、莺歌燕舞、千人一面、安居乐业、井井有条、旁若无人、再三再四、百花齐放、窗明几净、长年累月、成家立业、一五一十、心直口快、助人为乐、狗急跳墙、风和日丽、冷言冷语、安身立命、三心二意、人来人往、和风细雨、落花流水、天罗地网、十指连心、古往今来、星星点点、众多非一、面红耳赤、南来北往、自由自在、不由自主、一路平安、春风化雨、十字路口、答非所问、拔苗助长、非亲非故、念念不忘、水深火热、自以为是、十全十美、东山再起、炎黄子孙、春暖花开、白手起家、三更半夜、山清水秀、红男绿女、快言快语、别有洞天、春暖花开、炎黄子孙、鸟语花香、诗情画意、满面春风、五花八门、面目全非、滴水穿石、九牛一毛、无边无际、欣欣向荣、一知半解、众志成城、丰功伟绩、志同道合、八仙过海、一鸣惊人、斤斤计较、不约而同、漫山遍野、川流不息、奇形怪状、枯木逢春、两面三刀、吞吞吐吐、惊涛骇浪、鹅毛大雪、脚踏实地、尺有所短、繁荣昌盛、焦头烂额、叫苦连天、四海为家、一年一度、自取灭亡、怦然心动、千家万户、推陈出新、寿比南山、脱口而出、后继有人、天寒地冻、出尔反尔、纹丝不动、画龙点睛、日久天长、天高云淡、雨过天晴、胡言乱语、千变万化、灵丹妙药、隐隐约约、起早摸黑、日积月累、凤毛麟角、显而易见、勾心斗角、代代相传、省吃俭用、自始至终、齐心合力、一动不动、大显身手、单刀直入、甜言蜜语、呼风唤雨、枝繁叶茂、四面八方、五谷丰登、喝西北风、寸有所长、无家可归、滥竽充数、白雪皑皑、妙不可言、急不可耐、生机勃勃、怒火冲天、目不暇接、专心致志、转败为胜、左思右想、亭亭玉立、遥遥相对、步步登高、一筹莫展、震耳欲聋、斩钉截铁、聚精会神、绘声绘色、九五之尊、随心所欲、干将莫邪、相得益彰、借刀杀人、浪迹天涯、刚愎自用、镜花水月、黔驴技穷、肝胆相照、多多益善、叱咤风云、杞人忧天、作茧自缚、一飞冲天、殊途同归、风卷残云、因果报应、无可厚非、赶尽杀绝、天长地久、飞龙在天、桃之夭夭、南柯一梦、口是心非、草船借箭、铁石心肠、望其项背、头晕目眩、纵横天下、有问必答、无为而治、釜底抽薪、吹毛求疵、好事多磨、空谷幽兰、悬梁刺股、白手起家、完璧归赵、忍俊不禁、沐猴而冠、白云苍狗、贼眉鼠眼、围魏救赵、烟雨蒙蒙、炙手可热、尸位素餐、出水芙蓉、礼仪之邦、一丘之貉、鹏程万里、叹为观止、韦编三绝、今生今世、草木皆兵、宁缺毋滥、回光返照、露水夫妻、讳莫如深、贻笑大方、紫气东来、万马奔腾、一诺千金、老马识途、五花大绑、捉襟见肘、瓜田李下、水漫金山、苦心孤诣、可见一斑、五湖四海、虚怀若谷、欲擒故纵、风声鹤唳、毛遂自荐、蛛丝马迹、中庸之道、迷途知返、自由自在、龙飞凤舞、树大根深、雨过天晴、乘风破浪、筚路蓝缕、朝三暮四、患得患失、君子好逑、鞭长莫及、竭泽而渔、飞黄腾达、囊萤映雪、飞蛾扑火、自怨自艾、风驰电掣、白马非马、退避三舍、三山五岳、称心如意、望梅止渴、茕茕孑立、振聋发聩、运筹帷幄、逃之夭夭、杯水车薪、有的放矢、矫枉过正、睚眦必报、姗姗来迟、一鸣惊人、孜孜不倦、一马平川、入木三分、沆瀣一气、天伦之乐、9字10字11字12字藕断丝连、心猿意马、想入非非、盲人摸象、眉飞色舞、三教九流、高楼大厦、锲而不舍、过犹不及、狗尾续貂、斗酒学士、高山仰止、形影不离、小心翼翼、返璞归真、见贤思齐、按图索骥、枪林弹雨、桀骜不驯、遇人不淑、道貌岸然、名扬四海、虚与委蛇、门可罗雀、水落石出、不卑不亢、无法无天、拔苗助长、大快朵颐、因地制宜、单刀直入、时来运转、天方夜谭、一蹴而就、踌躇满志、战无不胜、插翅难飞、图穷匕见、鬼话连篇、亢龙有悔、望洋兴叹、爱屋及乌、惊鸿一瞥、风华绝代、名胜古迹、如履薄冰、持之以恒、潜移默化、昙花一现、巫山云雨、狡兔三窟、栉风沐雨、骇人听闻、断章取义、曲突徙薪、谢天谢地、脱颖而出、垂帘听政、一马当先、"
                .split("、");
        for (String s : list) {
            if(StringUtils.isNotEmpty(s)){
                QueryWrapper<NavLabel> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("label_name", s);
                if (iNavLabelService.getOne(queryWrapper) == null) {
                    NavLabel label = new NavLabel();
                    label.setLabelName(s);
                    label.setCreateBy("admin");
                    label.setCreateTime(DateUtils.getNowDate());
                    iNavLabelService.save(label);
                }
            }
        }
    }

    /**
     * 生成链接测试数据
     */
    @Test
    void addWebTestData(){
        QueryWrapper<NavClassify> classifyQueryWrapper = new QueryWrapper<>();
        classifyQueryWrapper.select("id");
        List<NavClassify> list = iNavClassifyService.list(classifyQueryWrapper);

        QueryWrapper<NavLabel> labelQueryWrapper = new QueryWrapper<>();
        classifyQueryWrapper.select("id");
        List<NavLabel> list1 = iNavLabelService.list(labelQueryWrapper);
        //生成网站测试数据
        for (int i = 0; i < 2000; i++) {
            //打乱顺序
            Collections.shuffle(list);
            Collections.shuffle(list1);
            NavWeb navWeb = new NavWeb();
            navWeb.setWebName("站点名称" + i);
            navWeb.setWebUrl("www.zym88.cn");
            navWeb.setWebDetails("网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍网站介绍");
            navWeb.setClassifyId(Long.valueOf(list.get(0).getId()));
            navWeb.setCreateBy("admin");
            navWeb.setCreateTime(DateUtils.getNowDate());
            if(navWebMapper.selectOne(new QueryWrapper<NavWeb>().eq("web_name", navWeb.getWebName())) == null){
                navWebMapper.insert(navWeb);
                //插入标签
                NavWebLabel navWebLabel = new NavWebLabel();
                navWebLabel.setWebId(navWeb.getId());
                navWebLabel.setLabelId(list1.get(0).getId());
                navWebLabel.setCreateBy("admin");
                navWebLabel.setCreateTime(DateUtils.getNowDate());
                iNavWebLabelService.save(navWebLabel);
            }
        }
    }

    /**
     * 生成评论测试数据
     */
    @Test
    void addCommentsTestData(){
        List<NavWeb> navWebs = navWebMapper.selectNoAuditNavWebList(new NavWeb());
        //生成评论测试数据
        for (int i = 0; i < 2000; i++) {
            //打乱顺序
            Collections.shuffle(navWebs);
            //插入评论
            NavComments navComments = new NavComments();
            navComments.setWebId(2763);
            navComments.setCommentsText("评论内容" + i);
            navComments.setCommentsIp("127.0.0.1");
            navComments.setCommentsIpadderss("中国");
            navComments.setCommentsBrowser("Chrome");
            navComments.setCommentsSystem("Windows");
            navComments.setCreateBy("admin");
            navComments.setCommentsEmail("2327972001@qq.com");
            navComments.setCreateTime(DateUtils.getNowDate());
            iNavCommentsService.save(navComments);
        }
    }

    /**
     * 获取用户信息
     */
    @Test
    void getUserInfo(){
        QueryWrapper<SysUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", 1).eq("del_flag", "0");
        System.out.println(iSysUserService.getOne(queryWrapper));
    }

    /**
     * 测试新增表是否可以查询
     */
    @Test
    void testAddTable(){
        iNavApiRequestKeyService.list().forEach(System.out::println);
        iNavApiResponseKeyService.list().forEach(System.out::println);
    }

    /**
     * 缓存数据进redis
     */
    @Test
    void cacheData(){
        redisCache.setCacheObject("nav_web:ceshi", "test");
    }

    /**
     * 获取缓存数据
     */
    @Test
    void getCacheData(){
        NavLabel json = redisCache.getCacheObject( "1");
        System.out.println(json);
        System.out.println(json == null);
    }

    /**
     * 获取附件列表
     */
    @Test
    void getAttachment(){
        List<File> fileList = FileUtils.getFilesInFolderList("D:\\ruoyi");
        for (File file : fileList) {
            System.out.println(file.getName());
        }
    }

    /**
     * 截取字符串
     */
    @Test
    void subString(){
        String str = "D:/ruoyi/uploadPath/upload/2022/09/15/kan_logo_20220915163833A003.png";
        String subStr = "upload/";
        // 截取某个字符之前的所有字符
        System.out.println(StringUtils.substringBefore(str, subStr));
        // 截取某个字符之后的所有字符
        System.out.println(StringUtils.substringAfter(str, subStr));
        // 获取文件访问地址
        String fileUrl = StringUtils.substringAfter(str, subStr);
        System.out.println(fileUrl);
    }
}
