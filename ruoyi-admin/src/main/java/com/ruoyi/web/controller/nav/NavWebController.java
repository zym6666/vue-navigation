package com.ruoyi.web.controller.nav;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.annotation.RateLimiter;
import com.ruoyi.common.constant.HttpStatus;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.enums.LimitType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.utils.sql.SqlUtil;
import com.ruoyi.system.domain.NavWeb;
import com.ruoyi.system.service.INavClassifyService;
import com.ruoyi.system.service.INavWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 网址列表Controller
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/navigation/web")
public class NavWebController extends BaseController
{
    @Autowired
    private INavWebService navWebService;

    @Autowired
    private INavClassifyService iNavClassifyService;

    /**
     * 查询随机网址列表
     */
    @Anonymous
    @GetMapping("/random")
    public AjaxResult random(Integer number)
    {
        if(number == null){
            error("参数错误");
        }
        return success(navWebService.selectNavWebListByRandom(number));
    }

    /**
     * 查询非审核链接数量
     */
    @Anonymous
    @GetMapping("/noAuditCount")
    public AjaxResult noAuditCount()
    {
        QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("audit_status", "0").or().eq("audit_status","2");
        Long noAuditCount = navWebService.count(queryWrapper);
        return success(noAuditCount);
    }

    /**
     * 查询待审核链接数量
     */
    @Anonymous
    @GetMapping("/auditCount")
    public AjaxResult auditCount()
    {
        QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("audit_status", "1");
        Long auditCount = navWebService.count(queryWrapper);
        return success(auditCount);
    }

    /**
     * 查询网址列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:list')")
    @GetMapping("/list")
    public TableDataInfo list(NavWeb navWeb)
    {
        startPage();
        List<NavWeb> list = navWebService.selectNavWebList(navWeb);
        return getDataTable(list);
    }

    /**
     * 查费非审核网址列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:list')")
    @GetMapping("/noAuditList")
    public TableDataInfo noAuditList(NavWeb navWeb)
    {
        startPage();
        List<NavWeb> list = navWebService.selectNoAuditNavWebList(navWeb);
        return getDataTable(list);
    }

    /**
     * 查费非审核网址列表列表
     */
    @Anonymous
    @GetMapping("/sortNoAuditList")
    public TableDataInfo sortNoAuditList(NavWeb navWeb)
    {
        if(navWeb.getLabelList() == null || navWeb.getLabelList().size() == 0){
            startPage();
            List<NavWeb> list = navWebService.selectSortNoAuditNavWebList(navWeb);
            return getDataTable(list);
        } else{
            //这种方式效率非常低，超过500条后查询非常缓慢，后期需要优化
            PageDomain pageDomain = TableSupport.buildPageRequest();
            Integer pageNum = pageDomain.getPageNum();
            Integer pageSize = pageDomain.getPageSize();
            String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
            Boolean reasonable = pageDomain.getReasonable();
            PageHelper.startPage(pageNum, pageSize, orderBy).setReasonable(reasonable);

            List<NavWeb> list = navWebService.selectSortNoAuditNavWebList(navWeb);
            List<NavWeb> webList = new ArrayList<>();
            for (NavWeb web : list) {
                int index = 0;
                for (String s : web.getLabelList()) {
                    if (navWeb.getLabelList().contains(s)) {
                        index++;
                    }
                }
                if (index >= navWeb.getLabelList().size()) {
                    webList.add(web);
                }
            }
            //获取处理好的list集合
            int num = webList.size();
            webList = webList.stream().skip((long) (pageNum - 1) * pageSize).limit(pageSize).collect(Collectors.toList());
            TableDataInfo rspData = new TableDataInfo();
            rspData.setCode(HttpStatus.SUCCESS);
            rspData.setMsg("查询成功");
            rspData.setRows(webList);
            rspData.setTotal(num);
            return rspData;
        }
    }

    /**
     * 查询非审核网址列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:list')")
    @GetMapping("/auditList")
    public TableDataInfo auditList(NavWeb navWeb)
    {
        startPage();
        List<NavWeb> list = navWebService.selectAuditNavWebList(navWeb);
        return getDataTable(list);
    }

    /**
     * 导出网址列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:export')")
    @Log(title = "网址列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavWeb navWeb)
    {
        List<NavWeb> list = navWebService.selectNavWebList(navWeb);
        ExcelUtil<NavWeb> util = new ExcelUtil<NavWeb>(NavWeb.class);
        util.exportExcel(response, list, "网址列表数据");
    }

    /**
     * 获取网址列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(navWebService.selectNavWebById(id));
    }

    /**
     * 获取网址列表详细信息
     */
    @Anonymous
    @GetMapping(value = "/info/{id}")
    public AjaxResult info(@PathVariable("id") Long id)
    {
        NavWeb navWeb = navWebService.selectNavWebById(id);
        if(navWeb == null){
            return error("链接不存在");
        }
        if(navWeb.getWebStatus().equals("2")){
            return error("链接不可见");
        }
        if(navWeb.getAuditStatus().equals("1")){
            return error("链接未审核");
        }
        if(navWeb.getAuditStatus().equals("3")){
            return error("链接审核未通过");
        }
        return success(navWeb);
    }

    /**
     * 前台提交网址接口
     */
    @PostMapping("/nav")
    public AjaxResult navAdd(@RequestBody NavWeb navWeb)
    {
        if(navWeb.getLabelList().size() == 0){
            return error("请选择标签");
        }
        if(navWeb.getLabelList().size() > 5){
            return error("标签最多5个");
        }
        if(StringUtils.isEmpty(navWeb.getWebName())){
            return error("网站名称不能为空");
        }
        if(navWeb.getWebName().length() > 20){
            return error("网站名称长度在 1 到 20 个字符");
        }
        if(StringUtils.isEmpty(navWeb.getWebDetails())){
            return error("网站详情不能为空");
        }
        if(navWeb.getWebDetails().length() > 100){
            return error("网站名称长度在 1 到 100 个字符");
        }
        if(StringUtils.isEmpty(navWeb.getWebUrl())){
            return error("网站链接不能为空");
        }
        if(navWeb.getWebUrl().length() < 3 || navWeb.getWebUrl().length() > 30){
            return error("网站链接长度在 3 到 30 个字符");
        }
        if(navWeb.getClassifyId() == null){
            return error("分类不能为空");
        }
        //检测分类是否存在
        if(iNavClassifyService.selectNavClassifyById(navWeb.getClassifyId().intValue()) == null){
            return error("分类不存在");
        }
        //默认审核状态为未审核
        navWeb.setAuditStatus("1");
        return toAjax(navWebService.insertNavWeb(navWeb));
    }

    /**
     * 新增网址列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:add')")
    @Log(title = "网址列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavWeb navWeb)
    {
        if(navWeb.getLabelList().size() == 0){
            return error("请选择标签");
        }
        if(navWeb.getLabelList().size() > 5){
            return error("标签最多5个");
        }
        //检测分类是否存在
        if(iNavClassifyService.selectNavClassifyById(navWeb.getClassifyId().intValue()) == null){
            return error("分类不存在");
        }
        return toAjax(navWebService.insertNavWeb(navWeb));
    }

    /**
     * 增加浏览量
     */
    @Anonymous
    @GetMapping("/addViewCount/{id}")
    @RateLimiter(key = "addViewCount", count = 2, time = 5, limitType = LimitType.IP)
    public AjaxResult addViewCount(@PathVariable("id") Long id)
    {
        NavWeb navWeb = navWebService.selectNavWebById(id);
        if(navWeb == null){
            return error("链接不存在");
        }
        NavWeb web = new NavWeb();
        web.setId(id);
        web.setWebViews(navWeb.getWebViews()+1);
        return toAjax(navWebService.saveOrUpdate(web));
    }

    /**
     * 修改网址列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:edit')")
    @Log(title = "网址列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavWeb navWeb)
    {
        return toAjax(navWebService.updateNavWeb(navWeb));
    }

    /**
     * 删除网址列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:web:remove')")
    @Log(title = "网址列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(navWebService.deleteNavWebByIds(ids));
    }
}
