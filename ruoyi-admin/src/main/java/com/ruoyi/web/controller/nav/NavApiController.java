package com.ruoyi.web.controller.nav;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavApi;
import com.ruoyi.system.service.INavApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 接口Controller
 * 
 * @author ZouYangMing
 * @date 2022-10-10
 */
@RestController
@RequestMapping("/navigation/api")
public class NavApiController extends BaseController
{
    @Autowired
    private INavApiService navApiService;

    /**
     * 获取接口数量
     */
    @Anonymous
    @GetMapping("/apiCount")
    public AjaxResult apiCount()
    {
        return success(navApiService.selectNavApiList(null).size());
    }

    /**
     * 查询接口列表
     */
    @Anonymous
    @GetMapping("/list")
    public TableDataInfo list(NavApi navApi)
    {
        startPage();
        List<NavApi> list = navApiService.selectNavApiList(navApi);
        return getDataTable(list);
    }

    /**
     * 导出接口列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:api:export')")
    @Log(title = "接口列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavApi navApi)
    {
        List<NavApi> list = navApiService.selectNavApiList(navApi);
        ExcelUtil<NavApi> util = new ExcelUtil<NavApi>(NavApi.class);
        util.exportExcel(response, list, "接口列表数据");
    }

    /**
     * 获取接口详细信息
     */
    @Anonymous
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(navApiService.selectNavApiById(id));
    }

    /**
     * 新增接口
     */
    @PreAuthorize("@ss.hasPermi('navigation:api:add')")
    @Log(title = "接口列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavApi navApi)
    {
        return toAjax(navApiService.insertNavApi(navApi));
    }

    /**
     * 修改接口
     */
    @PreAuthorize("@ss.hasPermi('navigation:api:edit')")
    @Log(title = "接口列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavApi navApi)
    {
        return toAjax(navApiService.updateNavApi(navApi));
    }

    /**
     * 删除接口
     */
    @PreAuthorize("@ss.hasPermi('navigation:api:remove')")
    @Log(title = "接口列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(navApiService.deleteNavApiByIds(ids));
    }
}
