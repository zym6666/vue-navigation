package com.ruoyi.web.controller.nav;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.NavWeb;
import com.ruoyi.system.domain.NavWebCollect;
import com.ruoyi.system.service.INavWebCollectService;
import com.ruoyi.system.service.INavWebService;
import com.ruoyi.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * 收藏Controller
 */
@RestController
@RequestMapping("/navigation/navWebCollect")
public class NavWebCollectController extends BaseController {

    @Autowired
    private INavWebCollectService navWebCollectService;

    @Autowired
    private INavWebService navWebService;

    @Autowired
    private ISysUserService userService;

    /**
     * 查询收藏列表
     */
    @Anonymous
    @GetMapping("/list")
    public AjaxResult noAuditList(Long userId, Integer pageNum, Integer listSize)
    {
        SysUser user = userService.selectUserById(userId);
        if (user == null) {
            return error("当前用户不存在");
        }
        if(user.getDelFlag().equals("2")){
            return error("当前用户不存在");
        }
        if(user.getStatus().equals("1")){
            return error("当前用户已被停用");
        }
        //查询当前用户的收藏列表
        QueryWrapper<NavWebCollect> collect = new QueryWrapper<>();
        collect.eq("user_id", userId);
        List<NavWebCollect> navWebCollects = navWebCollectService.list(collect);
        if(navWebCollects.size() == 0){
            HashMap<String, Integer> map = new HashMap<String, Integer>();
            map.put("total", 0);
            return success(map);
        }
        QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
        for (NavWebCollect nav : navWebCollects) {
            queryWrapper.or().eq("id", nav.getWebId());
        }
        return page(navWebService.page(new Page<>(pageNum, listSize), queryWrapper));
    }

    /**
     * 增加收藏
     *
     * @param navWebCollect 收藏信息
     */
    @GetMapping("/addCollect")
    public AjaxResult addCollect(NavWebCollect navWebCollect) {
        System.out.println(navWebCollect);
        if(navWebCollect.getWebId() == null || navWebCollect.getWebId() == 0){
            return error("参数错误");
        }
        NavWebCollect collect = new NavWebCollect();
        collect.setUserId(getUserId());
        collect.setWebId(navWebCollect.getWebId());
        collect.setCreateBy(getUsername());
        collect.setCreateTime(DateUtils.getNowDate());
        if(navWebCollectService.save(collect)){
            NavWeb navWeb = new NavWeb();
            navWeb.setId(navWebCollect.getWebId());
            navWeb.setWebCollection(navWebService.getById(navWebCollect.getWebId()).getWebCollection() + 1);
            return toAjax(navWebService.updateNavWeb(navWeb));
        }else{
            return error("未知错误");
        }
    }

    /**
     * 删除收藏
     *
     * @param id 收藏ID
     */
    @GetMapping("/deleteCollect")
    public AjaxResult deleteCollect(Long id) {
        if(id == null){
            return error("参数错误");
        }
        NavWeb web = navWebService.getById(id);
        if(web == null){
            return error("网址不存在");
        }
        QueryWrapper<NavWebCollect> collect = new QueryWrapper<>();
        collect.eq("user_id", getUserId());
        collect.eq("web_id", id);
        if(navWebCollectService.remove(collect)){
            NavWeb navWeb = new NavWeb();
            navWeb.setId(id);
            navWeb.setWebCollection(web.getWebCollection() - 1);
            return toAjax(navWebService.updateNavWeb(navWeb));
        }else{
            return error("未知错误");
        }
    }

    /**
     * 查询是否被收藏
     *
     * @param id 链接id
     */
    @GetMapping("/isCollect")
    public AjaxResult isCollect(Long id) {
        if(id == null || id == 0){
            return error("参数错误");
        }
        QueryWrapper<NavWebCollect> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("web_id", id);
        queryWrapper.eq("user_id", getUserId());
        if(navWebCollectService.getOne(queryWrapper) != null){
            return success(true);
        }else{
            return success(false);
        }
    }


}
