package com.ruoyi.web.controller.nav;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavLabel;
import com.ruoyi.system.domain.NavWebLabel;
import com.ruoyi.system.service.INavLabelService;
import com.ruoyi.system.service.INavWebLabelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 标签列表Controller
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/navigation/label")
public class NavLabelController extends BaseController
{
    @Autowired
    private INavLabelService navLabelService;

    @Autowired
    private INavWebLabelService iNavWebLabelService;

    /**
     * 查询标签数量
     */
    @Anonymous
    @GetMapping("/labelCount")
    public AjaxResult labelCount()
    {
        return success(navLabelService.count());
    }

    /**
     * 查询标签列表列表
     */
    @Anonymous
    @GetMapping("/list")
    public TableDataInfo list(NavLabel navLabel)
    {
        startPage();
        List<NavLabel> list = navLabelService.selectNavLabelList(navLabel);
        return getDataTable(list);
    }

    /**
     * 导出标签列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:label:export')")
    @Log(title = "标签列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavLabel navLabel)
    {
        List<NavLabel> list = navLabelService.selectNavLabelList(navLabel);
        ExcelUtil<NavLabel> util = new ExcelUtil<NavLabel>(NavLabel.class);
        util.exportExcel(response, list, "标签列表数据");
    }

    /**
     * 获取标签列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('navigation:label:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(navLabelService.selectNavLabelById(id));
    }

    /**
     * 新增标签列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:label:add')")
    @Log(title = "标签列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavLabel navLabel)
    {
        return toAjax(navLabelService.insertNavLabel(navLabel));
    }

    /**
     * 修改标签列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:label:edit')")
    @Log(title = "标签列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavLabel navLabel)
    {
        return toAjax(navLabelService.updateNavLabel(navLabel));
    }

    /**
     * 删除标签列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:label:remove')")
    @Log(title = "标签列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            QueryWrapper<NavWebLabel> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("label_id", id);
            if (iNavWebLabelService.list(queryWrapper).size() > 0) {
                return error("标签已被使用，不允许删除");
            }
        }
        return toAjax(navLabelService.deleteNavLabelByIds(ids));
    }
}
