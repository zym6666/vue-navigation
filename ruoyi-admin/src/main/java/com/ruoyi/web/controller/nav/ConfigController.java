package com.ruoyi.web.controller.nav;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 设置Controller
 */
@RestController
@RequestMapping("/navigation/config")
public class ConfigController extends BaseController {

    @Autowired
    private ISysConfigService configService;

    /**
     * 获取设置列表
     */
    @Anonymous
    @GetMapping("/list")
    public AjaxResult list() {
        HashMap<String, Object> map = new HashMap<>();
        //获取网站名称
        map.put("name", configService.selectConfigByKey("web.name"));
        //获取网站地址
        map.put("url", configService.selectConfigByKey("web.url"));
        //网站底部信息
        map.put("footerInfo", configService.selectConfigByKey("web.footer.info"));
        //网站SEO关键词
        List<String> keywordsList = new ArrayList<>(Arrays.asList(configService.selectConfigByKey("web.seo.keywords").split(",")));
        map.put("keywords", keywordsList);
        //网站SEO描述
        map.put("description", configService.selectConfigByKey("web.seo.description"));
        //网站SEO标题分隔符
        map.put("separate", configService.selectConfigByKey("nav.seo.separate"));
        //全局禁用关键词
        List<String> disableKeywords = new ArrayList<>(Arrays.asList(configService.selectConfigByKey("web.disable.keywords").split(",")));
        map.put("disableKeywords", disableKeywords);
        //验证码开关
        map.put("disableCaptcha", Boolean.parseBoolean(configService.selectConfigByKey("sys.account.captchaEnabled")));
        //注册开关
        map.put("disableRegister", Boolean.parseBoolean(configService.selectConfigByKey("sys.account.registerUser")));
        //首页链接显示数量
        map.put("indexLinkCount", Integer.parseInt(configService.selectConfigByKey("nav.index.link.number")));
        return success(map);
    }

}
