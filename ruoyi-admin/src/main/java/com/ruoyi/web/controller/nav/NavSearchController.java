package com.ruoyi.web.controller.nav;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavSearch;
import com.ruoyi.system.service.INavSearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 搜索列表Controller
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/navigation/search")
public class NavSearchController extends BaseController
{
    @Autowired
    private INavSearchService navSearchService;

    /**
     * 查询搜索列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:list')")
    @GetMapping("/list")
    public TableDataInfo list(NavSearch navSearch)
    {
        startPage();
        List<NavSearch> list = navSearchService.selectNavSearchList(navSearch);
        return getDataTable(list);
    }

    /**
     * 获取搜索列表
     */
    @Anonymous
    @GetMapping("/getList")
    public AjaxResult getList()
    {
        return success(navSearchService.getNavSearchList());
    }


    /**
     * 导出搜索列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:export')")
    @Log(title = "搜索列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavSearch navSearch)
    {
        List<NavSearch> list = navSearchService.selectNavSearchList(navSearch);
        ExcelUtil<NavSearch> util = new ExcelUtil<NavSearch>(NavSearch.class);
        util.exportExcel(response, list, "搜索列表数据");
    }

    /**
     * 获取搜索详细信息
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(navSearchService.selectNavSearchById(id));
    }

    /**
     * 新增搜索
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:add')")
    @Log(title = "搜索列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavSearch navSearch)
    {
        return toAjax(navSearchService.insertNavSearch(navSearch));
    }

    /**
     * 修改搜索
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:edit')")
    @Log(title = "搜索列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavSearch navSearch)
    {
        return toAjax(navSearchService.updateNavSearch(navSearch));
    }

    /**
     * 删除搜索
     */
    @PreAuthorize("@ss.hasPermi('navigation:search:remove')")
    @Log(title = "搜索列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(navSearchService.deleteNavSearchByIds(ids));
    }
}
