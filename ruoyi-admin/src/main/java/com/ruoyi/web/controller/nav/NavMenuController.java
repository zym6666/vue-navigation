package com.ruoyi.web.controller.nav;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavMenu;
import com.ruoyi.system.service.INavMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 菜单列表Controller
 * 
 * @author ZouYangMing
 * @date 2022-09-14
 */
@RestController
@RequestMapping("/navigation/menu")
public class NavMenuController extends BaseController
{
    @Autowired
    private INavMenuService navMenuService;

    /**
     * 查询菜单列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:list')")
    @GetMapping("/list")
    public TableDataInfo list(NavMenu navMenu)
    {
        startPage();
        List<NavMenu> list = navMenuService.selectNavMenuList(navMenu);
        return getDataTable(list);
    }

    /**
     * 前台查询菜单列表列表
     */
    @Anonymous
    @GetMapping("/noLimitList")
    public AjaxResult list()
    {
        return success(navMenuService.selectNavMenuNoLimitList());
    }

    /**
     * 导出菜单列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:export')")
    @Log(title = "菜单列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavMenu navMenu)
    {
        List<NavMenu> list = navMenuService.selectNavMenuList(navMenu);
        ExcelUtil<NavMenu> util = new ExcelUtil<NavMenu>(NavMenu.class);
        util.exportExcel(response, list, "菜单列表数据");
    }

    /**
     * 获取菜单列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(navMenuService.selectNavMenuById(id));
    }

    /**
     * 新增菜单列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:add')")
    @Log(title = "菜单列表", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavMenu navMenu)
    {
        return toAjax(navMenuService.insertNavMenu(navMenu));
    }

    /**
     * 修改菜单列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:edit')")
    @Log(title = "菜单列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavMenu navMenu)
    {
        return toAjax(navMenuService.updateNavMenu(navMenu));
    }

    /**
     * 删除菜单列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:menu:remove')")
    @Log(title = "菜单列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        for (Long id : ids) {
            NavMenu navMenu = navMenuService.selectNavMenuById(id);
            if (navMenu.getMenuType().equals("Y")) {
                return error("系统内置菜单不允许删除");
            }
        }
        return toAjax(navMenuService.deleteNavMenuByIds(ids));
    }
}
