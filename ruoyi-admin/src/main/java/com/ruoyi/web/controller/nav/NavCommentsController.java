package com.ruoyi.web.controller.nav;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavComments;
import com.ruoyi.system.service.INavCommentsService;
import com.ruoyi.system.service.INavWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 评论列表Controller
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/navigation/comments")
public class NavCommentsController extends BaseController
{
    @Autowired
    private INavCommentsService navCommentsService;

    @Autowired
    private INavWebService navWebService;

    /**
     * 查询评论数量
     */
    @Anonymous
    @GetMapping("/commentsCount")
    public AjaxResult commentsCount()
    {
        return success(navCommentsService.count());
    }

    /**
     * 查询评论列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:comments:list')")
    @GetMapping("/list")
    public TableDataInfo list(NavComments navComments)
    {
        startPage();
        List<NavComments> list = navCommentsService.selectNavCommentsList(navComments);
        return getDataTable(list);
    }

    /**
     * 前台查询评论列表
     */
    @Anonymous
    @GetMapping("/pidList")
    public TableDataInfo navList(NavComments navComments)
    {
        startPage();
        List<NavComments> list = navCommentsService.selectPidNavCommentsList(navComments);
        NavComments comments = new NavComments();
        list.forEach(item -> {
            comments.setCommentsId(item.getId());
            item.setChildren(navCommentsService.selectPidNavCommentsList(comments));
        });
        return getDataTable(list);
    }

    /**
     * 导出评论列表列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:comments:export')")
    @Log(title = "评论列表", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavComments navComments)
    {
        List<NavComments> list = navCommentsService.selectNavCommentsList(navComments);
        ExcelUtil<NavComments> util = new ExcelUtil<NavComments>(NavComments.class);
        util.exportExcel(response, list, "评论列表数据");
    }

    /**
     * 获取评论列表详细信息
     */
    @PreAuthorize("@ss.hasPermi('navigation:comments:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(navCommentsService.selectNavCommentsById(id));
    }

    /**
     * 新增评论列表
     */
    @PostMapping
    public AjaxResult add(@RequestBody NavComments navComments)
    {
        if(StringUtils.isEmpty(navComments.getCommentsText())){
            return error("评论内容不能为空");
        }
        if(navComments.getCommentsText().length() < 2 || navComments.getCommentsText().length() > 100){
            return error("评论内容长度必须在2到100之间");
        }
        if(navComments.getWebId() == null){
            return error("参数错误");
        }
        if (navWebService.getById(Long.valueOf(navComments.getWebId())) == null) {
            return error("评论所属的网址不存在");
        }
        NavComments comments = new NavComments();
        comments.setWebId(navComments.getWebId());
        comments.setCommentsText(navComments.getCommentsText());
        comments.setCommentsId(navComments.getCommentsId());
        comments.setBtUsername(navComments.getBtUsername());
        return toAjax(navCommentsService.insertNavComments(comments));
    }

    /**
     * 修改评论列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:comments:edit')")
    @Log(title = "评论列表", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavComments navComments)
    {
        return toAjax(navCommentsService.updateNavComments(navComments));
    }

    /**
     * 前台删除评论
     */
    @DeleteMapping("/nav/{id}")
    public AjaxResult remove(@PathVariable Integer id)
    {
        NavComments comments = navCommentsService.getById(id);
        if(comments == null){
            return error("评论不存在");
        }else{
            //判断是否是自己的评论
            if(comments.getCreateBy().equals(getUsername())){
                return toAjax(navCommentsService.deleteNavCommentsById(id));
            }else{
                return error("无法删除他人评论");
            }
        }
    }

    /**
     * 删除评论列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:comments:remove')")
    @Log(title = "评论列表", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(navCommentsService.deleteNavCommentsByIds(ids));
    }
}
