package com.ruoyi.web.controller.nav;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.NavClassify;
import com.ruoyi.system.domain.NavWeb;
import com.ruoyi.system.service.INavClassifyService;
import com.ruoyi.system.service.INavWebService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 分类Controller
 * 
 * @author zouyangming
 * @date 2022-09-07
 */
@RestController
@RequestMapping("/navigation/classify")
public class NavClassifyController extends BaseController
{
    @Autowired
    private INavClassifyService navClassifyService;

    @Autowired
    private INavWebService iNavWebService;

    /**
     * 查询分类列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:classify:list')")
    @GetMapping("/list")
    public TableDataInfo list(NavClassify navClassify)
    {
        startPage();
        List<NavClassify> list = navClassifyService.selectNavClassifyList(navClassify);
        return getDataTable(list);
    }

    /**
     * 查询分类列表
     *
     * @param navClassify 分类的类
     * @return 结果
     */
    @Anonymous
    @GetMapping("/listAll")
    public AjaxResult listAll(NavClassify navClassify)
    {
        List<NavClassify> list = navClassifyService.selectNavClassifyList(navClassify);
        return success(list);
    }

    /**
     * 导出分类列表
     */
    @PreAuthorize("@ss.hasPermi('navigation:classify:export')")
    @Log(title = "分类", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, NavClassify navClassify)
    {
        List<NavClassify> list = navClassifyService.selectNavClassifyList(navClassify);
        ExcelUtil<NavClassify> util = new ExcelUtil<NavClassify>(NavClassify.class);
        util.exportExcel(response, list, "分类数据");
    }

    /**
     * 获取分类详细信息
     */
    @Anonymous
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return success(navClassifyService.selectNavClassifyById(id));
    }

    /**
     * 新增分类
     */
    @PreAuthorize("@ss.hasPermi('navigation:classify:add')")
    @Log(title = "分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody NavClassify navClassify)
    {
        return toAjax(navClassifyService.insertNavClassify(navClassify));
    }

    /**
     * 修改分类
     */
    @PreAuthorize("@ss.hasPermi('navigation:classify:edit')")
    @Log(title = "分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody NavClassify navClassify)
    {
        return toAjax(navClassifyService.updateNavClassify(navClassify));
    }

    /**
     * 删除分类
     */
    @PreAuthorize("@ss.hasPermi('navigation:classify:remove')")
    @Log(title = "分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        for (Integer id : ids) {
            QueryWrapper<NavWeb> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("classify_id",id);
            if (iNavWebService.list(queryWrapper).size() > 0) {
                return error("分类下存在网址，不允许删除");
            }
        }
        return toAjax(navClassifyService.deleteNavClassifyByIds(ids));
    }
}
