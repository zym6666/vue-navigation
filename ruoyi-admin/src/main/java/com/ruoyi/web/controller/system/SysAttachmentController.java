package com.ruoyi.web.controller.system;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysAttachment;
import com.ruoyi.system.service.ISysAttachmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 附件管理控制层
 *
 * @author ZouYangMing
 */
@RestController
@RequestMapping("/system/attachment")
public class SysAttachmentController extends BaseController {

    @Autowired
    private ISysAttachmentService fileService;

    /**
     * 获取附件列表
     */
    @PreAuthorize("@ss.hasPermi('system:attachment:list')")
    @GetMapping("/list")
    public AjaxResult list(SysAttachment sysAttachment) {
        return success(fileService.getFileList(sysAttachment));
    }

    /**
     * 查询附件列表
     */
    @PreAuthorize("@ss.hasPermi('system:attachment:query')")
    @GetMapping("/query")
    public AjaxResult query(SysAttachment sysAttachment) {
        return success(fileService.queryFileList(sysAttachment));
    }

    /**
     * 批量删除附件
     */
    @PreAuthorize("@ss.hasPermi('system:attachment:remove')")
    @Log(title = "附件管理", businessType = BusinessType.DELETE)
    @PostMapping("/removeAll")
    public AjaxResult removeAll(@Validated @RequestBody String[] fileList) {
        if(fileList.length > 0){
            fileService.deleteFileList(fileList);
        }
        return success();
    }

    /**
     * 删除附件
     */
    @PreAuthorize("@ss.hasPermi('system:attachment:remove')")
    @Log(title = "附件管理", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    public AjaxResult remove(@Validated @RequestBody String path) {
        fileService.deleteFile(path);
        return success();
    }

}
