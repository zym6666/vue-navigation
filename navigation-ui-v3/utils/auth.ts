/**
 * Token名字
 */
const TokenKey = 'Token';

/**
 * 获取Token
 */
export const getToken = () => useCookie("Token").value;

/**
 * 新增Token
 * @param token
 */
export const setToken = (token:string) => {
    const setToken = useCookie(TokenKey, {
        path: "/",
        maxAge: 60 * 60 * 24 * 7
    });
    setToken.value = token;
};