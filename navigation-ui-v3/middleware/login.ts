export default defineNuxtRouteMiddleware((to, from) => {
    //从cookie中读取token
    let token = useToken();
    if(token.value){
        app.store.commit("user/SET_TOKEN",token);
    }
})