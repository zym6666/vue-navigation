// https://nuxt.com/docs/api/configuration/nuxt-config
import ElementPlus from 'unplugin-element-plus/vite';

export default defineNuxtConfig({
    runtimeConfig: {
        // 公开的变量
        public: {
            // api地址
            API_BASE: "http://127.0.0.1:3000/api"
        }
    },
    css: [
        '@/assets/css/reset.css',
        '@/assets/css/common.css',
        '@/assets/css/display.css',
        'element-plus/dist/index.css',
        'element-plus/theme-chalk/display.css',
    ],
    build: {
        transpile: ['element-plus/es'],
    },
    vite: {
        plugins: [
            ElementPlus()
        ],
    },
    nitro: {
        devProxy: {
            '/api': {
                target: 'http://localhost:8080',
                changeOrigin: true
            }
        }
    }
})
