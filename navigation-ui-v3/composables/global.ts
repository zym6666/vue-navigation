// 全局参数
export const useConfig = () => useState("config", () => ({
    "disableCaptcha": true,
    "keywords": ["关键词1", "关键词2", "关键词3"],
    "separate": " - ",
    "indexLinkCount": 20,
    "name": "聚合导航",
    "description": "这是一个很方便的导航",
    "disableKeywords": ["艹", "尼玛", "死了", "操你", "操她", "操他"],
    "disableRegister": true,
    "url": "www.zym88.cn",
    "footerInfo": "@2022 聚合导航"
}));
// 分类列表
export const useClassifyList = () => useState("classifyList", () => []);
// 菜单列表
export const useMenuList = () => useState("menuList", () => []);