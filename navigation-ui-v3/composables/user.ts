// 用户角色
export const useRoles = () => useState("roles", () => []);
// 用户信息
export const useUserInfo = () => useState("userInfo", () => []);
// 登录状态
export const useIsLogin = () => useState("isLogin", () => false);