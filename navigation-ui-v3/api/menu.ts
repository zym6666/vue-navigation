import Request from '@/utils/request';

export default new class menu extends Request {
    /**
     * 获取菜单列表
     */
    getList(){
        return this.get("/navigation/menu/noLimitList");
    }
}();